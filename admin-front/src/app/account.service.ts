import {Injectable} from '@angular/core';
import {JsIshopService} from './js-ishop-service';
import {catchError, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '../../node_modules/@angular/common/http';
import {MatBottomSheet} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends JsIshopService {
  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  checkUser(login: string, password: string): Observable<Response> {
    sessionStorage.setItem('logData', btoa(login + ':' + password));
    sessionStorage.removeItem("admin");
    return this.http.get<Response>(this.jsIshopUrl + `/stats/top10CustomerTotal`, this.getHttpOptions(login, password))
      .pipe(tap(response => {
          console.log(response);
          this.handleSuccess('Login OK');
          sessionStorage.setItem("admin","ok");
        }),
        catchError(this.handleError(
          `Username or password error`, null)));
  }
}
