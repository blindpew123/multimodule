import {Injectable} from '@angular/core';
import {JsIshopService} from './js-ishop-service';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {MatBottomSheet} from '@angular/material';
import {Observable} from 'rxjs';
import {Category} from './Data/category';
import {catchError, tap} from 'rxjs/operators';
import {Parameter} from './Data/parameter';
import {ParameterType} from './Data/parameter-type';
import {ShopItemSaveDto} from './Data/shop-item-save-dto';

@Injectable({
  providedIn: 'root'
})
export class ParameterService extends JsIshopService {

 /* private _paramters: Parameter[];
  get parameters(): Parameter[] {
    return this._paramters;
  }
  set parameters(parameters: Parameter[]) {
    this._paramters = parameters;
  }*/



  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  // оставить здесь
  getParametersForCategory(category: Category): Observable<Parameter[]> {
    return this.http.get<Parameter[]>(this.jsIshopUrl + `/getParametersForCategory/${category.idCategory}`, this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(parameters => {
          console.log(parameters);
          this.handleSuccess('Parameters loaded');
       // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Error loading parameters for Category ${category.idCategory}`, [])));
  }

  // оставить здесь
  getParameterTypes(): Observable<ParameterType[]> {
    return this.http.get<ParameterType[]>(this.jsIshopUrl + '/getParameterTypes', this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(parameterTypes => {
          console.log(parameterTypes);
          this.handleSuccess('Parameters Types loaded');
      //    this.parameterTypes = parameterTypes;
        }),
        catchError(this.handleError(
          'Error loading Parameter Types', [])));
  }

 /* getParameterValuesForCategory(): Observable<ParameterType[]> {
    return this.http.get<ParameterType[]>(this.jsIshopUrl + '/getParameterTypes', this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(parameterTypes => {
          console.log(parameterTypes);
          this.handleSuccess('Список типов параметров загружен');
          //    this.parameterTypes = parameterTypes;
        }),
        catchError(this.handleError(
          'Ошибка загузки типов параметров', [])));
  } */

  saveOrUpdateParameterList(parameters: Parameter[]): Observable<Response>{

    return this.http.post<Response>(this.jsIshopUrl + `/saveOrUpdateParameterDtoList`,
      JSON.stringify(parameters), this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(
      tap((response: Response) => {
        // console.log(`Параметры успешно добавлены или обновлены`);
        this.handleSuccess('Parameters saved');

      }),
      catchError(this.handleError<Response>('Error saving parameters'))
    );
  }

  deleteParameter(id: number):Observable<Response>{
    return this.http.post<Response>(this.jsIshopUrl + `/parameter/${id}/delete`,
      JSON.stringify(""),this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(
        tap((response: Response) => {
          // console.log(`Параметры успешно добавлены или обновлены`);
          this.handleSuccess('Parameters saved');

        }),
        catchError(this.handleError<Response>('Error saving parameters'))
      );
  }

}
