import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryDeleteConfirmComponent } from './category-delete-confirm.component';

describe('CategoryDeleteConfirmComponent', () => {
  let component: CategoryDeleteConfirmComponent;
  let fixture: ComponentFixture<CategoryDeleteConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryDeleteConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryDeleteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
