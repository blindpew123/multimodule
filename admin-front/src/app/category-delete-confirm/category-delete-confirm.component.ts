import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DeleteCategoryDto} from '../Data/delete-category-dto';

@Component({
  selector: 'app-category-delete-confirm',
  templateUrl: './category-delete-confirm.component.html',
  styleUrls: ['./category-delete-confirm.component.css']
})
export class CategoryDeleteConfirmComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CategoryDeleteConfirmComponent>,
      @Inject(MAT_DIALOG_DATA) public data: {}) {
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    console.log("Yes");
  }
}
