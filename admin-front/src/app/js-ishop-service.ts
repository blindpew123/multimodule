import {HttpHeaders} from '@angular/common/http';
import {MatBottomSheet} from '@angular/material';
import {BottomSheetComponent} from './bottom-sheet/bottom-sheet.component';
import {Observable, of} from 'rxjs';

export class JsIshopService {
   /*protected static httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic '+btoa("0:0")
    })
  }; */
  protected jsIshopUrl = 'http://localhost:8080/admin';  // URL to web api !! NO '/' at the end of string

  constructor(protected bottomSheet: MatBottomSheet) {
  }

  protected getHttpOptions(login?:string, password?:string):{headers:HttpHeaders}{

    if(!!login && !!password){
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Basic '+ btoa(login + ":" + password)
        })
      }
    } else {
      if (!!sessionStorage.getItem("logData")) {
        return {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + sessionStorage.getItem("logData")
          })
        }
      } else {
        return {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Basic '+ btoa("notAdmin" + ":" + "wrongPass")
          })
        }
    }
    }
  }

  protected getHttpFileSendOptions():{headers:HttpHeaders}{
    return {
      headers: new HttpHeaders({
        'Authorization': 'Basic '+ sessionStorage.getItem("logData")
      })
    }
  }

  protected openBottomSheet(data: any): void {
    this.bottomSheet.open(BottomSheetComponent, data);
  }

  protected handleSuccess(serviceSuccessMessage: string, result?: any): void {
    this.openBottomSheet({data: {
        info: serviceSuccessMessage,
        obj: !!result ? result : {}
      }});
  }

  // ToDo: check how will be handled error messages from controllers
  protected handleError<T> (serviceErrMessage = 'Ошибка', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      this.openBottomSheet({data: {
          error: {message: serviceErrMessage + ` (${error.status})`}
        }});

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  protected getRightWord(num: number): string {
    let result: string;
    const tmpStr: string = '' + num;
    const val: number = +tmpStr.charAt(tmpStr.length - 1);
    switch (val) {
      case 1:
        result = ' запись';
        break;
      case 2:
      case 3:
      case 4:
        result = ' записи';
        break;
      default:
        result = ' записей';
    }
    return result;
  }
}
