import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Category} from '../Data/category';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {

  @ViewChild('categoryForm') public categoryForm: NgForm;

  private _category: Category;
  @Input()
  set category(category: Category){
    this._category = category;
  }
  get category(): Category{
    return this._category;
  }
  @Input()
  protected selectedTab: number;

  constructor(public adminService: AdminService,
              private router: Router) { }



  ngOnInit() {
  }

  onSubmit(){
   this.adminService.saveOrUpdateCategory(this.category).subscribe(category => {
      if (!!category) {
        this.categoryForm.resetForm();
        this.router.navigate(['categoriesAndItemsManagement']);
      }
    });
    // console.log(JSON.stringify(this.categoryService.element.getCategory()));
  }

}
