import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-parameter-delete-confirm',
  templateUrl: './parameter-delete-confirm.component.html',
  styleUrls: ['./parameter-delete-confirm.component.css']
})
export class ParameterDeleteConfirmComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ParameterDeleteConfirmComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {}) {
  }

  ngOnInit() {
  }

}
