import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterDeleteConfirmComponent } from './parameter-delete-confirm.component';

describe('ParameterDeleteConfirmComponent', () => {
  let component: ParameterDeleteConfirmComponent;
  let fixture: ComponentFixture<ParameterDeleteConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParameterDeleteConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterDeleteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
