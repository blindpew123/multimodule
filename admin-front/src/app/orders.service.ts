import { Injectable } from '@angular/core';
import {JsIshopService} from './js-ishop-service';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {MatBottomSheet} from '@angular/material';

import {Observable} from 'rxjs';

import {catchError, tap} from 'rxjs/operators';
import {OrderDto} from './Data/order-dto';
import {OrderStatus} from './Data/order-status';
import {OrderStatusUpdateDto} from './Data/order-status-update-dto';

@Injectable({
  providedIn: 'root'
})
export class OrdersService extends JsIshopService{

  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  getOrders(dateStart: string, dateEnd: string): Observable<OrderDto[]> {
    return this.http.get<OrderDto[]>(this.jsIshopUrl + `/orders/${dateStart}/${dateEnd}`, this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(orders => {
          console.log(orders);
          this.handleSuccess('Orders loaded');
          // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Error loading orders between ${dateStart} - ${dateEnd}`, [])));
  }
  getOrderStatuses(): Observable<OrderStatus[]> {
    return this.http.get<OrderStatus[]>(this.jsIshopUrl + `/orders/orderStatuses`, this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(orderStatuses => {
          console.log(orderStatuses);
          this.handleSuccess('Orders statuses loaded');
          // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Error loading orders statuses`, [])));
  }
  postOrderStatus(orderStatusUpdateDto: OrderStatusUpdateDto): Observable<Response> {
    return this.http.post<Response>(this.jsIshopUrl + `/orders/updateOrderStatus`,
      JSON.stringify(orderStatusUpdateDto), this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(() => {
         // console.log();
          this.handleSuccess('Order status changed');
        }),
        catchError(this.handleError(
          `Error changing order status`, null)));
  }



}
