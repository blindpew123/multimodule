import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {OrdersService} from '../orders.service';
import {OrderDto} from '../Data/order-dto';
import {OrderStatus} from '../Data/order-status';
import {OrderStatusUpdateDto} from '../Data/order-status-update-dto';

@Component({
  selector: 'app-orders-management',
  templateUrl: './orders-management.component.html',
  styleUrls: ['./orders-management.component.css']
})
export class OrdersManagementComponent implements OnInit {

  datesForm: FormGroup;
  orders: OrderDto[];
  orderStatuses: OrderStatus[];

  constructor(private orderService: OrdersService) {
  }

  ngOnInit() {
    this.getOrderStatuses();
    this.datesForm = new FormGroup({
      dateStart: new FormControl(''),
      dateEnd: new FormControl(''),
    });

  }

  onSubmit() {
    this.orderService.getOrders(
      this.datesForm.value['dateStart'], this.datesForm.value['dateEnd'])
      .subscribe(orders =>
        this.orders = orders);
  }

  onSelect(i: number) {
    const orderStatusUpdateDto = new OrderStatusUpdateDto();
    orderStatusUpdateDto.idOrder = this.orders[i].idOrder;
    orderStatusUpdateDto.idStatus = this.orders[i].orderStatus;
    this.orderService.postOrderStatus(orderStatusUpdateDto).subscribe();
  }

  getOrderDate(dateArray: string[]): string {
    return dateArray[0] + '-'
      + dateArray[1] + '-'
      + dateArray[2] + ' '
      + dateArray[3] + ':'
      + dateArray[4] + ':'
      + dateArray[5];
  }

  getOrderStatuses() {
    this.orderService.getOrderStatuses().subscribe(orderStatuses => {
      this.orderStatuses = orderStatuses;
    });
  }

  getOrderTotal(i: number): number {
    let total = 0;
    for (let orderItem of this.orders[i].orderItems) {
        total += orderItem.itemPrice * orderItem.qty;
    }
    return total;
  }


}
