import { Component, OnInit } from '@angular/core';
import {SaleStatisticService} from '../sale-statistic.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {


  datesForm: FormGroup;
  top10ItemsQty:String[][];
  top10CustomersTotal: String[][];
  totalForPeriod: number;

  constructor(private saleStatisticService: SaleStatisticService) { }

  ngOnInit() {
    this.get10TopCustomersTotal();
    this.get10TopItemsQty();
    this.datesForm = new FormGroup({
      dateStart: new FormControl(''),
      dateEnd: new FormControl(''),
    });
  }

  get10TopItemsQty(){
    this.saleStatisticService.getTop10ItemsQty().subscribe(topItems=>{
      this.top10ItemsQty = topItems;
    })
  }
  get10TopCustomersTotal(){
    this.saleStatisticService.getTop10Customers().subscribe((topCustomers=>{
      this.top10CustomersTotal = topCustomers;
    }))
  }

  onSubmit() {
    this.saleStatisticService.getTotalForPeriod(
      this.datesForm.value['dateStart'], this.datesForm.value['dateEnd'])
      .subscribe(result =>
        this.totalForPeriod = result);
  }

}
