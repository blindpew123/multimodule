import {Component, Input, OnInit, OnChanges, SimpleChange} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Parameter} from '../Data/parameter';
import {AdminService} from '../admin.service';
import {Category} from '../Data/category';
import {MatDialog} from '@angular/material';
import {ParameterDeleteConfirmComponent} from '../parameter-delete-confirm/parameter-delete-confirm.component';

@Component({
  selector: 'app-parameter-form',
  templateUrl: './parameter-form.component.html',
  styleUrls: ['./parameter-form.component.css']
})
export class ParameterFormComponent implements OnInit, OnChanges {

  parametersForm: FormGroup;
  private _parameters: Parameter[];
  get parameters(): Parameter[] {
    return this._parameters;
  }

  set parameters(parameters: Parameter[]) {
    this._parameters = parameters;
  }

  private _currentCategory: Category;
  @Input()
  get currentCategory(): Category {
    return this._currentCategory;
  }

  set currentCategory(currentCategory: Category) {
    this._currentCategory = currentCategory;
  }

  @Input()
  protected selectedTab: number;

  get parameterFormArray(): FormArray {
    return this.parametersForm.get('parameters') as FormArray;
  }

  set parameterFormArray(formArray: FormArray) {
    this.parametersForm.setControl('parameters', formArray);
  }


  constructor(public adminService: AdminService, private formBuilder: FormBuilder, public dialog: MatDialog) {
    this.initForm();
  }

  ngOnInit() {
  }

  private initForm() {
    this.parametersForm = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      parameters: this.formBuilder.array([])
    });
  }

  addParameter(pos?: number) {
    let newOrdinal = 1;
    const parameter = this.fillOneParameter(new Parameter(), pos + newOrdinal);
    // renumbering ordinals
    if (pos < this.parameterFormArray.length) {
      const newFormArray = this.formBuilder.array([]);
      let i = 0;
      while (i < this.parameterFormArray.length) {
        if (i == pos) {
          newFormArray.push(parameter);
          newOrdinal++;
        }
        newFormArray.push(
          this.fillOneParameter(this.parametersForm.value['parameters'][i], newOrdinal));
        i++;
        newOrdinal++;
      }
      this.parametersForm.setControl('parameters', newFormArray);
    } else {
      this.parameterFormArray.push(parameter);
    }
  }

  deleteParameter(pos: number) {
    const dialogRef = this.dialog.open(ParameterDeleteConfirmComponent, {
      width: '30%',
      data: null
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        if (!!this.parametersForm.value['parameters'][pos]['idParameter']) {
          this.adminService.deleteParameter(this.parametersForm.value['parameters'][pos]['idParameter'])
            .subscribe(() => {
              this.parameterFormArray.removeAt(pos);
            });
        } else {
          this.parameterFormArray.removeAt(pos);
        }
      }
    });
  }

  fillParameters() {
    if (!this.parameters) {
      return;
    }
    this.initForm();
    console.log('init');
    this.parameters.forEach(param => {
      this.parameterFormArray.push(
        this.fillOneParameter(param)
      );
    });
  }

  fillOneParameter(parameter?: Parameter, ordinal?: number): FormGroup {
    return this.formBuilder.group({
      idParameter: [parameter.idParameter || ''],
      parameterName: [parameter.parameterName || '', Validators.required],
      parameterTypeId: [parameter.parameterTypeId || '', Validators.required],
      parameterOrdinal: ordinal || parameter.parameterOrdinal
    });
  }

  onSubmit() {
    console.warn(this.parameterFormArray.value as Parameter[]);
    console.log(this.createParameterList());
    this.adminService.saveOrUpdateParameterList(this.createParameterList()).subscribe(() => {
        this.loadParametersForCurrentCategory();
      }
    );


  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (!!this.adminService.element && this.adminService.currentTab == 1) {
      this.loadParametersForCurrentCategory();
    }
  }

  loadParametersForCurrentCategory() {
    this.adminService.getParametersForCurrentCategory().subscribe(parameters => {
      this.parameters = parameters;
      this.fillParameters();
      console.log(this.parameterFormArray);
    });
  }

  createParameterList(): Parameter[] {
    const paramsList: Parameter[] = [];
    (this.parameterFormArray.value as Parameter[]).forEach(
      parameter => {
        let param = new Parameter();
        param.idParameter = parameter.idParameter;
        param.parameterName = parameter.parameterName;
        param.parameterTypeId = parameter.parameterTypeId;
        param.categoryId = this.currentCategory.idCategory;
        console.log(param.categoryId);
        param.parameterOrdinal = parameter.parameterOrdinal;
        paramsList.push(param);
      });
    return paramsList;
  }
}





