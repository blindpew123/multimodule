import {Component, OnInit } from '@angular/core';
import {AppRoutingModule} from '../app-routing.module';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']})

export class MenuComponent implements OnInit {
  show: boolean;
  menuElements: Map<string, string>;
  public constructor() {
    this.menuElements = new Map<string, string>();
  }

  toggleCollapse() {
    this.show = !this.show;
  }

  ngOnInit() {
    this.show = false;
    for ( const element of AppRoutingModule.getRoutings()) {
      if (element.data) {
        this.menuElements.set(element.path, element.data.title);
      }
    }
  }

  getKeys(): string[] {
    return Array.from(this.menuElements.keys());
  }

  isLoggedIn():string{
    return sessionStorage.getItem("admin");
  }

}
