import { Injectable } from '@angular/core';
import {CategoryService} from './category.service';
import {ParameterService} from './parameter.service';
import {Category} from './Data/category';
import {CategoryBase} from './category-base';
import {Observable, of, pipe} from 'rxjs';
import {Parameter} from './Data/parameter';
import {ParameterType} from './Data/parameter-type';
import {DeleteCategoryDto} from './Data/delete-category-dto';
import {ShopItemService} from './shop-item.service';
import {ShopItemSaveDto} from './Data/shop-item-save-dto';
import {JsIshopService} from './js-ishop-service';
import {catchError, map, tap} from 'rxjs/operators';
import {CategoryContentSummaryDto} from './Data/category-content-summary-dto';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  spin: boolean;
  private _currentTab: number = 0;
  public get currentTab(): number{
    return this._currentTab;
  }
  public set currentTab(currentTab: number){
    this._currentTab = currentTab;
  }

  private _categoryChanged: boolean;

  get isCategoryChanged(): boolean {
    return this._categoryChanged;
  }

  private _topCategories: Category[];
  get topCategories(): Category[]{
    return this._topCategories;
  }
  set topCategories(topCategories: Category[]){
    this._topCategories = topCategories;
  }

  private _element: CategoryBase; // selected Category Element
  get element(): CategoryBase {
    return this._element;
  }
  set element(categoryElement: CategoryBase){
    this._categoryChanged = !(this._element === categoryElement);
    this._element = categoryElement;
  }

  private categoryPalceholders: Category[][];

  // From Parameter Service
  private _paramterTypes: ParameterType[];
  get parameterTypes(): ParameterType[] {
    return this._paramterTypes;
  }
  set parameterTypes(parameterTypes: ParameterType[]) {
    this._paramterTypes = parameterTypes;
  }


  constructor(private categoryService: CategoryService,
              private parameterService: ParameterService,
              private shopItemService: ShopItemService) {
    this.categoryPalceholders = [];


  }

  getCategories(){
    this.spin = true;
    this.categoryService.getCategories().subscribe(categories => {
      this.spin = false;
      this.topCategories = this.getTopLevel(categories)
    });
  }
  //ParameterType request to Controller
  public getParameterTypes(){
    this.spin = true;
    this.parameterService.getParameterTypes().subscribe(parameterTypes=>{
      this.spin = false;
      this.parameterTypes = parameterTypes;
    });
  }

  //Creates new placeholder's category or gets it from cache.

  getNewCategory(parent: number, ordinal: number): Category {
    if(!this.categoryPalceholders[+parent]){
      this.categoryPalceholders[+parent] = [];
    }
    if(!this.categoryPalceholders[+parent][+ordinal]){
      this.categoryPalceholders[+parent][+ordinal] = new Category();
      this.categoryPalceholders[+parent][+ordinal].categoryNumber = ordinal;
      this.categoryPalceholders[+parent][+ordinal].parentId = parent;

    }
    return this.categoryPalceholders[+parent][+ordinal];
  }

  // All Categories of top level (parent==null)
  getTopLevel(categories: Category[]):Category[]{
    const result: Category[] =[];
    let pos = 0;
    for (let i = 0; i < categories.length; i++){
      if(categories[i].parentId === 0){
        result[pos++] = categories[i];
      }
    }
    return result;
  }

  getChildren(idCategory: number): Category[]{
    const result: Category[] =[];
    let pos = 0;
    for (let i = 0; i < this.categoryService.categories.length; i++){
      if(this.categoryService.categories[i].parentId === idCategory){
        result[pos++] = this.categoryService.categories[i];
      }
    }
    return result;
  }

  saveOrUpdateCategory(category: Category): Observable<Category>{
    this.spin = true;
    return this.categoryService.saveOrUpdateCategory(category).pipe(map(cat=> {
      this.spin = false;
      return cat;
    }));


  }

  getParametersForCurrentCategory(): Observable<Parameter[]> {
    if(!!this.element && this.element.getCategory().idCategory > 0) {
      this.spin = true;
      return this.parameterService.getParametersForCategory(this.element.getCategory())
        .pipe(map(params=>{
          this.spin = false;
          return params;
        }));
    }
  }

  deleteCategory(deleteCategoryDto: DeleteCategoryDto): Observable<Response>{
    this.spin = true;
    return this.categoryService.deleteCategory(deleteCategoryDto)
      .pipe(map(dto=>{
        this.spin = false;
        return dto;
      }));
  }

  checkCategory(id: number):Observable<CategoryContentSummaryDto>{
    this.spin = true;
    return this.categoryService.checkCategory(id).pipe(map(dto=>{
      this.spin = false;
      return dto;
    }));
  }
  resetCurrentCategoryElement(){
    this.element = null;
  }

  //Parameters -----------------------------------------------------------

  saveOrUpdateParameterList(parameters: Parameter[]): Observable<Response> {
    this.spin = true;
    return this.parameterService.saveOrUpdateParameterList(parameters)
      .pipe(map(params=>{
        this.spin = false;
        return params;
      }));
  }

  deleteParameter(id: number):Observable<Response> {
    this.spin = true;
    return this.parameterService.deleteParameter(id).pipe(map(response =>{
      this.spin = false;
      return response;
    }));
  }

  // ShopItems ------------------------------------------------------------

  getShopItems(){
    this.spin = true;
    return this.shopItemService.getShopItems(this.element.getCategory())
      .pipe(map(items=>{
        this.spin = false;
        return items;
      }));
  }

  saveShopItem(shopItemSaveDto: ShopItemSaveDto): Observable<Response>{
    this.spin = true;
    return this.shopItemService.saveOrUpdateShopItem(shopItemSaveDto)
      .pipe(map (dto=>{
        this.spin = false;
        return dto;
      }));
  }

  saveImage(formData:FormData):Observable<Response>{
    this.spin = true;
    return this.shopItemService.saveImage(formData)
      .pipe(map(resp=>{
        this.spin = false;
        return resp;
      }));
  }

}
