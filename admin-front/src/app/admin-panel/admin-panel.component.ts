import { Component, OnInit } from '@angular/core';
import {AdminService} from '../admin.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {MatTabChangeEvent} from '@angular/material';
import {Category} from '../Data/category';
import {CategoryBase} from '../category-base';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  navigationSubscription;

  constructor(public adminService: AdminService,
              private route: ActivatedRoute,
              private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // Uses for reload page after every click on the link. (by default Angular doesn't do it)
      // If it is a NavigationEnd event re-initialise the component.
      if (e instanceof NavigationEnd) {
        this.adminService.getCategories();
      }
    });

  }

  ngOnInit() {
    this.adminService.currentTab = 0;
  }

  ngOnDestroy(): void {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  onChangeTab($event: MatTabChangeEvent) {
    this.adminService.currentTab = $event.index;
    this.adminService.resetCurrentCategoryElement();
    if(this.adminService.currentTab == 1){
      this.adminService.getParameterTypes();
    }
  }



}
