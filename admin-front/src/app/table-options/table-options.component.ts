import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-table-options',
  templateUrl: './table-options.component.html',
  styleUrls: ['./table-options.component.css']
})
export class TableOptionsComponent implements OnInit {

  columns;

  constructor(
    public dialogRef: MatDialogRef<TableOptionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {}) {
      this.columns = Object.getOwnPropertyNames(data);
  }

  ngOnInit() {
  }

  onChangeVisible($event){
    this.data[$event.source.id] = $event.source.checked;
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}
