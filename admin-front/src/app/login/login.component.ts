import { Component, OnInit } from '@angular/core';
import {AdminService} from '../admin.service';
import {FormControl, FormGroup} from '@angular/forms';
import {AccountService} from '../account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private adminService: AdminService,
              private accountService: AccountService,
              private router: Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      login: new FormControl(''),
      password: new FormControl(''),
    });
  }

  onSubmit(){
    this.accountService.checkUser(
      this.loginForm.value['login'], this.loginForm.value['password'])
      .subscribe((result) =>{
        if(!!result){
          this.router.navigate(['welcome']);
        }
      });
  }

  isLoggedIn():boolean{
    console.log(sessionStorage.getItem("admin"))
    return !!sessionStorage.getItem("admin");
  }

  logOut(){
    sessionStorage.removeItem("admin");
  }

}
