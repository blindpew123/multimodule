import {Component, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {Category} from '../Data/category';
import {CategoryService} from '../category.service';
import {CategoryBase} from '../category-base';
import {AdminService} from '../admin.service';

const SELECTED_CLASS = "placeholder-selected";
const UNSELECTED_CLASS = "placeholder";

@Component({
  selector: 'app-category-place',
  templateUrl: './category-place.component.html',
  styleUrls: ['./category-place.component.css']
})
export class CategoryPlaceComponent implements OnInit, OnChanges, CategoryBase {

  @Input() @Output()
  leftMargin: number = 0;

  @Input() newOrdinal: number;
  private _category: Category;
  @Input()
  set category(category: Category){
    this._category = category;
  }
  get category(): Category{
    return this._category;
  }
  @Input()
  protected selectedElement: CategoryBase;


  public class: string = SELECTED_CLASS;
  constructor(private adminService: AdminService) { }

  ngOnInit() {}

  toggleSelected(){
    if(this.adminService.element === this) {
      return;
    }
    this.adminService.element = this;
  }

  getCategory(): Category {
    return this.category;
  }

  setCategory(category: Category) {
    this.category = category;
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    if(this.adminService.element === this) {
      this.class = SELECTED_CLASS;
    } else {
      this.class = UNSELECTED_CLASS;
    }
  }

}
