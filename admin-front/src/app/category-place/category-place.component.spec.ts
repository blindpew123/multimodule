import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPlaceComponent } from './category-place.component';

describe('CategoryPlaceComponent', () => {
  let component: CategoryPlaceComponent;
  let fixture: ComponentFixture<CategoryPlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryPlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
