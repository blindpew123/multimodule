import {OrderItemDto} from './order-item-dto';

export class OrderDto {
  idOrder: number;
  customerFirstName: string;
  customerLastName: string;
  address: string;
  paymentType: string;
  shipping: string;
  orderStatus: number;
  orderDate: string;
  orderItems: OrderItemDto[];
}
