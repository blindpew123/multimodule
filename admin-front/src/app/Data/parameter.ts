export class Parameter {
  idParameter: number;
  parameterName: string;
  parameterTypeId: number;
  categoryId: number;
  parameterOrdinal: number;
}
