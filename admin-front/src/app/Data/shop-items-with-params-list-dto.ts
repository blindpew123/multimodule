export class ShopItemsWithParamsListDto {
  shopItemsList: object[];
  columnNames: string[];
  columnTypes: string[];
}
