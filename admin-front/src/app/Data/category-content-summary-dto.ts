export class CategoryContentSummaryDto {
  childrenCount: number;
  shopItemsCount: number;
}
