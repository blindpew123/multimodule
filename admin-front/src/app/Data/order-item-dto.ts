export class OrderItemDto {
  shopItem: string;
  qty:number;
  itemPrice:number;
}
