import {ShopItemDto} from './shop-item-dto';

export class ShopItemSaveDto {
  shopItem: ShopItemDto;
  paramValues: string[];
}
