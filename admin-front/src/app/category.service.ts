import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatBottomSheet} from '@angular/material';

import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Category} from './Data/category';
import {JsIshopService} from './js-ishop-service';
import {CategoryElementComponent} from './category-element/category-element.component';
import {CategoryPlaceComponent} from './category-place/category-place.component';
import {CategoryBase} from './category-base';
import {DeleteCategoryDto} from './Data/delete-category-dto';
import {CategoryContentSummaryDto} from './Data/category-content-summary-dto';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends JsIshopService {

  public _categories: Category[];
  get categories(): Category[] {
    return this._categories;
  }

  set categories(categories: Category[]) {
    this._categories = categories;
  }


  private _categoryPlace: CategoryPlaceComponent;
  get categoryPlace(): CategoryPlaceComponent {
    return this._categoryPlace;
  }

  set categoryPlace(categoryPlace: CategoryPlaceComponent) {
    this._categoryPlace = categoryPlace;
  }


  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  // оставить здесь
  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.jsIshopUrl + '/categoriesManagement', this.getHttpOptions()) //JsIshopService.httpOptions)
      .pipe(tap(categories => {
          console.log(categories);
          this.handleSuccess('Categories received');
          this.categories = categories;
        }),
        catchError(this.handleError('Categories cannot be loaded', [])));
  }

  // оставить здесь
  // TODO: JavaDoc
  // TODO: Отправлять DTO
  // TODO: Анализировать Ответ
  saveOrUpdateCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(this.jsIshopUrl + `/saveOrUpdateCategory`,
      JSON.stringify(category), this.getHttpOptions())//JsIshopService.httpOptions).
      .pipe(
        tap((savedOrUpdatedCategory: Category) => {
          // console.log(`Category saved`);
          this.handleSuccess('Category saved');
        }),
        catchError(this.handleError<Category>('Category cannot be saved'))
      );
  }

  // оставить здесь
  // TODO: JavaDoc
  deleteCategory(deleteCategoryDto: DeleteCategoryDto): Observable<Response> {
    return this.http.post<Response>(this.jsIshopUrl + `/deleteCategory`,
      JSON.stringify(deleteCategoryDto), this.getHttpOptions())//JsIshopService.httpOptions).
      .pipe(
        tap(() => {
          console.log(`Category deleted`);
          this.handleSuccess('Category deleted');
        }),
        catchError(this.handleError<Response>('Category cannot be deleted', Response.error()))
      );
  }

  checkCategory(id: number): Observable<CategoryContentSummaryDto>{
    return this.http.get<CategoryContentSummaryDto>(this.jsIshopUrl + `/category/${id}/check-empty`, this.getHttpOptions()) //JsIshopService.httpOptions)
      .pipe(tap(categoryContentSummaryDto => {
          console.log(categoryContentSummaryDto);
          this.handleSuccess('Information about children received');
        }),
        catchError(this.handleError('Cannot get info about children of selected category: ', null)));
  }


}

