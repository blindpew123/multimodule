import {Component, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {Category} from '../Data/category';
import {CategoryBase} from '../category-base';
import {DeleteCategoryDto} from '../Data/delete-category-dto';
import {Router} from '@angular/router';
import {CategoriesComponent} from '../categories/categories.component';
import {ParameterService} from '../parameter.service';
import {AdminService} from '../admin.service';
import {TableOptionsComponent} from '../table-options/table-options.component';
import {MatDialog} from '@angular/material';
import {CategoryDeleteConfirmComponent} from '../category-delete-confirm/category-delete-confirm.component';

const SELECTED_CLASS = "bg-danger h-25";
const UNSELECTED_CLASS = "gray h-25 border";

@Component({
  selector: 'app-category-element',
  templateUrl: './category-element.component.html',
  styleUrls: ['./category-element.component.css']
})
export class CategoryElementComponent implements OnInit, OnChanges, CategoryBase {

  public icon = 'keyboard_arrow_right';
  public class = SELECTED_CLASS;
  isChecked: boolean = false;

  private _children: Category[];
  get children(): Category[]{
    return this._children;
  }
  @Input()
  set children(children: Category[]){
    this._children = children;
  }

  @Input() @Output()
  leftMargin: number = 0;

  private _category: Category;
  @Input()
  set category(category: Category){
    this._category = category;
  }
  get category(): Category{
    return this._category;
  }



  @Input()
  expanded: boolean = false;

  constructor(protected adminService: AdminService,
              private router: Router, public dialog: MatDialog) {}

  ngOnInit() {}

  toggleExpanded(){
    this.expanded = !this.expanded;
    this.icon = this.expanded ? 'keyboard_arrow_down' : 'keyboard_arrow_right'
  }

  toggleSelected(){
    if(this.adminService.element === this) {
      return;
    }
    this.adminService.element = this;
  }



  getCategory(): Category {
    return this.category;
  }
  setCategory(category: Category) {
    this.category = category;
  }

  removeCategory(){
    const deleteCategoryDto = new DeleteCategoryDto();
    deleteCategoryDto.idCategory = this.category.idCategory;
    this.adminService.deleteCategory(deleteCategoryDto).subscribe( response => {
      if (!response) {
        this.router.navigate(['categoriesAndItemsManagement']);
      }
    });
  }

  categoryCheckBeforeDelete(){
    this.adminService.checkCategory(this.category.idCategory).subscribe(categoryCheckDto=>{
      if(!!categoryCheckDto){
        const dialogRef = this.dialog.open(CategoryDeleteConfirmComponent, {
          width: '250px',
          data: categoryCheckDto
        });
        dialogRef.afterClosed().subscribe(result => {
          if(!!result){
            this.removeCategory();
          }
        });
      }
    });
  }

  OnChange($event){
    // console.log($event);
    this.isChecked = $event.isChecked;
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    if(this.adminService.element === this) {
      this.class = SELECTED_CLASS;
    } else {
      this.class = UNSELECTED_CLASS;
    }
  }

  getExpandedStyle() {
    return {'margin-top.px': -10,
      'z-index' : 9999,
    };
  }

}
