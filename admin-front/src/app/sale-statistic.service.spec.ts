import { TestBed, inject } from '@angular/core/testing';

import { SaleStatisticService } from './sale-statistic.service';

describe('SaleStatisticService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaleStatisticService]
    });
  });

  it('should be created', inject([SaleStatisticService], (service: SaleStatisticService) => {
    expect(service).toBeTruthy();
  }));
});
