import { TestBed, inject } from '@angular/core/testing';

import { ShopItemService } from './shop-item.service';

describe('ShopItemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopItemService]
    });
  });

  it('should be created', inject([ShopItemService], (service: ShopItemService) => {
    expect(service).toBeTruthy();
  }));
});
