import { Injectable } from '@angular/core';
import {JsIshopService} from './js-ishop-service';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {MatBottomSheet} from '@angular/material';
import {Category} from './Data/category';
import {Observable} from 'rxjs';
import {Parameter} from './Data/parameter';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaleStatisticService extends JsIshopService{

  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  getTop10ItemsQty(): Observable<string[][]> {
    return this.http.get<string[][]>(this.jsIshopUrl + `/stats/top10ItemsQty`, this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(top10items => {
          console.log(top10items);
          this.handleSuccess('Top 10 shop items loaded');
          // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Error loading top 10 shop items`, [])));
  }

  getTop10Customers(): Observable<string[][]> {
    return this.http.get<string[][]>(this.jsIshopUrl + `/stats/top10CustomerTotal`, this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(top10customer => {
          console.log(top10customer);
          this.handleSuccess('Top 10 customers loaded');
          // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Error loading top 10 customers`, [])));
  }

  getTotalForPeriod(dateStart: string, dateEnd: string): Observable<number> {
    return this.http.get<number>(this.jsIshopUrl + `/stats/periodTotal/${dateStart}/${dateEnd}`, this.getHttpOptions())//JsIshopService.httpOptions)
      .pipe(tap(result=> {
          console.log(result);
          this.handleSuccess('Total for period loaded');
          // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Error loading total for period`, 0)));
  }
}
