import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {CategoriesComponent} from './categories/categories.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {OrdersManagementComponent} from './orders-management/orders-management.component';
import {StatsComponent} from './stats/stats.component';
import {LoginComponent} from './login/login.component';

const appRoutes: Routes = [
  {
    path: 'admin-front/categoriesAndItemsManagement',
    component: AdminPanelComponent,
    runGuardsAndResolvers: 'always',
    data: { title: 'Categories and Shop Items'}
  },
  {
    path: 'admin-front/ordersManagement',
    component: OrdersManagementComponent,
 //   runGuardsAndResolvers: 'always',
    data: { title: 'Orders'}
  },
  {
    path: 'admin-front/stats',
    component: StatsComponent,
     runGuardsAndResolvers: 'always',
    data: { title: 'Stats'}
  },
  {
    path: 'admin-front/login',
    component: LoginComponent,
    runGuardsAndResolvers: 'always'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload'})
  ],
  declarations: []
})
export class AppRoutingModule {
  public static getRoutings(): Routes {
    return appRoutes;
  }
}
