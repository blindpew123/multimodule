package tk.blindpew123.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tk.blindpew123.dto.AdvShopItemDto;
import tk.blindpew123.model.ShopItemList;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;

@Singleton
@Startup
public class ShopRestClient {

    private static final Logger logger = LogManager.getLogger(ShopRestClient.class.getName());

    private static final String TOP10_URL = "http://localhost:8080/admin/shop-items/adv-top10";

    @Inject
    private ShopItemList list;

    @PostConstruct
    public void makeStartRequest()  {
        logger.info("connect....");
        getTopItemsList();
    }

    public ShopItemList getTopItemsList(){
        try {
            System.out.println("Started");
            URL url = new URL(TOP10_URL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            setupConnParameters(con);
            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.forName("UTF-8")));

            Jsonb jsonb = JsonbBuilder.create();
            list.clear();
            list.addAll(jsonb.fromJson(reader, new ArrayList<AdvShopItemDto>(){}.getClass().getGenericSuperclass()));
            logger.debug(list);
            logger.info(list);
            con.disconnect();
        } catch (IOException e){
            System.out.println("not connected");
        }

        return list;
    }

    private HttpURLConnection setupConnParameters(HttpURLConnection connection) throws ProtocolException {
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");
        String name = "0";
        String password = "0";
        String authString = name + ":" + password;
        String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());
        connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
        return connection;
    }

    public ShopItemList getList() {
        return list;
    }
}
