package tk.blindpew123.service;

import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(name = "ShopAdvMDB", activationConfig = {

        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),

        @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/adv"),

        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })

public class ShopAdvMDB implements MessageListener {

    @Inject
    private ShopRestClient restClient;

    @Inject
    private AdvChannelService advService;


    public void onMessage(Message rcvMessage) {
       restClient.getTopItemsList();
       advService.sendMessage();
    }
}