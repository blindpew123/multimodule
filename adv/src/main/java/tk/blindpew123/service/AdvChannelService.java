package tk.blindpew123.service;

import tk.blindpew123.dto.AdvShopItemDto;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class AdvChannelService implements Serializable {

    private static final Logger LOG = Logger.getLogger(AdvChannelService.class.getName());
    private static final String UPD_MESSAGE = "update";

    @Inject
    @Push
    PushContext helloChannel;

    @Inject
    ShopRestClient restClient;

    public void sendMessage() {
        LOG.log(Level.INFO, "send push message");
        helloChannel.send(UPD_MESSAGE);
    }
}
