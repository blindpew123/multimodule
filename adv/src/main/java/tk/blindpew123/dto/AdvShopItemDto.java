package tk.blindpew123.dto;

public class AdvShopItemDto {

    private long idShopItem;
    private double price;
    private String itemName = "";
    private String imageLink = "";

    public long getIdShopItem() {
        return idShopItem;
    }

    public void setIdShopItem(long idShopItem) {
        this.idShopItem = idShopItem;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
