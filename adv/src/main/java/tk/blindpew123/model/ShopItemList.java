package tk.blindpew123.model;

import tk.blindpew123.dto.AdvShopItemDto;
import tk.blindpew123.service.AdvChannelService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;


@Named
@ApplicationScoped
public class ShopItemList extends ArrayList<AdvShopItemDto> {

}
