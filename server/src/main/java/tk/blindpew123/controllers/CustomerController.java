package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tk.blindpew123.services.*;
import tk.blindpew123.services.dto.CartListDto;
import tk.blindpew123.services.dto.OrdersHistoryDto;
import tk.blindpew123.services.dto.UserProfileEditDto;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.validators.UserProfileDtoValidator;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Contains methods for processing operation related with information about Customer.
 */
@Controller
@SessionAttributes(names = {"userProfileEditDto"})
public class CustomerController {

    // various path to views
    private static final String PATH_TO_USER_INFO = "WEB-INF/jsp/views/userInformation.jsp";
    private static final String PATH_TO_ORDER_HISTORY = "WEB-INF/jsp/views/orderHistory.jsp";
    private static final String PATH_TO_PROFILE_CHANGED = "WEB-INF/jsp/views/changeProfileSuccess.jsp";
    private static final String INDEX = "/indexpage.jsp";

    private static final Logger logger = LogManager.getLogger(CustomerController.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private PageService pageService;

    @Autowired
    private UserProfileDtoValidator userProfileDtoValidator;

    /**
     * Sets validator into WebDataBinder instance
     * @param binder WebDataBinder instance
     */
    @InitBinder("userProfileEditDto")
    protected void initBinder(final WebDataBinder binder) {
        binder.setValidator(userProfileDtoValidator);
    }

    /**
     * Creates model attribute with information about Customer
     * @param principal current user
     * @return UserProfileEditDto with Customer profile
     * @see UserProfileEditDto
     */
    @ModelAttribute("userProfileEditDto")
    public UserProfileEditDto getUserProfileEditDto(final Principal principal) {
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        return customerService.getDtoWithCustomerProfile(userProfileEditDto,
                 principal == null ? null : userService.findByEmailAsLogin(principal.getName()));
    }

      /**
     * Setup model attributes for view that displays Customer profile
     *
     * @param userProfileEditDto dto that will be filled info about Customer
     * @param bindingResult will be contains validation result
     * @param model Spring Model instance
     * @param principal Current user
     * @param request HttpRequest
     * @return String with name of view (always INDEX).
       * A real page will be created by this jsp using templates.
     */

    @GetMapping("/customer/showUserInformation")
    public String getUserInformationForm(final @ModelAttribute("userProfileEditDto")
                                                 UserProfileEditDto userProfileEditDto,
                                         final BindingResult bindingResult,
                                         final Model model,
                                         final Principal principal,
                                         final HttpServletRequest request) {
        setCartToModel(model, request);
        customerService.getDtoWithCustomerProfile(userProfileEditDto,
                userService.findByEmailAsLogin(principal.getName()));
        userProfileDtoValidator.validate(userProfileEditDto, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("org.springframework.validation.BindingResult.userProfileEditDto", bindingResult);
            if (!userProfileEditDto.getEmptyAddress().checkEmpty()) {
                model.addAttribute("addressMessage",
                        "Новый адрес " + userProfileEditDto.getEmptyAddress() + " содержит ошибки");
            }
        }
        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Customer profile", PATH_TO_USER_INFO, null, principal));
        return INDEX;
    }

    /**
     *  Uses as target of redirecting (as part of PRG pattern) when Customer makes some changes in his profile
     *  Checks information about Customer with helps of instance of UserProfileValidator
     *  and redirects to view with success message or redirects back to form for fixing incorrect filled fields
     *
     * @param userProfileEditDto dto with Customer profile data
     * @param bindingResult uses for validation userProfileEditDto
     * @param addressMessage will be used for info or error message in view
     * @param model Spring Model instance
     * @param sessionStatus Spring SessionStatus instance
     * @param principal current user
     * @param request HttpRequest
     * @return String with name of view (always INDEX).
     *     A real page will be created by this jsp using templates.

     */

    @GetMapping("/customer/processUserInformation")
    public String getUserInformationFormDelAddress(final @ModelAttribute("userProfileEditDto")
                                                           UserProfileEditDto userProfileEditDto,
                                                   final BindingResult bindingResult,
                                                   final @ModelAttribute Object addressMessage,
                                                   final Model model,
                                                   final SessionStatus sessionStatus,
                                                   final Principal principal,
                                                   final HttpServletRequest request) {
        userProfileDtoValidator.validate(userProfileEditDto, bindingResult);
        setCartToModel(model, request);
        if (bindingResult.hasErrors()) {
            model.addAttribute("pageElementDescriptor",
                    pageService.getCurrentPageDescriptor(
                            "Customer profile", PATH_TO_USER_INFO, null, principal));

            model.addAttribute("org.springframework.validation.BindingResult.userProfileEditDto", bindingResult);
            if (addressMessage instanceof String) {
                model.addAttribute("addressMessage", addressMessage);
            } else if (!userProfileEditDto.getEmptyAddress().checkEmpty()) {
                model.addAttribute("addressMessage",
                        "Новый адрес " + userProfileEditDto.getEmptyAddress() + " содержит ошибки");
            }
        } else {
            if (userProfileEditDto.getAddresses().size() == 0){
                model.addAttribute("noAddress", true);
            }
            model.addAttribute("pageElementDescriptor",
                    pageService.getCurrentPageDescriptor(
                            "Customer profile has been changed", PATH_TO_PROFILE_CHANGED, null, principal));
            sessionStatus.setComplete();
        }
        return INDEX;
    }

    /**
     * Receives request when user wants to delete one of his addresses,
     * performs deleting and makes redirect to the next method
     *
     * @param delId ID of the address to be deleted
     * @param userProfileEditDto dto with Customer profile data
     * @param redirectAttribute RedirectAttribute instance for placing results of deleting address
     * @return String with name of redirected path
     */

    @PostMapping(value = "/customer/processUserInformation", params = "delId") // delete address by index in list
    public String postUserInformationFormDelAddress(final @RequestParam int delId,
                                                    final @ModelAttribute("userProfileEditDto")
                                                            UserProfileEditDto userProfileEditDto,
                                                    final RedirectAttributes redirectAttribute) {


        redirectAttribute.addFlashAttribute("addressMessage",
                customerService.removeAddress(userProfileEditDto.getAddresses(), delId));
        return "redirect:/customer/processUserInformation";
    }

    /**
     * Receives request when user wants to set up new primary address
     * @param primaryId id of new primary address
     * @param userProfileEditDto dto with Customer profile data
     * @param redirectAttribute RedirectAttribute instance for placing results of setting new primary address
     * @return String with name of redirected path
     */

    @PostMapping(value =  "/customer/processUserInformation", params = "primaryId")
    // make rimary address by index in list
    public String postUserInformationFormSetPrimaryAddress(final @RequestParam int primaryId,
                                                           final @ModelAttribute("userProfileEditDto")
                                                                   UserProfileEditDto userProfileEditDto,
                                                           final RedirectAttributes redirectAttribute) {
        customerService.clearPrimary(userProfileEditDto.getAddresses());
        redirectAttribute.addFlashAttribute("addressMessage",
                customerService.setPrimary(userProfileEditDto.getAddresses().get(primaryId)));
        return "redirect:/customer/processUserInformation";
    }

    /**
     *  Receives request when user wants to update his profile
     * @param userProfileEditDto dto with Customer profile data
     * @param bindingResult uses for validation userProfileEditDto
     * @param redirectAttribute RedirectAttribute instance for placing results of creating new address
     * @return String with name of redirected path
     */
    @PostMapping(value = "/customer/processUserInformation", params = "save") // add new address
    public String postUserInformationFormAddNewAddress(
            final @ModelAttribute("userProfileEditDto") UserProfileEditDto userProfileEditDto,
            final BindingResult bindingResult,
            final RedirectAttributes redirectAttribute) {

        userProfileDtoValidator.validate(userProfileEditDto, bindingResult);
        if (!bindingResult.hasErrors()) { // will be successfully redirected by next method
            if (!userProfileEditDto.getEmptyAddress().checkEmpty()) {
                // address filled, ok(no err), has to be added
                userProfileEditDto.getAddresses().add(userProfileEditDto.getEmptyAddress());
                customerService.saveOrUpdateAddress(userProfileEditDto.getEmptyAddress(),
                        userProfileEditDto.getIdUser());
                redirectAttribute.addFlashAttribute("addressMessage",
                        "Адрес " + userProfileEditDto.getEmptyAddress() + " добавлен");
                userProfileEditDto.setEmptyAddress(new Address());
            }

            if (customerService.saveOrUpdateCustomerProfile(userProfileEditDto)) {
                return "redirect:/logout"; //user changes his login, need re-enter;
            }
        } else if (!userProfileEditDto.getEmptyAddress().checkEmpty()) {
            redirectAttribute.addFlashAttribute("addressMessage",
                    "Новый адрес " + userProfileEditDto.getEmptyAddress() + " содержит ошибки");
        }

        return "redirect:/customer/processUserInformation";
    }

    /**
     * Creates models for view that performs displaying history of customer's orders
     *
     * @param ordersHistoryDto dto with infrmation about all orders of current customer
     * @param model Spring model instance
     * @param principal current user
     * @param request HttpRequest
     * @return String with name of view (always INDEX).
     *     A real page will be created by this jsp using templates.
     */

    @GetMapping("customer/orderHistory")
    public String showOrderHistory(@ModelAttribute("ordersHistoryDto") final OrdersHistoryDto ordersHistoryDto,
                                   final Model model,
                                   final Principal principal,
                                   HttpServletRequest request) {
        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Order's history", PATH_TO_ORDER_HISTORY, null, principal));
        orderService.getOrdersHistoryDto(ordersHistoryDto, principal);
        setCartToModel(model, request);
        model.addAttribute("orderHistoryDto", ordersHistoryDto);
        return INDEX;
    }

    // TODO: to service
    private void setCartToModel(final Model model, final HttpServletRequest request) {
        model.addAttribute("cart", ((CartListDto) request.getSession().getAttribute("cart")));
    }
}
