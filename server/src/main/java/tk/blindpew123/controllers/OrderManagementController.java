package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.OrderService;
import tk.blindpew123.services.dto.OrderAdminDto;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.OrderStatus;
import tk.blindpew123.services.dto.OrderStatusUpdateDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Rest Controller provides API for retrieving some statistical data about orders.
 */

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class OrderManagementController {

    private static final Logger logger = LogManager.getLogger(OrderManagementController.class.getName());

    @Autowired
    private OrderService orderService;

    /**
     * Returns List of dto that contains information about orders
     * @param dateStart orders with date greater or equal than dateStart
     * @param dateEnd orders with date less or equal than dateEnd
     * @return List of OrderAdminDto
     * @see OrderAdminDto
     */
    @GetMapping("orders/{dateStart}/{dateEnd}")
    public List<OrderAdminDto> getOrders(final @PathVariable String dateStart,
                                         final @PathVariable String dateEnd) {
        LocalDateTime d1 = LocalDateTime.parse(dateStart);
        LocalDateTime d2 = LocalDateTime.parse(dateEnd);
        return orderService.getOrderAdminDtoList(d1, d2);
    }

    /**
     * Returns all allowed order statuses
     * @return List of OrderStatus
     * @see OrderStatus
     */

    @GetMapping("/orders/orderStatuses")
    public List<OrderStatus> getOrderStatuses(){
        return orderService.getAllOrderStatuses();
    }

    /**
     * Handles changes in order status
     * @param orderStatusUpdateDto dto with inframtion about order and its new status
     * @return ResponseEntity with result(OK).
     */

    @PostMapping("/orders/updateOrderStatus")
    public ResponseEntity udaterderStatus(final @RequestBody OrderStatusUpdateDto orderStatusUpdateDto){
        orderService.orderStatusUpdate(orderStatusUpdateDto);
        return new ResponseEntity(HttpStatus.OK);
    }
}
