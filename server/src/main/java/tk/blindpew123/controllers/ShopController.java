package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.*;
import tk.blindpew123.services.ParameterService;
import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.ShopItem;
import tk.blindpew123.services.validators.CartValidator;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;


/**
 * The controller contains methods for performing common store operations.
 */

@Controller
//@SessionAttributes(names = {"cart"})
public class ShopController {

    private static final String PATH_TO_LIST_CATEGORY_ITEMS = "WEB-INF/jsp/views/shopItemsList.jsp";
    private static final String PATH_TO_ITEM_INFO = "WEB-INF/jsp/views/shopItemInfo.jsp";
    private static final String PATH_TO_START_PAGE = "WEB-INF/jsp/views/news.jsp";
    private static final String INDEX = "/indexpage.jsp";
    private static final int PAGE_SIZE = 12;


    private static final Logger logger = LogManager.getLogger(ShopController.class.getName());
    @Autowired
    private ShopItemService shopItemService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ParameterService parameterService;
    @Autowired
    private CartService cartService;
    @Autowired
    private PageService pageService;

    @Autowired
    private CartValidator cartValidator;

    /**
     * SetUp shopping cart validator
     *
     * @param binder Spring WebDataBinder
     */
    @InitBinder("cart")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(cartValidator);
    }

    /**
     * Returns the shopping cart. New, if there is no cart dto , or returns existing ones
     *
     * @param request HttpServlert request
     * @return CartListDto
     */

    @ModelAttribute("cart")
    public CartListDto getCartList(final HttpServletRequest request) {
        if (request.getSession().getAttribute("cart") == null) {
            logger.debug("New cart created");
            return new CartListDto();
        }
        logger.debug("I have one, use old");
        return (CartListDto) request.getSession().getAttribute("cart");
    }


    /**
     * Handles requests to main page
     *
     * @param cartListDto shopping cart dto
     * @param request     HttpServletRequest
     * @param model       Spring model
     * @param principal   current user
     * @return String with name of view (always INDEX).
     * A real page will be created by this jsp using templates.
     */
    @GetMapping(value = "/index")
    public String getIndex(final @ModelAttribute("cart") CartListDto cartListDto,
                           final HttpServletRequest request,
                           final Model model,
                           final Principal principal) {

        cartService.addResultPreviousCartCheckingToModel(request, model);
        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "MTG shop", PATH_TO_START_PAGE, null,
                        principal));
        return INDEX;
    }

    /**
     * Returns info about ShopItem with selected id
     *
     * @param idShopItem id of ShopItem that will be shown
     * @param model      Spring model
     * @param request    HttpServlet request
     * @param principal  current user
     * @return String with name of view (always INDEX).
     * A real page will be created by this jsp using templates.
     */
    @GetMapping(value = "/shop/shopItemInfo/{id}")
    public String getItemInfo(final @PathVariable("id") String idShopItem,
                              // final @ModelAttribute("cart") CartListDto cartListDto,
                              final Model model,
                              final HttpServletRequest request,
                              final Principal principal) {
        CartListDto cartListDto = (CartListDto) request.getSession().getAttribute("cart");
        model.addAttribute("cart", cartListDto);
        logger.info("get info");
       /* PageElementDescriptor pageElementDescriptor = new PageElementDescriptor();
        pageElementDescriptor.setContentURI(PATH_TO_ITEM_INFO); */
        SingleShopItemTupleDto shopItemTupleDto =
                shopItemService.getSingleShopItemInfo(Long.parseLong(idShopItem));
        //  pageElementDescriptor.setTitle(shopItemTupleDto.getShopItem().getItemName());

        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        shopItemTupleDto.getShopItem().getItemName(),
                        PATH_TO_ITEM_INFO,
                        shopItemTupleDto.getShopItem().getCategory().getIdCategory(),
                        principal));


        model.addAttribute("shopItemTupleDto", shopItemTupleDto);
        //    model.addAttribute("pageElementDescriptor", pageElementDescriptor);
        CartItemDto cartItemDto = new CartItemDto();

        cartService.addResultPreviousCartCheckingToModel(request, model);

        cartItemDto.setIdShopItem(shopItemTupleDto.getShopItem().getIdShopItem());
        cartItemDto.setPrice(shopItemTupleDto.getShopItem().getPrice());
        model.addAttribute("cartItemDto", cartItemDto);
        return INDEX;
    }


    /**
     * Handles request related to adding item to shopping cart and makes redirect to path that returns
     * the same page (info about item)
     *
     * @param idShopItem    id of ShopItem
     * @param cartItemDto   dto with info about ShopItem being added (id, qty etc.)
     * @param cartListDto   dto that represents Shopping Cart
     * @param bindingResult Spring BindingResult is used to future validation
     * @param principal     current User
     * @param request       HttpServletRequest
     * @return redirect path
     */

    @PostMapping(value = "/shop/shopItemInfo/{id}")
    public String postItemInfo(final @PathVariable("id") String idShopItem,
                               final @ModelAttribute("cartItemDto") CartItemDto cartItemDto,
                               final @ModelAttribute("cart") CartListDto cartListDto,
                               final BindingResult bindingResult,
                               final Principal principal,
                               final HttpServletRequest request) {


        cartService.addItemFromDtoToCart(cartItemDto, cartListDto, principal);
        cartValidator.validate(cartListDto, bindingResult);
        request.getSession().setAttribute("cartFormBinding", bindingResult);
        request.getSession().setAttribute("cart", cartListDto);
        return "redirect:/shop/shopItemInfo/" + idShopItem;
    }

    /**
     * Handles request relating to changes qty made by user in shopping cart
     *
     * @param cartListDto dto that represents Shopping Cart
     * @param cartBind    Spring BindingResult is used to cart validation
     * @param request     HttpServletRequest
     * @param principal   current User
     * @return redirect path (back to the same page)
     */
    @PostMapping(value = "/shop/cartProcess", params = "save")
    public String processCart(
            final @RequestParam String save,
            final @ModelAttribute("cart") CartListDto cartListDto,
            final BindingResult cartBind,
            final HttpServletRequest request,
            final Principal principal) {
        cartValidator.validate(cartListDto, cartBind);
        cartService.mergeCart(cartListDto, principal);
        request.getSession().setAttribute("cartFormBinding", cartBind);
        request.getSession().setAttribute("cart", cartListDto);
        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    /**
     * Handles request related to deleting item from shopping cart
     *
     * @param delId         id of item being deleted
     * @param cartListDto   dto that represents Shopping Cart
     * @param bindingResult Spring BindingResult is used to cart validation
     * @param request
     * @return redirect path (back to the same page)
     */
    @PostMapping(value = "/shop/cartProcess", params = "delId")
    public String delSingleItem(final @RequestParam long delId,
                                final @ModelAttribute("cart") CartListDto cartListDto,
                                final BindingResult bindingResult,
                                final HttpServletRequest request) {
        cartService.deletePositionFromCart(cartListDto, delId);
        cartValidator.validate(cartListDto, bindingResult);
        request.getSession().setAttribute("cart", cartListDto);
        request.getSession().setAttribute("cartFormBinding", bindingResult);
        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    /**
     * Handles request related to display all items from selected category starting from selected position
     *
     * @param idCategory  id of selected Category
     * @param start       start position
     * @param formDto     dto with selection parameters
     * @param cartListDto dto that represents shopping cart
     * @param model       Spring model
     * @param principal   current user
     * @return String with name of view (always INDEX).
     * A real page will be created by this jsp using templates.
     */

    @GetMapping(value = "/shop/list/{id}/{start}")
    public String getItemsForCategory(final @PathVariable("id") String idCategory,
                                      final @PathVariable("start") String start,
                                      final @ModelAttribute("formDto") FilterFormDto formDto,
                                      final @ModelAttribute("cart") CartListDto cartListDto,
                                      final Model model,
                                      final HttpServletRequest request,
                                      final Principal principal) {
        PageListParamsDto pageListParamsDto = new PageListParamsDto();
        pageListParamsDto.setSize(PAGE_SIZE);
        pageListParamsDto.setStart(Integer.parseInt(start));

        PaginationDto paginationDto = shopItemService.getPaginationDto(pageListParamsDto);
        paginationDto.setCurrentCategory(Long.parseLong(idCategory));

        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        categoryService.getCategoryNameById(Long.parseLong(idCategory)),
                        PATH_TO_LIST_CATEGORY_ITEMS,
                        Long.parseLong(idCategory), principal));
        List<String> aliasesForShopItemObj = ShopItem.getFieldAliases();
        if (formDto.getFilterList().size() == 0) {
            model.addAttribute("formDto", buildFilterForm(Long.parseLong(idCategory))); //for selection
            model.addAttribute("shopItemsObjArrayList",
                    shopItemService.getShopItemsForCategoryAsObjArray(Long.parseLong(idCategory), pageListParamsDto)); //list
            aliasesForShopItemObj.addAll(formDto.getAllNonEmptyAliases());
            paginationDto.setMaxPages(shopItemService.getCountShopItemsForCategory(Long.parseLong(idCategory)) / PAGE_SIZE);
        } else {
            FilterFormDto newFormDto = buildFilterForm(Long.parseLong(idCategory));
            newFormDto.setValues(formDto.getValues());
            newFormDto.setConditions(formDto.getConditions());
            newFormDto.setSortName(formDto.getSortName());
            model.addAttribute("formDto", newFormDto); //for selection
            //  aliasesForShopItemObj.addAll(newFormDto.getAllNonEmptyAliases());
            aliasesForShopItemObj = new ArrayList<>(newFormDto.getAllNonEmptyAliases());
            model.addAttribute("shopItemsObjArrayList",
                    shopItemService.getShopItemsSortedAndFiltered(
                            Long.parseLong(idCategory), newFormDto, pageListParamsDto)); //list
            paginationDto.setMaxPages(shopItemService.getCountShopItemsWithParamsForCategory(
                    Long.parseLong(idCategory), newFormDto) / PAGE_SIZE);
            paginationDto.setParams(request.getQueryString());
        }
        model.addAttribute("aliasesForShopItemObj", aliasesForShopItemObj);
        SortFormDto sortFormDto = new SortFormDto();
        sortFormDto.setSortNamesList(parameterService.getFullFieldAliasesList(Long.parseLong(idCategory)));
        model.addAttribute("currentCategoryId", idCategory);
        cartService.addResultPreviousCartCheckingToModel(request, model);
        model.addAttribute("paginationDto", paginationDto);
        return INDEX;
    }

    // TODO may be to service layer ?

    private FilterFormDto buildFilterForm(final long id) {
        FilterFormDto formDto = new FilterFormDto();
        List<String> fieldAliasesList = parameterService.getFullFieldAliasesList(id);
        List<String> fieldTypesList = parameterService.getFullFieldTypesList(id);
        //  formDto.setFilterList(new ArrayList<>());
        for (int i = 0; i < fieldAliasesList.size(); i++) {
            FilterCriteriaDto filterCriteriaDto = new FilterCriteriaDto();
            filterCriteriaDto.setFieldType(fieldTypesList.get(i));
            filterCriteriaDto.setFieldAlias(fieldAliasesList.get(i));
            // only ShopItem's fields have unique name. All ParameterValue fields have the same name - value.
            // So I'll use synthetic name pN
            filterCriteriaDto.setFieldName(
                    i >= ShopItem.getFieldNames().size() ?
                            "p" + i : ShopItem.getFieldNames().get(i));
            formDto.getFilterList().add(filterCriteriaDto);
        }
        return formDto;
    }
}
