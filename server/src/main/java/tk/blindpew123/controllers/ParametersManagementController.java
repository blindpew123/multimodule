package tk.blindpew123.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.ParameterService;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.ParameterType;

import java.util.List;


/**
 *  REST Controller provides API for Parameters' management.
 *  For using these methods user must have Admin's privileges
 */

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class ParametersManagementController {

    @Autowired
    private ParameterService parameterService;

    /**
     * Returns List of dto that contains info about parameters for the category
     * @param id id of category for which parameter list is requested
     * @return List<ParameterDto>
     * @see ParameterDto
     */

    @GetMapping("getParametersForCategory/{id}")
    public List<ParameterDto> getParametersForCategory(final @PathVariable String id){
        return parameterService.getParametersForCategoryAsDto(Long.parseLong(id));
    }

    /**
     * Returns List of dto that contains all types of parameters.
     * Current version has only two types: number and string
     * @return List<ParameterType>
     */
    @GetMapping("getParameterTypes")
    public List<ParameterType> getParameterType(){
        return parameterService.getAllParameterTypes();
    }

    /**
     * Handles a request related to creating a new or updating an existing parameter.
     * @param parameterDtoList list of all parameters for category with changes
     * @return ResponseEntity with ok status
     */
    @PostMapping("saveOrUpdateParameterDtoList")
    public ResponseEntity saveOrUpdateParameterDtoList(final @RequestBody List<ParameterDto> parameterDtoList){
        parameterService.saveOrUpdateParameterDtoList(parameterDtoList);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Handles a request related to delete parameter
     * @param id id of parameter that have to be deleted
     * @return ResponseEntity with ok status
     */
    @PostMapping("parameter/{id}/delete")
    public ResponseEntity deleteParameter(final @PathVariable String id){
        parameterService.deleteParameter(Long.parseLong(id));
        return new ResponseEntity(HttpStatus.OK);
    }

}
