package tk.blindpew123.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import tk.blindpew123.services.FileService;

import javax.servlet.http.HttpServletRequest;

/**
 * Contains method for uploading image file.
 */

@Controller
@CrossOrigin
@RequestMapping("/admin")
public class FileUploadController {

    @Autowired
    private FileService fileService;

    /**
     * Performs receiving and saving file image
     * @param request MultipartHttpServletRequest
     * @return ResponseBody with result of file saving
     */
    @RequestMapping(path = "/uploadImage")
    public @ResponseBody ResponseEntity saveFile(
            final MultipartHttpServletRequest request){
        request.getFileNames().forEachRemaining(
                fileName -> {
                    fileService.saveFileService(
                            request.getFile(fileName), request);
                });

        return new ResponseEntity(HttpStatus.OK);
    }





}
