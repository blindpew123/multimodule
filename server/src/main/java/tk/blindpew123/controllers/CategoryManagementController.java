package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.CategoryService;
import tk.blindpew123.services.UserService;
import tk.blindpew123.services.dao.CategoryDao;
import tk.blindpew123.services.dto.CategoryContentSummaryDto;
import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.dto.DeleteCategoryDto;
import tk.blindpew123.services.entities.Category;

import java.util.List;


/**
 *  REST Controller provides API for Categories' management.
 *  For using these methods user must have Admin's privileges
 */
@RestController
@CrossOrigin
@RequestMapping("/admin")
public class CategoryManagementController {

    private static final Logger logger = LogManager.getLogger(CategoryManagementController.class.getName());

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;


    /**
     * Returns List of dto that contain the necessary information for creating, renaming or deleting categories
     * @return List<CategoryDto>
     * @see CategoryDto
     */

    @GetMapping(value = "/categoriesManagement")
    public List<CategoryDto> categoriesManagement(){
        return categoryService.getAllCategoriesAsDtoList();
    }

    /**
     * Saves new Category or updates exiting
     *
     * @param categoryDto dto with inforation about Category
     * @return saved or updated Category
     * @see CategoryDto
     */

    @PostMapping(value = "/saveOrUpdateCategory")
    public Category saveOrUpdateCategory(final @RequestBody() CategoryDto categoryDto){
        return categoryService.saveOrUpdate(categoryDto);
    }



    /** Receives dto with information for delete Category
     * @param deleteCategoryDto dto that describes Category for deleting
     * @return ResponseEntity with result of deleting
     * @see DeleteCategoryDto
     */

    @PostMapping(value = "/deleteCategory")
    public ResponseEntity deleteCategory(final @RequestBody() DeleteCategoryDto deleteCategoryDto){
        logger.debug("delete: get Dto with data: id = "
                + deleteCategoryDto.getIdCategory());
        categoryService.deleteCategory(deleteCategoryDto);
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Method for checking user's credentials. If user is admin he will receives stastus code 200.
     * @return ResponseEntity with 200 (OK).
     */
    @GetMapping(value = "/checkUser")
    public ResponseEntity checkUser(){
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Returns dto with information about number of children Categories
     * and total of ShopItems in the selected Category and its children
     * @param id id of checking Category
     * @return CategoryContentSummaryDto with information
     * @see CategoryContentSummaryDto
     */
    @GetMapping("/category/{id}/check-empty")
    public CategoryContentSummaryDto countAllInsideCategory(final @PathVariable String id){
        return categoryService.getCategorySummaryDto(Long.parseLong(id));
    }


}
