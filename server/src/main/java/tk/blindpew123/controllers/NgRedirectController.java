package tk.blindpew123.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NgRedirectController {
    @RequestMapping({"/admin-front/","admin-front/categoriesAndItemsManagement",
            "/admin-front/ordersManagement","admin-front/stats", "admin-front/login" })
    public String redirectToNg(){
        return "forward:/admin-front/index.html";
    }
}
