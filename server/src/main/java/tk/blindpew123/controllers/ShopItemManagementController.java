package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import tk.blindpew123.services.ShopItemService;
import tk.blindpew123.services.dto.ShopItemSaveDto;
import tk.blindpew123.services.dto.ShopItemsColNamesColTypesTripletDto;

import javax.servlet.annotation.MultipartConfig;



/** REST Controller provides API for various operations with ShopItems.
 */
@RestController
@CrossOrigin
@RequestMapping("/admin")
public class ShopItemManagementController {

    private static final Logger logger = LogManager.getLogger(ShopItemManagementController.class.getName());

    @Autowired
    private ShopItemService shopItemService;


    /**
     * Provides dto that contains ShopItems and their parameters
     * @param id id of selected category
     * @return ShopItemsColNamesColTypesTripletDto with ShopItems and parameters
     * @see ShopItemsColNamesColTypesTripletDto
     */
    @GetMapping("/getShopItemsForCategory/{id}")
    public ShopItemsColNamesColTypesTripletDto getShopItemsForCategory (@PathVariable String id){
        return shopItemService.getShopItemsAndValuesAndNamesAndTypesAsDto(Long.parseLong(id));
    }

    /**
     * Handles request related to saving or updating ShopItem and its parameters
     * @param shopItemSaveDto dto that represents new or changed ShopItem
     * @return ResponseEntity with ok status
     */
    @PostMapping("/saveOrUpdateShopItem")
    public ResponseEntity saveOrUpdateShopItemDtoWithParam(final @RequestBody() ShopItemSaveDto shopItemSaveDto){
        shopItemService.saveOrUpdateShopItemDtoWithParam(shopItemSaveDto);
        return new ResponseEntity(HttpStatus.OK);
    }



}
