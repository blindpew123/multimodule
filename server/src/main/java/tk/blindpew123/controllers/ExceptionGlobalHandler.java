package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tk.blindpew123.exceptions.CategoryManagementException;
import tk.blindpew123.exceptions.NoSuchCustomerException;
import tk.blindpew123.exceptions.ParameterManagementException;
import tk.blindpew123.exceptions.ShopItemManagementException;

import javax.servlet.http.HttpServletRequest;


/**
 * Uses for catch some exception.
 *
 */
@ControllerAdvice
public class ExceptionGlobalHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LogManager.getLogger(ExceptionGlobalHandler.class.getName());

    /**
     * Catch NoSuchCustomerException - in this case performs redirect user to form with empty customer profile
     * @param e NoSuchCustomerException exception
     * @param redirectAttributes
     * @return string with path to mapped method which returns view that contains customer profile
     */
    @ExceptionHandler(NoSuchCustomerException.class)
    public String handleNoSuchCustomer(final NoSuchCustomerException e, final RedirectAttributes redirectAttributes) {
        logger.debug("For user: " + e.getUser().getEmailAsLogin() + "customer not found");
        redirectAttributes.addFlashAttribute("message", "Please, fill the form");
        return "redirect:/customer/showUserInformation";
    }


    @ExceptionHandler({CategoryManagementException.class,
            ParameterManagementException.class, ShopItemManagementException.class})
    protected ResponseEntity<Object> handleManagementException(
            RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
