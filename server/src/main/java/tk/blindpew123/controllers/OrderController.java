package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tk.blindpew123.services.CartService;
import tk.blindpew123.services.OrderService;
import tk.blindpew123.services.PageService;
import tk.blindpew123.services.dto.CartListDto;
import tk.blindpew123.services.dto.OrderProcessDto;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.validators.CartValidator;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


/**
 * Handles requests related to order processing
 */
@Controller
@SessionAttributes("cart")
public class OrderController {
    private static final Logger logger = LogManager.getLogger(OrderController.class.getName());

    private static final String PATH_TO_ORDER_FORM = "WEB-INF/jsp/views/processOrder.jsp";
    private static final String PATH_TO_ORDER_FAIL = "WEB-INF/jsp/views/orderFail.jsp";
    private static final String PATH_TO_ORDER_SUCCESS = "WEB-INF/jsp/views/orderSuccess.jsp";
    private static final String INDEX = "/indexpage.jsp";

    @Autowired
    private CartService cartService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private PageService pageService;
    @Autowired
    private CartValidator cartValidator;

    /**
     * Creates dto that represents shopping cart, or returns exiting shopping cart
     * @param request HttpServletRequest
     * @return CartListDto new or existing shopping cart
     */
   @ModelAttribute("cart")
   public CartListDto getCartList (HttpServletRequest request) {
       if (request.getSession().getAttribute("cart") == null) {
           logger.debug("New cart");
           return new CartListDto();
       }
       logger.debug("use old");
       return (CartListDto) request.getSession().getAttribute("cart");
   }

    /**
     *  It's target method for redirection in case problems with creating order.
     *  Creates model for view that will be used when order creation will fail
     *
     * @param cartListDto CartListDto shopping cart dto
     * @param bindingResult will be contains result of validation
     * @param model Spring model
     * @param principal current user
     * @return String with name of view (always INDEX).
     *     A real page will be created by this jsp using templates.
     */
   @GetMapping("/cart/orderFail")
   public String getOrderFail(final @ModelAttribute("cart") CartListDto cartListDto,
                              BindingResult bindingResult,
                              final Model model,
                              final Principal principal){
       cartValidator.validate(cartListDto, bindingResult);
       model.addAttribute("pageElementDescriptor",
               pageService.getCurrentPageDescriptor(
                       "Can't create your order", PATH_TO_ORDER_FAIL, null, principal));
       return INDEX;
   }

    /**
     *  It's target method for redirection in case successfully created order.
     *  Creates model for view that will be used when order created.
     *
     * @param orderId Id of new order
     * @param model Spring model
     * @param principal Current user
     * @return String with name of view (always INDEX).
     *     A real page will be created by this jsp using templates.
     */

    @GetMapping("/cart/orderSuccess")
    public String getOrderSuccess(final @ModelAttribute("orderId") long orderId,
                                  final Model model,
                                  final Principal principal){
        orderService.sendMessage();
        model.addAttribute("orderId", orderId);
        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Order created", PATH_TO_ORDER_SUCCESS, null, principal));
        return INDEX;
    }


    /**
     * Handles request related to creating new order
     * @param cartListDto shopping cart dto
     * @param orderProcessDto information for creating new order
     * @param bindingResult uses for validation orderProcessDto
     * @param attributes if successful, saves the new order identifier during the redirect
     * @param principal current user
     * @param request HttpServletRequest
     * @param sessionStatus Spring SessionStatus instance
     * @return String with path to redirect
     */

   @PostMapping("/cart/processOrder")
   public String makeOrder(final @ModelAttribute("cart") CartListDto cartListDto,

                           final @ModelAttribute("orderDto") OrderProcessDto orderProcessDto,
                           final BindingResult bindingResult,
                           final RedirectAttributes attributes,
                           final Principal principal,
                           final HttpServletRequest request,
                           final SessionStatus sessionStatus){

       orderProcessDto.setCartListDto((CartListDto) request.getSession().getAttribute("cart"));
       Order order = orderService.processOrder(orderProcessDto, bindingResult, principal);
       logger.info("!!! New Order " + (order == null ? "null" : order.getIdOrder()));
       if (order == null){
           // order creation fail

           return "redirect:/cart/orderFail";
       } else {
           ((CartListDto) request.getSession().getAttribute("cart")).getCartItems().clear();
           sessionStatus.setComplete();
           attributes.addFlashAttribute("orderId", order.getIdOrder());
       }
       return "redirect:/cart/orderSuccess";
   }

    /**
     * Target method for redirection request when customer wants to create new order.
     * Performs validation and prepares model for view with order form.
     *
     * @param cartListDto dto with shopping cart
     * @param cartBind uses for validation cartListDto
     * @param model Spring model
     * @param principal current user
     * @param request HttpServletRequest request
     * @return String with name of view (always INDEX).
     *  A real page will be created by this jsp using templates.
     */

    @GetMapping("/cart/readyToOrder")
    public String getPreparedOrderForm(@ModelAttribute("cart") CartListDto cartListDto,
                                       final BindingResult cartBind,
                                       final Model model,
                                       final Principal principal,
                                       final HttpServletRequest request){

      //artListDto =  (CartListDto) request.getSession().getAttribute("cart");
       model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Create order", PATH_TO_ORDER_FORM, null, principal));
        // Cart may has problems
        cartService.mergeCart(cartListDto, principal);
        setCartToModel(model, request);
        cartValidator.validate(cartListDto, cartBind);
        request.getSession().setAttribute("cart", cartListDto);
         if (cartBind.hasErrors()){
             // request.getSession().setAttribute("cartFormBinding", cartBind); ??????
                cartService.addResultPreviousCartCheckingToModel(request, model);
                model.addAttribute("cartError", true);
        }
        model.addAttribute("orderDto", orderService.getOrderProcessDto(cartListDto, principal));
        return INDEX;
    }

    /**
     * This method handles request to creating order only for authorized users
     * @return String with redirect to path to method that will create order form
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping(value = "/shop/cartProcess", params = "makeOrder")
    public String prepareOrderForm(final @RequestParam String makeOrder) {
       return "redirect:/cart/readyToOrder";
    }

    private void setCartToModel(final Model model, final HttpServletRequest request) {
        model.addAttribute("cart", ((CartListDto) request.getSession().getAttribute("cart")));
    }
}
