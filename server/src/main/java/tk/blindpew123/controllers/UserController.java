package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tk.blindpew123.services.*;
import tk.blindpew123.services.dto.CartListDto;
import tk.blindpew123.services.dto.UserRegisterDto;
import tk.blindpew123.services.validators.CartValidator;
import tk.blindpew123.services.validators.UserValidator;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


/**
 * Controller contains methods that handles requests related to authentication of users
 */
@Controller
@SessionAttributes("cart")
public class UserController {
    private static final Logger logger = LogManager.getLogger(UserController.class.getName());
    private static final String REGISTRATION = "/WEB-INF/jsp/views/registration_inner.jsp";
    private static final String SUCCESS_REG = "/WEB-INF/jsp/views/successReg.jsp";
    private static final String PATH_TO_LOGIN = "/WEB-INF/jsp/views/login_inner.jsp";
    private static final String PATH_TO_REG_FORM = "WEB-INF/jsp/views/orderSuccess.jsp";
    private static final String PATH_TO_START_PAGE = "WEB-INF/jsp/views/news.jsp";
    private static final String INDEX = "/indexpage.jsp";

    @Autowired
    private UserService userService;

    @Autowired
    private PageService pageService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private CartValidator cartValidator;

    @Autowired
    private CartService cartService;

    @ModelAttribute("cart")
    public CartListDto getCartList(final Principal principal, HttpServletRequest request) {
        if (request.getSession().getAttribute("cart") == null) {
            if (principal == null) {
                return new CartListDto();
            } else {
                logger.info("Cart for" + principal.getName());
                return new CartListDto();
            }
        } else return (CartListDto) request.getSession().getAttribute("cart");
    }

   /* @ModelAttribute("userForm")
    public UserRegisterDto getUserRegisterDto(){
        return new UserRegisterDto();
    }*/

    /**
     * Handles request related to registration new user
     *
     * @param cartListDto   shopping cart
     * @param userForm      new user's data
     * @param bindingResult Spring BindingResult is used to user's data validation
     * @param model         Spring model
     * @param principal     current user
     * @param request       HttpServlet request
     *                      return String with name of view (always INDEX).
     *                      A real page will be created by this jsp using templates.
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(final @ModelAttribute("cart") CartListDto cartListDto,
                               final @ModelAttribute("userForm") UserRegisterDto userForm,
                               final @ModelAttribute("bindingResult") Object bindingResult,
                               final Model model,
                               final Principal principal,
                               final HttpServletRequest request) {
        model.addAttribute("userForm", userForm);
        // setCartToModel(model, request);
        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Registration", "/WEB-INF/jsp/views/registration_inner.jsp", null, principal));
        if (bindingResult instanceof BindingResult) {
            if (((BindingResult) bindingResult).hasErrors()) {
                model.addAttribute("org.springframework.validation.BindingResult.userForm", bindingResult);
            }
        }
        return INDEX;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(final @ModelAttribute("userForm") UserRegisterDto userForm,
                               final BindingResult bindingResult,
                               Model model,
                               HttpServletRequest request,
                               final RedirectAttributes attributes) {
        userValidator.validate(userForm, bindingResult);
        setCartToModel(model, request);
        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/registration";
        } else {
            userService.saveUserWithUserRole(userForm);
            securityService.autologin(userForm.getUserName(), userForm.getPasswordConfirm());
        }
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(final Model model,
                        final String error,
                        final String logout,
                        final Principal principal,
                        final HttpServletRequest request) {

        logger.debug("Current user" + (principal == null ? null : principal.getName()));

        if (error != null)
            model.addAttribute("error", "Ваши имя и/или пароль содержат ошибки.");

        if (logout != null)
            model.addAttribute("message", "Вы вышли из аккаунта. До новых встреч!");

        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Log in", PATH_TO_LOGIN, null, principal));
        setCartToModel(model, request);
        return INDEX;
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(@ModelAttribute("cart") CartListDto cartListDto,
                          final BindingResult bindingResult,
                          final Model model,
                          final HttpServletRequest request,
                          final Principal principal) {
        //   cartListDto = (CartListDto) request.getSession().getAttribute("cart");

        if (cartListDto == null) {
            cartListDto = new CartListDto();
            logger.debug("Cart not found!!!!");
        }
        logger.debug(cartListDto.getCartItems().size());
        cartService.mergeCart(cartListDto, principal);
        cartValidator.validate(cartListDto, bindingResult);
        request.getSession().setAttribute("cartFormBinding", bindingResult);
        cartService.addResultPreviousCartCheckingToModel(request, model);
        // setCartToModel(model, request);
        //  if (request.getSession().getAttribute("cart") == null) {
        request.getSession().setAttribute("cart", cartListDto);
        //   }
        String referer = request.getHeader("Referer");
        if (referer != null && referer.toLowerCase().contains("registration")) {
            model.addAttribute("pageElementDescriptor",
                    pageService.getCurrentPageDescriptor(
                            "Registration success", SUCCESS_REG, null, principal));

        } else {
            logger.debug("/ process");
            model.addAttribute("pageElementDescriptor",
                    pageService.getCurrentPageDescriptor(
                            "MTG shop", PATH_TO_START_PAGE, null,
                            principal));
        }
        return INDEX;
    }


    private void setCartToModel(final Model model, final HttpServletRequest request) {
        model.addAttribute("cart", ((CartListDto) request.getSession().getAttribute("cart")));
    }
}