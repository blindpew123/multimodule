package tk.blindpew123.cfg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

/**
 * JMS Configuration. This implementation made for outer Wildfly's MQ.
 */

@Configuration
public class MessagingConfiguration {

    private String CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
    private String DESTINATION = "jms/queue/adv";


    /**
    * Creates Spring's Bean connectionFactory. Created instance of ConnectionFactory is wrapped in
     *  UserCredentialsConnectionFactoryAdapter for sending credentials information with messages
     *
     * @return created ConnectionFactory instance
     * @throws NamingException
     * */
    @Bean
    @Lazy
    public ConnectionFactory connectionFactory() throws NamingException {
        Context namingContext = null;
        // Set up the namingContext for the JNDI lookup
        final Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.naming.remote.client.InitialContextFactory");
        env.put(Context.PROVIDER_URL, "http-remoting://192.168.0.188:8100");
        env.put(Context.SECURITY_PRINCIPAL, "guest");
        env.put(Context.SECURITY_CREDENTIALS, "guest");
        namingContext = new InitialContext(env);
        QueueConnectionFactory connectionFactory = (QueueConnectionFactory) namingContext
                .lookup(CONNECTION_FACTORY);

        UserCredentialsConnectionFactoryAdapter resultFactory = new UserCredentialsConnectionFactoryAdapter();
        resultFactory.setTargetConnectionFactory(connectionFactory);
        resultFactory.setUsername("guest");
        resultFactory.setPassword("guest");
        return resultFactory;
    }

    /**
     * Creates JmsTemplate instance
     *
     * @return a JmsTemplate instance
     * @throws NamingException
     */
    @Bean
    @Lazy
    public JmsTemplate jmsTemplate() throws NamingException {
        Context namingContext = null;
        // Set up the namingContext for the JNDI lookup
        final Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.naming.remote.client.InitialContextFactory");
        env.put(Context.PROVIDER_URL, "http-remoting://192.168.0.188:8100");
        env.put(Context.SECURITY_PRINCIPAL, "guest");
        env.put(Context.SECURITY_CREDENTIALS, "guest");
        namingContext = new InitialContext(env);
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        template.setDefaultDestination((Destination) namingContext
                .lookup(DESTINATION));
        return template;
    }

}
