package tk.blindpew123.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.dao.UserDao;
import tk.blindpew123.services.dto.UserRegisterDto;
import tk.blindpew123.services.entities.Role;

import javax.transaction.Transactional;
import java.util.HashSet;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Transactional
    public void saveUserWithUserRole(final UserRegisterDto userRegisterDto) {
        tk.blindpew123.services.entities.User user = mapUserRegDtoToUser(userRegisterDto);
        user.setRoles(new HashSet<Role>(userDao.findUserRoleAsUser()));
        userDao.saveOrUpdate(user);
    }

    @Transactional
    public void saveUserWithAdminRole(final UserRegisterDto userRegisterDto) {
        tk.blindpew123.services.entities.User user = mapUserRegDtoToUser(userRegisterDto);
        user.setRoles(new HashSet<Role>(userDao.findUserRoleAsAdmin()));
        userDao.saveOrUpdate(user);
    }

    private tk.blindpew123.services.entities.User mapUserRegDtoToUser(final UserRegisterDto userRegisterDto){
        tk.blindpew123.services.entities.User user = new tk.blindpew123.services.entities.User();
        user.setEmailAsLogin(userRegisterDto.getUserName());
        user.setPassword(bCryptPasswordEncoder.encode(userRegisterDto.getPassword()));
        return user;
    }

    @Transactional
    public tk.blindpew123.services.entities.User findByEmailAsLogin(String emailAsUserName) {
        return userDao.findUserByLogin(emailAsUserName);
    }

    @Transactional
    public tk.blindpew123.services.entities.User findById(final long id) {
        return userDao.findById(id);
    }




}
