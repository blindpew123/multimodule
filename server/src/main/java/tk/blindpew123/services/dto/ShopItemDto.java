package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.ShopItem;

import java.io.Serializable;

public class ShopItemDto implements Serializable {
    private long idShopItem;
    private double price;
    private String itemName;
    private long idCategory;
    private long weight; // in grams
    private double volume;
    private int qnty;
    private String imageLink;

    public ShopItemDto(){

    };

    public ShopItemDto(long idShopItem,
                       double price,
                       String itemName,
                       long idCategory,
                       long weight,
                       double volume,
                       int qnty,
                       String imageLink){
        this.idShopItem = idShopItem;
        this.price = price;
        this.itemName = itemName;
        this.idCategory = idCategory;
        this.weight = weight;
        this.volume = volume;
        this.qnty = qnty;
        this.imageLink = imageLink;
    }

    public long getIdShopItem() {
        return idShopItem;
    }

    public void setIdShopItem(long idShopItem) {
        this.idShopItem = idShopItem;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public int getQnty() {
        return qnty;
    }

    public void setQnty(int qnty) {
        this.qnty = qnty;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
