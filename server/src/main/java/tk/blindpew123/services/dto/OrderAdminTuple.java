package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

public class OrderAdminTuple {
    private OrderAdminDto orderAdminDto;
    private List<OrderItemAdminDto> orderItemAdminDtoList;

    public List<OrderItemAdminDto> getOrderItemAdminDtoList() {
        return orderItemAdminDtoList;
    }

    public void setOrderItemAdminDtoList(List<OrderItemAdminDto> orderItemAdminDtoList) {
        this.orderItemAdminDtoList = orderItemAdminDtoList;
    }

    public OrderAdminDto getOrderAdminDto() {
        return orderAdminDto;
    }

    public void setOrderAdminDto(OrderAdminDto orderAdminDto) {
        this.orderAdminDto = orderAdminDto;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
