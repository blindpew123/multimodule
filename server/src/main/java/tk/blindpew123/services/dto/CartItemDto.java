package tk.blindpew123.services.dto;

import javax.validation.constraints.Min;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class CartItemDto {
    @Min(0)
    private int qnty;
    @Min(1)
    private long idShopItem;
    private long idCartItem;
    private double price;
    private String shopItemName;
    private String shopItemSmallImg;

    public int getQnty() {
        return qnty;
    }

    public void setQnty(int qnty) {
        this.qnty = qnty;
    }

    public long getIdShopItem() {
        return idShopItem;
    }

    public void setIdShopItem(long idShopItem) {
        this.idShopItem = idShopItem;
    }

    public String getShopItemName() {
        return shopItemName;
    }

    public void setShopItemName(String shopItemName) {
        this.shopItemName = shopItemName;
    }

    public String getShopItemSmallImg() {
        return shopItemSmallImg;
    }

    public void setShopItemSmallImg(String shopItemSmallImg) {
        this.shopItemSmallImg = shopItemSmallImg;
    }

    public long getIdCartItem() {
        return idCartItem;
    }

    public void setIdCartItem(long idCartItem) {
        this.idCartItem = idCartItem;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
