package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class UserRegisterDto {
    private long idUser;
    private String userName;
    private String password;
    private String passwordConfirm;


    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

}