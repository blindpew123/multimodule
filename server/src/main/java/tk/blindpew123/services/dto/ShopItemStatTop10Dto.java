package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class ShopItemStatTop10Dto {
    private String itemName;
    private long qty;

    public ShopItemStatTop10Dto(){}

    public ShopItemStatTop10Dto(String itemName, long qty) {
        this.itemName = itemName;
        this.qty = qty;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
