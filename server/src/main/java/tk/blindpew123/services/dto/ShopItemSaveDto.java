package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

public class ShopItemSaveDto {
    private ShopItemDto shopItem;
    private String[] paramValues;

    public ShopItemDto getShopItem() {
        return shopItem;
    }

    public void setShopItem(ShopItemDto shopItem) {
        this.shopItem = shopItem;
    }

    public String[] getParamValues() {
        return paramValues;
    }

    public void setParamValues(String[] paramValues) {
        this.paramValues = paramValues;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
