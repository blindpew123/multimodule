package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.*;

import java.util.List;

public class OrderHistoryDto {
    private long idOrder;
    private long idCustomer;
    private String customerFirstName;
    private String customerLastName;
    private Address shippingAddress;
    private Shipping shipping;
    private PaymentType payment;
    private OrderStatus orderStatus;
    private List<OrderItem> orderItems;

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
