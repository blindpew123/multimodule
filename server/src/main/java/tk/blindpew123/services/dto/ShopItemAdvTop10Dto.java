package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.LinkedHashMap;
import java.util.Map;


public class ShopItemAdvTop10Dto {

    private long idShopItem;
    private double price;
    private String itemName;
    private String imageLink;

    public long getIdShopItem() {
        return idShopItem;
    }

    public void setIdShopItem(long idShopItem) {
        this.idShopItem = idShopItem;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }


}
