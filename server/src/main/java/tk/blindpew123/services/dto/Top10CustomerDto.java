package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Top10CustomerDto {
    private long idCustomer;
    private String firstName;
    private String lastName;
    private double custTotal;

    public Top10CustomerDto(){}

    public Top10CustomerDto(long idCustomer, String firstName, String lastName, double custTotal) {
        this.idCustomer = idCustomer;
        this.firstName = firstName;
        this.lastName = lastName;
        this.custTotal = custTotal;
    }

    public long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getCustTotal() {
        return custTotal;
    }

    public void setCustTotal(double custTotal) {
        this.custTotal = custTotal;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

}
