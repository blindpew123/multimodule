package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Model that describes which jsp has needed include to indexpage.jsp template
 * for properly show full page to User.
 */

public class PageElementDescriptor {

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static class Breadcrumb{
        private String name;
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    private String contentURI;
    private String title;
    private List<Category> currentLevelMenu = new ArrayList<>();
    private List<Breadcrumb> breadCrumbs = new ArrayList<>();
    private String userName;

    /**
     * Default constructor for bean creatiion
     */
    public PageElementDescriptor() {}

    /**
     *
     * @param contentURI contains part of URI
     * @param title contains title of page
     */
    public PageElementDescriptor(final String contentURI, final String title) {
        this.contentURI = contentURI;
        this.title = title;
    }

    public String getContentURI() {
        return contentURI;
    }

    public void setContentURI(final String contentURI) {
        this.contentURI = contentURI;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public List<Category> getCurrentLevelMenu() {
        return currentLevelMenu;
    }

    public void setCurrentLevelMenu(List<Category> currentLevelMenu) {
        this.currentLevelMenu = currentLevelMenu;
    }

    public List<Breadcrumb> getBreadCrumbs() {
        return breadCrumbs;
    }

    public void setBreadCrumbs(List<Breadcrumb> breadCrumbs) {
        this.breadCrumbs = breadCrumbs;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
