package tk.blindpew123.services.dto;


import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.time.LocalDateTime;
import java.util.List;

public class OrderAdminDto {
    private long idOrder;
    private String customerFirstName;
    private String customerLastName;
    private String address;
    private String paymentType;
    private String shipping;
    private long orderStatus;
    private LocalDateTime orderDate;
    private List<OrderItemAdminDto> orderItems;

    public long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(long idOrder) {
        this.idOrder = idOrder;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public long getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(long orderStatus) {
        this.orderStatus = orderStatus;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public List<OrderItemAdminDto> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemAdminDto> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
