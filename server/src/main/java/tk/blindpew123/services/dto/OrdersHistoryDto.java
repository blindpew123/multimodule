package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.Order;

import java.util.List;

public class OrdersHistoryDto {
    private List<Order> orderList;

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
