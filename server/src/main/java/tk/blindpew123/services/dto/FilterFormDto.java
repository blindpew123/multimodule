package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FilterFormDto {

    private String sortName;

    private List<FilterCriteriaDto> filterList = new ArrayList<>();

    public List<FilterCriteriaDto> getFilterList() {
        return filterList;
    }


    public void setFilterList(List<FilterCriteriaDto> filterList) {
        this.filterList = filterList;
    }

    public String[] getValues(){
        String[] result = null;
        if (filterList != null) {
            result = new String[filterList.size()];
            for (int i = 0; i < filterList.size();i++){
                result[i] = filterList.get(i).getValue();
            }
        }
        return result;
    }

    public void setValues(final String[] values){
        if (values == null) {
            throw  new IllegalArgumentException(
                    "Values' array is null. Expected " + this.filterList.size());
        }
        if (values.length != this.filterList.size()){
            throw  new IllegalArgumentException(
                    "Values' array size different! " + values.length + "Expected " + this.filterList.size());
        }
        for (int i = 0; i < values.length; i++){
            this.filterList.get(i).setValue(values[i]);
        }
    }

    public String[] getAliases(){
        String[] result = null;
        if (filterList != null) {
            result = new String[filterList.size()];
            for (int i = 0; i < filterList.size();i++){
                result[i] = filterList.get(i).getFieldAlias();
            }
        }
        return result;
    }

    public void setConditions(final String[] conditions){
        if (conditions == null) {
            throw  new IllegalArgumentException(
                    "Conditions' array is null. Expected " + this.filterList.size());
        }
        if (conditions.length != this.filterList.size()){
            throw  new IllegalArgumentException(
                    "Operations' array size different! " + conditions.length + "Expected " + this.filterList.size());
        }
        for(int i = 0; i < conditions.length; i++){
            this.filterList.get(i).setMathCondition(conditions[i]);
        }
    }

    public String[] getConditions(){
        String[] conditions = new String[this.filterList.size()];
        for(int i = 0; i < this.filterList.size(); i++){
            conditions[i] = this.filterList.get(i).getMathCondition();
        }
        return conditions;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public int getSortNameNumberField(){
        int result = -1;
        if(filterList != null){
            for(int i = 0; i < filterList.size(); i++){
                if (filterList.get(i).getFieldAlias().equals(sortName)){
                   result = i;
                   break;
                }
            }
        }
        return result;
    }

    // adds all non-empty aliases or alias used for sorting
    public List<String> getAllNonEmptyAliases(){
        List<String> result = new ArrayList<>();
        if (filterList != null) {
            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getFieldAlias() != null) {
                    if (!filterList.get(i).getFieldAlias().equals("") || !filterList.get(i).getFieldAlias().equals(sortName)) {
                        result.add(filterList.get(i).getFieldAlias());
                    }
                }
            }
        }
        return result;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
