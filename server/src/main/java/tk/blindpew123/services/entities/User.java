package tk.blindpew123.services.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class User {

    private long user;
    private String emailAsLogin;
    private String password;
    private Set<Role> roles = new HashSet<>();
    private Set<CartItem> cartItems = new HashSet<>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getUser() {
        return user;
    }

    public void setUser(long idUser) {
        this.user = idUser;
    }

    @Column(name = "emailAsLogin")
    public String getEmailAsLogin() {
        return emailAsLogin;
    }

    public void setEmailAsLogin(String emailAsLogin) {
        this.emailAsLogin = emailAsLogin;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @OneToMany(mappedBy = "user")
    public Set<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Set<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("User: {")
                .append("user: ").append(user).append(", ")
                .append("emailAsLogin: ").append(emailAsLogin)
                .append("}").toString();
    }
}
