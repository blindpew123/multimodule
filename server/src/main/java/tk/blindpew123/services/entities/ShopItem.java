package tk.blindpew123.services.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import tk.blindpew123.services.dto.ShopItemAdvTop10Dto;
import tk.blindpew123.services.dto.ShopItemStatTop10Dto;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries({
        @NamedQuery(name = "ShopItem.findAllItemsForUpdateByIds",
                query = "SELECT  s FROM ShopItem s WHERE s.id IN (:ids)"),
        @NamedQuery(name = "ShopItem.top10ShopItemsByQty",
                query = "SELECT s.itemName, SUM(oi.qty) AS itemTotal FROM ShopItem s JOIN OrderItem oi " +
                        "ON s = oi.shopItem GROUP BY s.itemName ORDER BY itemTotal"),
        @NamedQuery(name = "ShopItem.top10AdvShopItemsByQty",
                query = "SELECT s.idShopItem as idShopItem,s.itemName as itemName, " +
                        "s.price as price, s.imageLink as imageLink FROM ShopItem s JOIN OrderItem oi " +
                        "ON s = oi.shopItem GROUP BY s.itemName ORDER BY SUM(oi.qty) DESC"),
        @NamedQuery(name = "ShopItem.countItemsInCategories",
                query = "SELECT COUNT(s.id) FROM ShopItem s WHERE s.category in(:categories)"),
        @NamedQuery(name = "ShopItem.countItemsInSingleCategory",
        query = "SELECT COUNT(s.id) FROM ShopItem s WHERE s.category = :category")

})
@SqlResultSetMapping(
        name = "ShopItem.top10ShopItemsByQty",
        classes = @ConstructorResult(
                targetClass = ShopItemStatTop10Dto.class,
                columns = {
                        @ColumnResult(name = "itemName"),
                        @ColumnResult(name = "itemTotal", type = Long.class)
                }
        )
)

@SqlResultSetMapping(
        name = "ShopItem.top10AdvShopItemsByQty",
        classes = @ConstructorResult(
                targetClass = ShopItemAdvTop10Dto.class,
                columns = {
                        @ColumnResult(name = "idShopItem"),
                        @ColumnResult(name = "price"),
                        @ColumnResult(name = "itemName"),
                        @ColumnResult(name = "imageLink")
                }
        )
)

public class ShopItem {

    private static final List<String> FIELD_NAMES = new ArrayList<>();
    static {
        FIELD_NAMES.add("idShopItem");
        FIELD_NAMES.add("category");
        FIELD_NAMES.add("itemName");
        FIELD_NAMES.add("price");
        FIELD_NAMES.add("weight");
        FIELD_NAMES.add("volume");
        FIELD_NAMES.add("qnty");
        FIELD_NAMES.add("imageLink");
    }
    private static final List<String> FIELD_ALIASES = new ArrayList<>();
    static {
        FIELD_ALIASES.add("id Товара");
        FIELD_ALIASES.add("id Категории");
        FIELD_ALIASES.add("Название");
        FIELD_ALIASES.add("Цена");
        FIELD_ALIASES.add("Вес, г");
        FIELD_ALIASES.add("Объем");
        FIELD_ALIASES.add("Количество");
        FIELD_ALIASES.add("Ссылка на изображение");
    }
    private static final List<String> FIELD_TYPES = new ArrayList<>();
    static {
        FIELD_TYPES.add(ParameterType.ParamType.NUMBER.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.NUMBER.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.STRING.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.NUMBER.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.NUMBER.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.NUMBER.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.NUMBER.getParamTypeName());
        FIELD_TYPES.add(ParameterType.ParamType.STRING.getParamTypeName());

    }

    private long idShopItem;
    private Category category;
    private String itemName;
    private double price;
    private long weight; // in grams
    private double volume;
    private int qnty;
    private String imageLink;

 //   private List<ParameterValue> valueList;

    public static List<String> getFieldNames(){return new ArrayList<>(FIELD_NAMES); }

    public static List<String> getFieldAliases() {
        return new ArrayList<>(FIELD_ALIASES);
    }

    public static List<String> getFieldTypes() {
        return new ArrayList<>(FIELD_TYPES);
    }



    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdShopItem() {
        return idShopItem;
    }

    public void setIdShopItem(long idShopItem) {
        this.idShopItem = idShopItem;
    }

    @ManyToOne
    @JoinColumn(name = "Category")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Column(name = "ItemName")
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Column(name = "Price")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Column(name = "Weight")
    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    @Column(name = "Volume")
    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    @Column(name = "Qnty")
    public int getQnty() {
        return qnty;
    }

    public void setQnty(int qnty) {
        this.qnty = qnty;
    }

    @Column(name = "ImageLink")
    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
/*

   @OneToMany(mappedBy = "shopItem")
   @JsonIgnore
   public List<ParameterValue> getValueList() {
        return valueList;
    }

    public void setValueList(List<ParameterValue> valueList) {
        this.valueList = valueList;
    } */

    @Override
    public String toString() {
        return "ShopItem{" +
                "idShopItem=" + getIdShopItem() +
                ", price=" + getPrice() +
                ", itemName='" + getItemName() + '\'' +
                ", category_id=" + (getCategory() == null ? null : getCategory().getIdCategory()) +
                ", weight=" + getWeight() +
                ", volume=" + getVolume() +
                ", qnty=" + getQnty() +
                ", imageLink='" + getImageLink() + '\'' +
                '}';
    }
}
