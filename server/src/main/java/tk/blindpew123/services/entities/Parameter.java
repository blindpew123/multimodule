package tk.blindpew123.services.entities;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries({
        @NamedQuery(name = "Parameter.findAllParamsWithNames",
                query = "SELECT DISTINCT pt FROM Parameter pt " +
                        "LEFT OUTER JOIN FETCH ParameterType pn " +
                        "ON pt.parameterType = pn ORDER BY pt.category.idCategory, pt.parameterOrdinal"),
        @NamedQuery(name = "Parameter.findAllParamNamesWithValuesOrdered",
        query =  "SELECT new tk.blindpew123.services.dto.ParamNameParamValueDto(p.parameterName, v.parameterValue)" +
                " FROM Parameter p LEFT OUTER JOIN FETCH ParameterValue v " +
                "ON p.idParameter = v.parameter.idParameter WHERE v.shopItem =:id ORDER BY p.parameterOrdinal")
})
public class Parameter {

    private long idParameter;
    private String parameterName;
    private ParameterType parameterType;
    private Category category;
    private int parameterOrdinal;
    private List<ParameterValue> parameterValues = new ArrayList<>();


    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdParameter() {
        return idParameter;
    }

    public void setIdParameter(long idParameter) {
        this.idParameter = idParameter;
    }
    @Column(name = "ParameterName")
    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @OneToMany(targetEntity = ParameterValue.class)
    @JoinColumn(name = "Parameter")
    public List<ParameterValue> getParameterValues() {
        return parameterValues;
    }

    public void setParameterValues(List<ParameterValue> parameterValues) {
        this.parameterValues = parameterValues;
    }

    @ManyToOne
    @JoinColumn(name = "ParameterType")
    public ParameterType getParameterType() {
        return parameterType;
    }

    public void setParameterType(ParameterType parameterType) {
        this.parameterType = parameterType;
    }

    @ManyToOne
    @JoinColumn(name = "Category")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Column(name = "ParameterOrdinal")
    public int getParameterOrdinal() {
        return parameterOrdinal;
    }

    public void setParameterOrdinal(int parameterOrdinal) {
        this.parameterOrdinal = parameterOrdinal;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("Parameter: {")
                .append("idParameterType: ").append(idParameter).append(", ")
                .append("parameterName: ").append(Objects.toString(parameterName)).append(", ")
                .append("parameterType: ").append(parameterType == null ? "null" : parameterType.getParameterTypeName()).append(", ")
                .append("category: ").append(category == null ? null : category.getCategoryName())
                .append("parameterOrdinal: ").append(parameterOrdinal)
                .append('}').toString();
    }
}
