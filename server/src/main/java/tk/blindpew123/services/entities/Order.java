package tk.blindpew123.services.entities;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries({
        @NamedQuery(name = "Order.findAllOrderByCustomer",
                query = "SELECT DISTINCT  o FROM Order o LEFT JOIN FETCH o.orderItemList WHERE o.customer = :c ORDER BY o.orderDate"),
        @NamedQuery(name = "Order.findAllBetweenDates",
                query = "SELECT DISTINCT  o FROM Order o LEFT JOIN FETCH o.orderItemList WHERE o.orderDate >= :d1 AND o.orderDate <= :d2 ORDER BY o.orderDate"),
        @NamedQuery(name = "Order.findOrderWithAddress",
                query = "SELECT o FROM Order o WHERE o.address = :a"),
        @NamedQuery(name = "Order.getPeriodTotal",
                query = "SELECT SUM(oi.qty * oi.itemPrice) as TOTAL FROM Order o " +
                        "LEFT JOIN OrderItem as oi ON" +
                        " o = oi.order WHERE o.orderDate >= :d1 AND o.orderDate <= :d2")
})
@NamedNativeQuery(name = "Order.getPeriodTotalNative",
        query = "SELECT SUM(orderitem.qty*orderitem.itemPrice) as TOTAL FROM customerorder o LEFT JOIN OrderItem on o.idOrder = orderitem.CustOrder" +
                " WHERE o.OrderDate >= :d1 AND o.orderDate <= :d2")

@Table(name = "customerorder")
public class Order {
    private long idOrder;
    private Customer customer;
    private Address address;
    private PaymentType paymentType;
    private Shipping shipping;
    private OrderStatus orderStatus;
    private LocalDateTime orderDate;
    private List<OrderItem> orderItemList = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(long idOrder) {
        this.idOrder = idOrder;
    }

    @ManyToOne
    @JoinColumn(name = "Customer")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne
    @JoinColumn(name = "Address")
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @ManyToOne
    @JoinColumn(name = "PaymentType")
    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @ManyToOne
    @JoinColumn(name = "Shipping")
    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    @ManyToOne
    @JoinColumn(name = "OrderStatus")
    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Column(name = "OrderDate")
    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    @OneToMany(mappedBy = "order")
    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("Order: {")
                .append("idOrder: ").append(idOrder).append(", ")
                .append("customer(id): ").append(customer == null ? "null" : customer.getIdCustomer()).append(", ")
                .append("address: ").append(address == null ? "null" : address.getIdAddress()).append(", ")
                .append("paymentType: ").append(paymentType == null ? "null" : paymentType.getPaymentName()).append(", ")
                .append("shipping: ").append(shipping == null ? "null" : shipping.getShippingName())
                .append("orderStatus: ").append(orderStatus == null ? "null" : orderStatus.getOrderStatusName())
                .append("orderDate: ").append(Objects.toString(orderDate))
                .append("}").toString();
    }
}
