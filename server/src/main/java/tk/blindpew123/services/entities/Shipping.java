package tk.blindpew123.services.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries(
        @NamedQuery(name = "Shipping.findAllShipping",
        query = "FROM Shipping s")
)
public class Shipping {

    public enum ShippingType{
        COURIER(1),
        RUSSIAN_POST(2);
        ShippingType(int value){
            this.value = value;
        }
        long value;
        public long getValue(){
            return this.value;
        }
    }


    private long idShipping;
    private String shippingName;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdShipping() {
        return idShipping;
    }

    public void setIdShipping(long idShipping) {
        this.idShipping = idShipping;
    }

    @Column(name = "ShippingName")
    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("Shipping: {")
                .append("idShipping: ").append(idShipping).append(", ")
                .append("shippingName").append(shippingName)
                .append("}").toString();
    }
}
