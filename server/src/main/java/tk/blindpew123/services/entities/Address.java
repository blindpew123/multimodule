package tk.blindpew123.services.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries(
        @NamedQuery(name = "Address.findAddressById",
        query = "FROM Address a WHERE a.id = :id"))
public class Address {
    private long idAddress;
    private String country;
    private String city;
    private String postCode;
    private String street;
    private String building;
    private String apt;
    private Customer customer;
    private boolean primaryAdr;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(long idAddress) {
        this.idAddress = idAddress;
    }
    @Column(name = "Country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    @Column(name = "City")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    @Column(name = "PostCode")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "Street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    @Column(name = "Building")
    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    @Column(name = "Apt")
    public String getApt() {
        return apt;
    }

    public void setApt(String apt) {
        this.apt = apt;
    }

    @ManyToOne
    @JoinColumn(name = "customer")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Column(name = "primaryadr")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isPrimaryAdr() {
        return primaryAdr;
    }

    public void setPrimaryAdr(boolean primary) {
        this.primaryAdr = primary;
    }

    public boolean checkEmpty(){
        return isEmpty(country) && isEmpty(city) && isEmpty(postCode) && isEmpty(street) && isEmpty(building);
    }

    private boolean isEmpty(String fieldValue){
        return fieldValue == null || fieldValue.isEmpty();
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("Country: ").append(Objects.toString(country)).append(", ")
                .append("City: ").append(Objects.toString(city)).append(", ")
                .append("Street: ").append(Objects.toString(street)).append(", ")
                .append("Building: ").append(Objects.toString(building)).append(", ")
                .append("Apt.: ").append(Objects.toString(apt)).toString();
    }
}
