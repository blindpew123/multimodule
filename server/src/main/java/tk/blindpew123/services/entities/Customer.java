package tk.blindpew123.services.entities;

import tk.blindpew123.services.dto.Top10CustomerDto;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries({
        @NamedQuery(name = "Customer.findCustomerByUser",
        query = "FROM Customer c WHERE c.user = :user ")
})
@NamedNativeQueries(
        @NamedNativeQuery(name = "Customer.Top10Customer",
        query = "Select c.idCustomer, c.firstName, c.lastName, SUM(oi.Qty*oi.ItemPrice) as custTotal FROM Customer c " +
                "JOIN orderItem oi WHERE oi.custOrder IN" +
                "(SELECT c2.idOrder FROM customerorder c2 WHERE c.idCustomer = c2.Customer) GROUP BY c.idCustomer " +
                "ORDER BY custTotal DESC")
)
@SqlResultSetMapping(
        name = "Customer.Top10Customer",
        classes = @ConstructorResult(
                targetClass = Top10CustomerDto.class,
                columns = {
                        @ColumnResult(name = "idCustomer", type = Long.class),
                        @ColumnResult(name = "firstName"),
                        @ColumnResult(name = "lastName"),
                        @ColumnResult(name = "custTotal", type = Double.class)
                }
        )
)
public class Customer {
    private long idCustomer;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private User user;
    private List<Address> addresses = new ArrayList<>();


    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(long idCustomer) {
        this.idCustomer = idCustomer;
    }

    @Column(name = "FirstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "LastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "BirthDay")
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "user")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(targetEntity = Address.class, mappedBy = "customer")
    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("Customer: {")
                .append("idCustomer: ").append(idCustomer).append(", ")
                .append("firstName: ").append(Objects.toString(firstName)).append(", ")
                .append("lastName: ").append(Objects.toString(lastName)).append(", ")
                .append("birthDate: ").append(Objects.toString(birthDate)).append(", ")
                .append("user: ").append(user == null? "null" : user.getUser())
                .append("}").toString();
    }
}
