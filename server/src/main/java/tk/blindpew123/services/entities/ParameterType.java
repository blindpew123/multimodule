package tk.blindpew123.services.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class ParameterType {

    public enum ParamType{
        NUMBER("Number"),
        STRING("String");
        ParamType(String paramTypeName){
            this.paramTypeName = paramTypeName;
        }
        String paramTypeName;
        public String getParamTypeName(){
            return this.paramTypeName;
        }
    }

    private long idParameterType;
    private String parameterTypeName;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdParameterType() {
        return idParameterType;
    }

    public void setIdParameterType(long idParameterType) {
        this.idParameterType = idParameterType;
    }

    @Column(name = "ParameterTypeName")
    public String getParameterTypeName() {
        return parameterTypeName;
    }

    public void setParameterTypeName(String parameterTypeName) {
        this.parameterTypeName = parameterTypeName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("ParameterType: {")
                .append("idParameterType: ").append(idParameterType).append(", ")
                .append("parameterTypeName: ").append(parameterTypeName)
                .append('}').toString();
    }
}
