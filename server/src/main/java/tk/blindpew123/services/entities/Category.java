package tk.blindpew123.services.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Contains name of trading items' category.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c"),
        @NamedQuery(name = "Category.incrementChildOrderField",
                query = "UPDATE Category c SET c.categoryNumber = c.categoryNumber + :i " +
                        "WHERE (((:p IS NULL AND c.parent IS NULL) OR c.parent = :p) AND c.categoryNumber >= :n)"),
        @NamedQuery(name  = "Category.findById", query = "SELECT c FROM Category c WHERE c.idCategory = :id  AND c.deleted != true"),
        @NamedQuery(name = "Category.deleteCategory", query = "DELETE FROM Category c WHERE c.idCategory = :id"),
        @NamedQuery(name = "Category.updateParentForClosestChildrenWithId",
                query = "UPDATE Category c SET c.parent = : np WHERE c.parent = :op"),
        @NamedQuery(name = "Category.updateParentForClosestChildrenWithNull",
                query = "UPDATE Category c SET c.parent = : np WHERE c.parent IS NULL"),
        @NamedQuery(name = "Category.findMaxCategoryOrdinalInRow",
                query = "SELECT max(c.categoryNumber) FROM Category c " +
                        "WHERE ((:p IS NULL AND c.parent IS NULL) OR c.parent = :p)"),
        @NamedQuery(name = "Category.findChildrenByParent",
                query = "SELECT c FROM Category c WHERE ((:p is NULL and c.parent IS NULL) OR c.parent = :p) AND c.deleted != true ORDER BY c.categoryNumber"),
        @NamedQuery(name = "Category.setDeletedForCategoryList",
        query = "UPDATE Category c SET c.deleted = true WHERE c IN(:list)")
        })
@Table(name = "Category")
public class Category {

    // It selects only categories that not have parents in table.
    // Uses subselect for creating tmp table with ids available
    public static final String SQL_DELETE_ORPHANS = "DELETE c.* FROM category c WHERE Parent NOT IN " +
            "(SELECT idCategory FROM (SELECT idCategory FROM category ) tmp)";

    private long idCategory;
    private String categoryName;
    private Category parent;
    private long categoryNumber;
    private boolean deleted;

    @Id
    @NotNull
    @GeneratedValue(strategy = IDENTITY)
    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    @Column(name = "CategoryName")
    @NotNull
    @Size(min = 1, max = 45)
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @ManyToOne
    @JoinColumn(name="Parent")
    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    @Column(name = "CategoryNumber")
    @Min(0)
    public long getCategoryNumber() {
        return categoryNumber;
    }

    public void setCategoryNumber(long categoryNumber) {
        this.categoryNumber = categoryNumber;
    }

    @Column(name = "deleted")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        return sb.append("Category: {")
                .append("idCategory: ").append(idCategory).append(", ")
                .append("categoryName: ").append(Objects.toString(categoryName)).append(", ")
                .append("parent(id): ").append(parent == null?"null":parent.idCategory).append(", ")
                .append("categoryNumber: ").append(categoryNumber)
                .append("}").toString();
    }

}
