package tk.blindpew123.services.dao;

import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.OrderItem;

import javax.transaction.Transactional;

/**
 * Implements OrderItemDao interface
 * @see OrderItemDao
 */
@Service
public class OrderItemDaoImpl extends GenericDaoImpl<OrderItem> implements OrderItemDao  {

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public void delete(final OrderItem orderItem) {
        sessionFactory.getCurrentSession().remove(orderItem);
    }
}
