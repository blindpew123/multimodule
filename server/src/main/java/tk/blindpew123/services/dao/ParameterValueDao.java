package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.Parameter;
import tk.blindpew123.services.entities.ParameterValue;
import tk.blindpew123.services.entities.ShopItem;

import java.util.List;

public interface ParameterValueDao extends GenericDao<ParameterValue> {    
    List<ParameterValue> addParameterValueList(List<ParameterValue> parameterValueList);
    List<ParameterValue> findAllParametersValuesForShopItem(ShopItem shopItem);
    void deleteAllValuesForParameter(Parameter parameter);
}
