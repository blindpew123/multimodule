package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Handles operations with Order Entity. User of the interface can perform any CRUD operations or findAll.
 * User can skip implementation delete and/or findAll methods. In this case if any of this methods will be invoked
 * UnupportedOperationException will be thrown
 * Also available:
 * findAllOrdersBetweenDates(LocalDateTime startDate, LocalDateTime endDate)
 * findAllOrderByCustomer(Customer customer)
 * findTotalForPeriod(LocalDateTime startDate, LocalDateTime endDate)
 * findOrdersWithAddress(Address address)
 */

public interface OrderDao extends GenericDao<Order>{

    /**
     * Returns all Orders was made between two dates
     * @param startDate start date
     * @param endDate end date
     * @return List<Order>
     */
    List<Order> findAllOrdersBetweenDates(LocalDateTime startDate, LocalDateTime endDate);

    /**
     * Returns all Orders made by selected Customer
     * @param customer selected Customer
     * @return List<Order>
     */
    List<Order> findAllOrderByCustomer(Customer customer);

    /**
     * Calculates the total amount of sales for the period
     * @param startDate start Date
     * @param endDate end Date
     * @return
     */
    Double findTotalForPeriod(LocalDateTime startDate, LocalDateTime endDate);

    /**
     *  Returns all orders that contains selected Address
     * @param address selected Address
     * @return List<Order>
     */
    List<Order> findOrdersWithAddress(Address address);
}
