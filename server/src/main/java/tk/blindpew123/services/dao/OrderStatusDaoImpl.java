package tk.blindpew123.services.dao;

import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.OrderStatus;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Implements OrderStatusDao interface
 * @see OrderStatusDao
 * @see GenericDaoImpl<T>
 */
@Service
public class OrderStatusDaoImpl extends GenericDaoImpl<OrderStatus> implements OrderStatusDao {
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<OrderStatus> findAll(){
        return sessionFactory.getCurrentSession()
                .createNamedQuery("OrderStatus.getAllOrderStatuses", OrderStatus.class)
                .getResultList();
    }
}
