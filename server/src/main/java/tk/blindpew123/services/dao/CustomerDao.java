package tk.blindpew123.services.dao;

import tk.blindpew123.services.dto.Top10CustomerDto;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.User;

import java.util.List;

/**
 * Handles operations with Customer Entity. User of the interface can perform any CRUD operations or findAll.
 * User can skip implementation delete and/or findAll methods. In this case if any of this methods will be invoked
 * UnupportedOperationException will be thrown
 * Also available: findCustomerByUser(User user),  findTop10CustomerTotal()
 * @see GenericDao<T>
 */
public interface CustomerDao extends GenericDao<Customer>{
    /**
     * Returns Customer for selected user
     * @param user selected user
     * @return Customer that match to selected user
     */
    Customer findCustomerByUser(User user);

    /**
     * Returns top 10 Customers by their total purchases
     * @return List<Customers>
     */
    List<Top10CustomerDto> findTop10CustomerTotal();

}
