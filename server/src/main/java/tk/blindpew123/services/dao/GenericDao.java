package tk.blindpew123.services.dao;

import java.util.List;

/**
 *  Provides main CRUD operations with any Entity type T
 *  by default delete and findAll operations throws UnsupportedOperationException
 *  User of this interface can override them
 * @param <T>
 */
public interface GenericDao<T> {
    T saveOrUpdate(T t);
    default void delete(T t){
        throw new UnsupportedOperationException("Delete operation not implemented");
    }
    T findById(long id);
    default List<T> findAll(){
        throw new UnsupportedOperationException("Find All operation not implemented");
    }
}
