package tk.blindpew123.services.dao;

import org.hibernate.query.Query;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class ParameterTypeDaoImpl extends GenericDaoImpl<ParameterType> implements ParameterTypeDao {
    @Transactional
    @Override
    public List<ParameterType> findAllParameterType() {
        CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<ParameterType> criteriaQuery = builder.createQuery(ParameterType.class);
        Root<ParameterType> root = criteriaQuery.from(ParameterType.class);
        criteriaQuery.select(root).orderBy(builder.asc(root.get(ParameterType_.parameterTypeName)));
        Query<ParameterType> allParameterTypeName = sessionFactory.getCurrentSession().createQuery(criteriaQuery);
        return allParameterTypeName.getResultList();
    }

    @Transactional
    @Override
    public List<String> findAllParameterTypesForCategoryAsOrderedList(final Category category) {
        CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
        Root<Parameter> root = criteriaQuery.from(Parameter.class);
        //root.fetch(Parameter_.parameterType, JoinType.LEFT);
        criteriaQuery.where(builder.equal(root.get(Parameter_.category), category));
        criteriaQuery.select(root.get(Parameter_.parameterType)
                .get(ParameterType_.parameterTypeName))
                .orderBy(builder.asc(root.get(Parameter_.parameterOrdinal)));
        //criteriaQuery.distinct(true);
        Query<String> allParametersTypeNames = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery); //.setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
        return allParametersTypeNames.getResultList();
    }
}
