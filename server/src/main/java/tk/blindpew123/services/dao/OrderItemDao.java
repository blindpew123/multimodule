package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.OrderItem;

/**
 * Handles operations with Order Item Entity. User of the interface can perform any CRUD operations or findAll.
 * User can skip implementation delete and/or findAll methods. In this case if any of this methods will be invoked
 * UnupportedOperationException will be thrown
 * @see GenericDao<T>
 */

public interface OrderItemDao extends GenericDao<OrderItem>  {
}
