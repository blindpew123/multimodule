package tk.blindpew123.services.dao;


import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.entities.Category;

import java.util.List;

/**
 * Handles operations with Category Entity. User of the interface can perform any CRUD operations or findAll.
 * User can skip implementation delete and/or findAll methods. In this case if any of this methods will be
 * invoked UnupportedOperationException will be thrown
 * Also for user available next methods: findAllCategoriesAsDtoList, renumberSameLevelCategories,
 * findAllCategoriesForParent, delete(List<Category>)
 *
 * UnupportedOperationException will be thrown
 * @see GenericDao<T>
 */

public interface CategoryDao extends GenericDao<Category> {

    /**
     * Deletes category from database
     * @param category Category object to be deleted.
     * @param isWithOrphans true if orphans need be deleted too (optional)
     */
    @Deprecated
    void deleteCategory(Category category, boolean... isWithOrphans);

    /**
     * Retrieves all available categories as Dto
     * @return all categories as List<CategoryDto>
     */
    List<CategoryDto> findAllCategoriesAsDtoList();

    /**
     * Updates ordinal of category with common Parent, starting with pointed position
     * incrementing or decrementing depends on the step
     * @param commonParent Parent for searching common children
     * @param after starting position (excluded)
     * @param step negative or positive number that will be added to current position
     */
    void renumberSameLevelCategories(Category commonParent, long after, long step);

    /**
     * Returns List<Category> that contains all Categories are a children in relation to a given category
     * @param parent parent Category
     * @return List<Category>
     */
    List<Category> findAllCategoriesForParent(Category parent);

    /**
     *
     * @param categoriesForDelete
     */
    void delete(List<Category> categoriesForDelete);

}
