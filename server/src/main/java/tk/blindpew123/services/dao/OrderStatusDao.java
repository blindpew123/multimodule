package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.OrderStatus;

/**
 * Handles operations with OrderStatus Entity. User of the interface can perform any CRUD operations or findAll.
 * User can skip implementation delete and/or findAll methods. In this case if any of this methods will be invoked
 * UnupportedOperationException will be thrown.
 * By default, this behavior is also defined for the method saveOrUpdate(OrderStatus o)
 */
public interface OrderStatusDao extends GenericDao<OrderStatus> {
    default OrderStatus saveOrUpdate(OrderStatus orderStatus){
        throw new UnsupportedOperationException("SaveOrUpdate not implemented");
    }
}
