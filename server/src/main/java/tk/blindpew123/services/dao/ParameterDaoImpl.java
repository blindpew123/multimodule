package tk.blindpew123.services.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.dto.ParamNameParamValueDto;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.*;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;


@Service
public class ParameterDaoImpl extends GenericDaoImpl<Parameter> implements ParameterDao {

    private static final Logger logger = LogManager.getLogger(ParameterDaoImpl.class.getName());



    @Transactional
    @Override
    public List<Parameter> addParameterList(final List<Parameter> parameterList) {
        logger.debug("Start adding List<Parameter> to DataBase");
        if (parameterList == null){
            String errMessage = "Error adding List<Parameter> to DataBase: it is NULL!!!?!";
            logger.debug(errMessage);
            throw new IllegalArgumentException(errMessage);
        }
        logger.debug("target count: " + parameterList.size());
        for (Parameter parameter : parameterList){
            saveOrUpdate(parameter);
        }
        logger.debug("Adding List<Parameter> to DataBase completed");
        return parameterList;
    }


    @Transactional
    @Override
    public List<Parameter> findAllParametersForCategoryOrdered(final Category category) {
        CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Parameter> criteriaQuery = builder.createQuery(Parameter.class);
        Root<Parameter> root = criteriaQuery.from(Parameter.class);
        criteriaQuery.select(root)
                .where(builder.equal(root.get(Parameter_.category), category));
       // criteriaQuery.distinct(true);
        criteriaQuery.orderBy(builder.asc(root.get(Parameter_.parameterOrdinal)));
        Query<Parameter> allParameterTypeForCategory = sessionFactory.getCurrentSession().createQuery(criteriaQuery); //.setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
        return allParameterTypeForCategory.getResultList();
    }

    /**TODO: JavaDoc
     * status unknown, untested
     * @param category
     * @return
     */
    @Transactional
    @Override
    public List<ParameterDto> findAllParametersForCategoryAsDto(final Category category) {
        CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<ParameterDto> criteriaQuery = cb.createQuery(ParameterDto.class);
        Root<Parameter> root = criteriaQuery.from(Parameter.class);
        criteriaQuery.select(cb.construct(ParameterDto.class,
                root.get(Parameter_.idParameter),
                root.get(Parameter_.parameterName),
                root.get(Parameter_.category).get(Category_.idCategory),
                root.get(Parameter_.parameterOrdinal),
                root.get(Parameter_.parameterType).get(ParameterType_.idParameterType)));
      //  root.join(Parameter_.category, JoinType.LEFT);
      //  root.join(Parameter_.parameterType, JoinType.LEFT);
        criteriaQuery.where(cb.equal(root.get(Parameter_.category), category));
        criteriaQuery.orderBy(cb.asc(root.get(Parameter_.category)),
                cb.asc(root.get(Parameter_.parameterOrdinal)));
        criteriaQuery.distinct(true);
        return sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }


    @Override
    public List<ParamNameParamValueDto> findAllParameterNamesAndValuesOrderedByOrdinalForShopItem(ShopItem shopItem){
        Query<ParamNameParamValueDto> query = sessionFactory.getCurrentSession()
                .createNamedQuery("Parameter.findAllParamNamesWithValuesOrdered", ParamNameParamValueDto.class);
        query.setParameter("id", shopItem);
        return query.getResultList();
    }

    @Override
    public List<ShopItem> findAllShopItemForCriteriesList(final List<ParameterValue> parameterValueList) {
        return null;
    }



    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Parameter> findAllParameters() {
        throw new UnsupportedOperationException("FindAllParameters not implemented yet");
    }


    @Transactional
    @Override
    public List<String> findAllParameterNamesForCategory(final Category category){
        CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
        Root<Parameter> root = criteriaQuery.from(Parameter.class);
        criteriaQuery.where(builder.equal(root.get(Parameter_.category), category));
        criteriaQuery.select(root.get(Parameter_.parameterName)).orderBy(builder.asc(root.get(Parameter_.parameterOrdinal)));
        Query<String> allParametersNames = sessionFactory.getCurrentSession().createQuery(criteriaQuery);
        return allParametersNames.getResultList();
    }

    @Transactional
    @Override
    public ParameterValue findParameterValueByShopItemAndParameter(final ShopItem shopItem, final Parameter parameter) {
        CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<ParameterValue> criteriaQuery = builder.createQuery(ParameterValue.class);
        Root<ParameterValue> root = criteriaQuery.from(ParameterValue.class);
        criteriaQuery.select(root).where(builder.and(builder.equal(root.get(ParameterValue_.shopItem), shopItem),
                builder.equal(root.get(ParameterValue_.parameter), parameter)));
        Query<ParameterValue> parameterValues = sessionFactory.getCurrentSession().createQuery(criteriaQuery);
        List<ParameterValue> results = parameterValues.getResultList();
        return results.size() == 0 ? null : results.get(0);
    }
}
