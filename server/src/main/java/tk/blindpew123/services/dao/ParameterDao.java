package tk.blindpew123.services.dao;


import tk.blindpew123.services.dto.ParamNameParamValueDto;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.*;

import javax.persistence.Tuple;
import java.util.List;

/**
 * Handles operations with Parameter Entity. User of the interface can perform any CRUD operations or findAll.
 * User can skip implementation delete and/or findAll methods. In this case if any of this methods will be invoked
 * UnsupportedOperationException will be thrown.
 * Also available
 * addParameterList(List<Parameter> parameterList);
 * findAllParametersForCategoryOrdered(Category category);
 * findAllParameterNamesAndValuesOrderedByOrdinalForShopItem(ShopItem shopItem);
 * findAllParametersForCategoryAsDto(Category category);
 * findAllParameterNamesAndValuesOrderedByOrdinalForShopItem(ShopItem shopItem);
 * findAllParameterNamesForCategory(Category category);
 */

public interface ParameterDao extends GenericDao<Parameter>{        // Тоже для списка

    /**
     * Adds List<Paraater> to database
     * @param parameterList List<Parameter>
     * @return List<Parameter> List of added parameters
     */
    List<Parameter> addParameterList(List<Parameter> parameterList);

    /**
     * Returns all Parameters for Category ordered
     * @param category
     * @return
     */

    List<Parameter> findAllParametersForCategoryOrdered(Category category);
    // Получить список параметров для конкретной Категории в виде Dto
    List<ParameterDto> findAllParametersForCategoryAsDto(Category category);
    // Получить список параметров со значенями для товара

    List<ParamNameParamValueDto> findAllParameterNamesAndValuesOrderedByOrdinalForShopItem(ShopItem shopItem);
    // Найти все Id Товаров Для шаблона поиска - название категории - значение(список шаблонов)
    List<ShopItem> findAllShopItemForCriteriesList(List<ParameterValue> parameterValueList);

    // Получить все Параметры
    List<Parameter> findAllParameters();
    //
    List<String> findAllParameterNamesForCategory(Category category);

    ParameterValue findParameterValueByShopItemAndParameter(ShopItem shopItem, Parameter parameter);
}
