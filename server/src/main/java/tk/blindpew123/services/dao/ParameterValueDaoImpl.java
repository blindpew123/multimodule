package tk.blindpew123.services.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;


@Service
public class ParameterValueDaoImpl extends GenericDaoImpl<ParameterValue> implements ParameterValueDao {

    private static final Logger logger = LogManager.getLogger(ParameterValueDaoImpl.class.getName());

    @Transactional
    @Override
    public ParameterValue saveOrUpdate(final ParameterValue parameterValue) {
        logger.debug("Start adding ParameterValue to DataBase: ");
        if (parameterValue == null) {
            String errMessage = "Error adding ParameterValue to DataBase: it is NULL!!!?!";
            logger.debug(errMessage);
            throw new IllegalArgumentException(errMessage);
        }
        logger.debug(parameterValue);

        if (parameterValue.getShopItem() == null){
            String errMessage = "Error adding ParameterValue to DataBase: shopItem is NULL!!!?!";
            logger.debug(errMessage);
            throw new IllegalArgumentException(errMessage);
        }

        if (parameterValue.getParameter() == null){
            String errMessage = "Error adding ParameterValue to DataBase: parameter is NULL!!!?!";
            logger.debug(errMessage);
            throw new IllegalArgumentException(errMessage);
        }

        Session session = sessionFactory.getCurrentSession();
        parameterValue.setParameter(session.load(Parameter.class,
                parameterValue.getParameter().getIdParameter()));
        parameterValue.setShopItem(session.load(ShopItem.class,
                parameterValue.getShopItem().getIdShopItem()));
        session.persist(parameterValue);
        logger.debug("ParameterValue Added to DataBase, id: " + parameterValue.getIdParameterValue());
        return parameterValue;
    }

    @Transactional
    @Override
    public List<ParameterValue> addParameterValueList(final List<ParameterValue> parameterValueList) {
        logger.debug("Start adding List<ParameterValue> to DataBase");

        if (parameterValueList == null) {
            String errMessage = "Error adding List<ParameterValue> to DataBase: it is NULL!!!?!";
            logger.debug(errMessage);
            throw new IllegalArgumentException(errMessage);
        }
        logger.debug("target count: " + parameterValueList.size());

        for (ParameterValue parameterValue : parameterValueList){
            saveOrUpdate(parameterValue);
        }

        logger.debug("Adding List<ParameterValue> to DataBase completed");
        return parameterValueList;
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<ParameterValue> findAllParametersValuesForShopItem(final ShopItem shopItem) {
        CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<ParameterValue> criteriaQuery = builder.createQuery(ParameterValue.class);
        Root<ParameterValue> root = criteriaQuery.from(ParameterValue.class);
        criteriaQuery.select(root)
                .where(builder.equal(root.get(ParameterValue_.shopItem), shopItem));
        // criteriaQuery.distinct(true);
        criteriaQuery.orderBy(builder.asc(root.get(ParameterValue_.parameter).get(Parameter_.parameterOrdinal)));
        Query<ParameterValue> allParameterValueForShopItem = sessionFactory.getCurrentSession().createQuery(criteriaQuery); //.setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
        return allParameterValueForShopItem.getResultList();
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public void deleteAllValuesForParameter(Parameter parameter) {
        sessionFactory.getCurrentSession()
                .createNamedQuery("ParameterValue.deleteAllValuesForParameter")
                .setParameter("p", parameter).executeUpdate();
    }
}
