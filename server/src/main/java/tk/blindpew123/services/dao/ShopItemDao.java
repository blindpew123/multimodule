package tk.blindpew123.services.dao;


import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.Category;
import tk.blindpew123.services.entities.ShopItem;
import java.util.List;

public interface ShopItemDao extends GenericDao<ShopItem> {

    List<ShopItemDto> getShopItemsForCategoryAsDtoList(Category category);

    List<Object[]> getShopItemsForCategoryAsObjArray(Category category, PageListParamsDto pageListParamsDto);

    List<Object[]> findAllShopItemsWithParameterValuesForCategory(Category category);

    Object[] findShopItemWithParameterValuesByShopItem(ShopItem shopItem);

    List<Object[]> findAllShopItemsWithParameterValuesSelectedAndOrdered(
            List<FilterCriteriaDto> condition,
            int sortFieldNumber,
            Category category,
            List<String> shopItemFields,
            PageListParamsDto pageListParamsDto);

    List<ShopItem> findAllItemsForUpdateByIds(Object[] ids);

    List<ShopItemStatTop10Dto> findTop10QtyShopItems();

    List<ShopItemAdvTop10Dto> findTop10QtyShopItemsAdv();

    long countShopItemsInCategories(List<Category> categoryList);

    long countShopItemsInSingleCategory(Category category);

    long countShopItemsInSingleCategoryWithParams(List<FilterCriteriaDto> condition,
                                                  int sortFieldNumber,
                                                  Category category,
                                                  List<String> shopItemFields);
}
