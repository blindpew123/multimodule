package tk.blindpew123.services.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Implements OrderDao interface
 * @see GenericDao<T>
 * @see OrderDao
 */
@Service
public class OrderDaoImpl extends GenericDaoImpl<Order> implements OrderDao {


    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Order> findAllOrdersBetweenDates(final LocalDateTime startDate, final LocalDateTime endDate) {
        return sessionFactory.getCurrentSession()
                .createNamedQuery("Order.findAllBetweenDates", Order.class)
                .setParameter("d1", startDate).setParameter("d2", endDate)
                .getResultList();
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Order> findAllOrderByCustomer(final Customer customer) {
        return sessionFactory.getCurrentSession()
                .createNamedQuery("Order.findAllOrderByCustomer", Order.class)
                .setParameter("c", customer)
                .getResultList();
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public Double findTotalForPeriod(final LocalDateTime startDate, final LocalDateTime endDate) {
        List<Double> resultList = sessionFactory.getCurrentSession()
                .createNamedQuery("Order.getPeriodTotal", Double.class)
                .setParameter("d1", startDate).setParameter("d2", endDate).getResultList();
        if (resultList == null || resultList.size() == 0){
            return 0d;
        } else return resultList.get(0);
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Order> findOrdersWithAddress(final Address address){
        return sessionFactory.getCurrentSession()
                .createNamedQuery("Order.findOrderWithAddress", Order.class)
                .setParameter("a", address).getResultList();
    }

}
