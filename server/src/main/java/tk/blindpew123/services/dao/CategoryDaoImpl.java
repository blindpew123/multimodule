package tk.blindpew123.services.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.Query;
import org.jgroups.annotations.Unsupported;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.entities.Category;
import tk.blindpew123.services.entities.Category_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 *  Implementation CategoryDao
 *  @see CategoryDao
 */
@Service
public class CategoryDaoImpl extends GenericDaoImpl<Category> implements CategoryDao {

  //  private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(CategoryDaoImpl.class.getName());

    /**
     * Deletes category from database. It works in two modes, depends on isWithOrphans flag.
     * When this falg is true method deletes all orhans appears in that case row by row.
     * SQL query search all Category without parent and deletes them, until affected row not 0
     * In case when isWithOrphans false method deletes only category from parameter. All children moves on level up
     * Method gives them ordinal numbers after those which there are already
     * @param category Category object to be deleted.
     * @param isWithOrphans true if orphans need be deleted too (optional).
     *                      Else closest children get link to parent of deleted element.
     */
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public void deleteCategory(final Category category, final boolean... isWithOrphans) {

        throw new UnsupportedOperationException("Can't delete");

     /*   Session session = sessionFactory.getCurrentSession();
        renumberSameLevelCategories(category.getParent(), category.getCategoryNumber(), -1L);
        Query categoryDeleteQuery = session.createNamedQuery("Category.delete");
        categoryDeleteQuery.setParameter("id", category.getIdCategory());
        categoryDeleteQuery.executeUpdate();

        if (isWithOrphans.length > 0 && isWithOrphans[0]) {
            // JPA not support subselect in FROM clause. So we use native SQL
            Query orphanDeleteQuery = session.createNativeQuery(Category.SQL_DELETE_ORPHANS);
            while (orphanDeleteQuery.executeUpdate() > 0) ; // until no affected rows returns
        } else {
            // Change ordinal numbers for children that move to top level
            Long maxOrdinal = (Long) session.createNamedQuery("Category.findMaxCategoryOrdinalInRow")
                    .setParameter("p", session.load(Category.class, category.getIdCategory()))
                    .getSingleResult();
            logger.info(maxOrdinal);
            renumberSameLevelCategories(category, 0,  maxOrdinal);

            // Moving children of removed Category to top level by replacing their Parent
            Query updateChildrenQuery = session.createNamedQuery("Category.updateParentForClosestChildrenWithId");
            updateChildrenQuery.setParameter("op",
                    session.load(Category.class, category.getIdCategory()));
            if (category.getParent() != null) {
                updateChildrenQuery.setParameter("np", category.getParent());
            } else {
                updateChildrenQuery.setParameter("np", (Category) null);
            }
            updateChildrenQuery.executeUpdate();
        } */
    }

// -------------------v2--------------------------------------
    /**
     * Retrieves all available categories as Dto
     * TODO: reduce number of SQL query, May be JPQL????
     * @return all categories as List<CategoryDto>
     *
     */

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<CategoryDto> findAllCategoriesAsDtoList() {
        CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<CategoryDto> criteriaQuery = cb.createQuery(CategoryDto.class);
        Root<Category> root = criteriaQuery.from(Category.class);
        criteriaQuery.select(cb.construct(CategoryDto.class,
                root.get(Category_.idCategory),
                root.get(Category_.categoryName),
                root.get(Category_.parent),
                root.get(Category_.categoryNumber)));
        root.join(Category_.parent, JoinType.LEFT);
        criteriaQuery.where(cb.isFalse(root.get(Category_.DELETED)));
        criteriaQuery.orderBy(cb.asc(root.get(Category_.parent).get(Category_.idCategory)),
                cb.asc(root.get(Category_.categoryNumber)));
        criteriaQuery.distinct(true);
        return sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    /**
     * Updates ordinal of category with common Parent, starting with pointed position
     * incrementing or decrementing depends on the step
     * @param commonParent Parent for searching common children
     * @param after starting position (excluded)
     * @param step negative or positive number that will be added to current position
     */

    @Transactional(Transactional.TxType.MANDATORY)
    public void renumberSameLevelCategories(
            final Category commonParent, final long after, final long step) {
        Session session = sessionFactory.getCurrentSession();
        Query query  = session.createNamedQuery("Category.incrementChildOrderField");
        query.setParameter("p", commonParent);
        query.setParameter("n", after);
        query.setParameter("i", step);
        query.executeUpdate();
    }

    /**
     * Performs cascade delete orphans of target category
     *
     * @param category Category object which orphans will be deleted.
     */
    @Transactional(Transactional.TxType.MANDATORY)
    public void deleteChildrenOfCategory(final Category category){
        throw new UnsupportedOperationException();
       /* Session session = sessionFactory.getCurrentSession();
        Query orphanDeleteQuery = session.createNativeQuery(Category.SQL_DELETE_ORPHANS);
        while (orphanDeleteQuery.executeUpdate() > 0){
            session.flush();
        }; // until no affected rows returns */
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Category> findAllCategoriesForParent(final Category parent) {
    return sessionFactory.getCurrentSession()
                .createNamedQuery("Category.findChildrenByParent", Category.class)
                .setParameter("p", parent)
                .getResultList();
    }

    /**
     *  Unsupported method. Use delete(List<Category> categoriesForDelete)
     *
     * @param category
     */
    @Override
    @Unsupported
    public void delete(final Category category){
        throw new UnsupportedOperationException("Can't delete single category");
    }



    /**
     * Deletes categories from database.
     *
     * @param categoriesForDelete List<Category> contains all Categories which will be deleted.
     */
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public void delete(final List<Category> categoriesForDelete) {
        if (categoriesForDelete == null){
            return;
        }
        sessionFactory.getCurrentSession()
                .createNamedQuery("Category.setDeletedForCategoryList")
                .setParameterList("list", categoriesForDelete).executeUpdate();
    }
}
