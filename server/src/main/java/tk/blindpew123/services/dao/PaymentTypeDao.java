package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.PaymentType;



public interface PaymentTypeDao extends GenericDao<PaymentType> {
    default PaymentType saveOrUpdate(PaymentType paymentType){
        throw new UnsupportedOperationException("SaveOrUpdate not implemented");
    }
}
