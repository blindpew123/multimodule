package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.Shipping;

public interface ShippingDao extends GenericDao<Shipping> {
    default Shipping saveOrUpdate(Shipping paymentType){
        throw new UnsupportedOperationException("SaveOrUpdate not implemented");
    }
}
