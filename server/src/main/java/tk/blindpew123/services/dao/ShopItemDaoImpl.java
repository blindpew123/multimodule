package tk.blindpew123.services.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.*;


import javax.persistence.LockModeType;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.*;

@Service
public class ShopItemDaoImpl extends GenericDaoImpl<ShopItem> implements ShopItemDao {

    private static final Logger logger = LogManager.getLogger(ShopItemDaoImpl.class.getName());

    @Transactional(value = Transactional.TxType.MANDATORY)
    @Override
    public List<ShopItemDto> getShopItemsForCategoryAsDtoList(final Category category) {
        CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<ShopItemDto> criteriaQuery = cb.createQuery(ShopItemDto.class);
        Root<ShopItem> root = criteriaQuery.from(ShopItem.class);
        criteriaQuery.select(cb.construct(ShopItemDto.class,
                root.get(ShopItem_.idShopItem),
                root.get(ShopItem_.price),
                root.get(ShopItem_.itemName),
                root.get(ShopItem_.category).get(Category_.idCategory),
                root.get(ShopItem_.weight),
                root.get(ShopItem_.volume),
                root.get(ShopItem_.qnty),
                root.get(ShopItem_.imageLink)));
        criteriaQuery.where(cb.equal(root.get(ShopItem_.category), category));
        criteriaQuery.orderBy(cb.asc(root.get(ShopItem_.itemName)));
   //   criteriaQuery.distinct(true);
        return sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery)
           //   .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }


    @Transactional(value = Transactional.TxType.MANDATORY)
    @Override
    public List<Object[]> getShopItemsForCategoryAsObjArray(final Category category,
                                                            final PageListParamsDto pageListParamsDto) {
        CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Object[]> criteriaQuery = cb.createQuery(Object[].class);
        Root<ShopItem> root = criteriaQuery.from(ShopItem.class);
        criteriaQuery.multiselect(
                root.get(ShopItem_.idShopItem),
                root.get(ShopItem_.category).get(Category_.idCategory),
                root.get(ShopItem_.itemName),
                root.get(ShopItem_.price),
                root.get(ShopItem_.weight),
                root.get(ShopItem_.volume),
                root.get(ShopItem_.qnty),
                root.get(ShopItem_.imageLink));
        criteriaQuery.where(cb.and(cb.equal(root.get(ShopItem_.category), category),
                cb.greaterThan(root.get(ShopItem_.price), 0d)));

        criteriaQuery.orderBy(cb.asc(root.get(ShopItem_.itemName)));

        return sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery).setFirstResult(pageListParamsDto.getStart())
                .setMaxResults(pageListParamsDto.getSize())
                .getResultList();    }

    @Override
    public Object[] findShopItemWithParameterValuesByShopItem(ShopItem shopItem) {
        return new Object[0];
    }

    @SuppressWarnings("unchecked")
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Object[]> findAllShopItemsWithParameterValuesSelectedAndOrdered(final List<FilterCriteriaDto> condition,
                                                                                final int sortFieldNumber,
                                                                                final Category category,
                                                                                final List<String> shopItemFields,
                                                                                PageListParamsDto pageListParamsDto) {
        // base columns

        int maxParamValueNumber = getNumberOfParametersForCategory(category);
        Map<String, Object> paramMap = new HashMap<>();

        String orderingClause = "";

        StringBuilder sb = new StringBuilder("SELECT ");
        for (int i = 0; i < shopItemFields.size(); i++){
            sb.append("s.").append(shopItemFields.get(i));
            // check sorting
            if (i == sortFieldNumber){
               orderingClause = "s." +  shopItemFields.get(i);
            }
            if (i < shopItemFields.size() - 1) {
                sb.append(", ");
            }
        }

        // now additional fields
        for (int i = 1; i <= maxParamValueNumber; i++){
            //checking form data beyond ShopItem's fields
            if (isNotEmpty(condition.get(i + shopItemFields.size() - 1).getValue())
                    || (i + shopItemFields.size() - 1 == sortFieldNumber)){ // may be it's sorting field
                sb.append(", ");
                sb.append("p").append(i).append(".pval" + i);

                //try to create sorting name field

                if (i + shopItemFields.size() - 1 == sortFieldNumber){
                 //   orderingClause = "p" + i + ".parametervalue" + i + " ";
                    orderingClause = "pval" + i + " ";
                }
            }
        }
        sb.append(" FROM ShopItem s");

        for (int i = 1; i <= maxParamValueNumber; i++) {
            if (isNotEmpty(condition.get(i + shopItemFields.size() - 1).getValue())
                    || (i + shopItemFields.size() - 1 == sortFieldNumber)){ // may be it's sorting field
                sb.append(" LEFT JOIN  (select pv.parameterValue as pval" + i + ", pv.shopItem from ParameterValue pv WHERE pv.parameter =");
                sb.append("(SELECT param.idParameter FROM parameter param WHERE param.category = ");
                sb.append(category.getIdCategory()); //param
                sb.append(" AND param.parameterOrdinal =").append(i).append(")) p").append(i);
                sb.append(" ON s.idShopItem = p").append(i).append(".shopItem ");
            }
        }

         sb.append(" where s.category = ").append(category.getIdCategory());
         sb.append(" and s.price > 0");
        // condition for ShopItemFields

        for (int i = 0; i < shopItemFields.size(); i++){
            if (isNotEmpty(condition.get(i).getValue())){
                sb.append(" AND ").append("(").append(shopItemFields.get(i));
                if (condition.get(i).getFieldType().equals(ParameterType.ParamType.NUMBER.getParamTypeName())) {
                    StringBuilder numberExpr = new StringBuilder();
                    numberExpr.append(condition.get(i).getMathCondition());
                    numberExpr.append(condition.get(i).getValue());
                    if(numberExpr.toString().equals("=0")){
                        numberExpr.append(" or ").append(shopItemFields.get(i)).append(" is null");
                    }
                    sb.append(numberExpr.toString());
                } else {
                    sb.append(" LIKE '%");
                    sb.append(condition.get(i).getValue()).append("%'");
                }
                sb.append(")");
            }
        }

        // conditions for outside of shopitem's fields
        for (int i = 1; i <= maxParamValueNumber; i++){
            if (isNotEmpty(condition.get(i + shopItemFields.size() - 1).getValue())){
       //      sb.append(" AND ").append("p").append(i).append(".parametervalue" + i);
                sb.append(" AND ").append("(").append("pval" + i);
             if (condition.get(i + shopItemFields.size() - 1).getFieldType().equals(ParameterType.ParamType.NUMBER.getParamTypeName())){
                 StringBuilder numberExpr = new StringBuilder();
                 numberExpr.append(condition.get(i + shopItemFields.size() - 1).getMathCondition());
                 numberExpr.append(condition.get(i + shopItemFields.size() - 1).getValue());
                 if(numberExpr.toString().equals("=0")){
                     numberExpr.append(" or ").append("pval" + i).append(" is null");
                 }
                 sb.append(numberExpr.toString());
             } else {
                 sb.append(" LIKE '%");
                 sb.append(condition.get(i + shopItemFields.size() - 1).getValue()).append("%'");
             }
             sb.append(")");
            }
        }

        if (isNotEmpty(orderingClause)) {
            sb.append(" ORDER BY ").append(orderingClause);
        }

        logger.debug(sb.toString());

        Query shopItemsSelectedAndSorted = sessionFactory.getCurrentSession().createNativeQuery(sb.toString());


        List<Object[]> result = shopItemsSelectedAndSorted
                .setFirstResult(pageListParamsDto.getStart())
                .setMaxResults(pageListParamsDto.getSize()).
                        getResultList();
        return result;
    }


    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public long  countShopItemsInSingleCategoryWithParams(final List<FilterCriteriaDto> condition,
                                                                                final int sortFieldNumber,
                                                                                final Category category,
                                                                                final List<String> shopItemFields) {
        // base columns

        int maxParamValueNumber = getNumberOfParametersForCategory(category);
        Map<String, Object> paramMap = new HashMap<>();

        String orderingClause = "";

        StringBuilder sb = new StringBuilder("SELECT COUNT(s.idShopItem)");
        for (int i = 0; i < shopItemFields.size(); i++){
            if (i == sortFieldNumber){
                orderingClause = "s." +  shopItemFields.get(i);
            }
        }

        sb.append(" FROM ShopItem s");

        for (int i = 1; i <= maxParamValueNumber; i++) {
            if (isNotEmpty(condition.get(i + shopItemFields.size() - 1).getValue())
                    || (i + shopItemFields.size() - 1 == sortFieldNumber)){ // may be it's sorting field
                sb.append(" LEFT JOIN  (select pv.parameterValue as pval" + i + ", pv.shopItem from ParameterValue pv WHERE pv.parameter =");
                sb.append("(SELECT param.idParameter FROM parameter param WHERE param.category = ");
                sb.append(category.getIdCategory()); //param
                sb.append(" AND param.parameterOrdinal =").append(i).append(")) p").append(i);
                sb.append(" ON s.idShopItem = p").append(i).append(".shopItem ");
            }
        }

        sb.append(" where s.category = ").append(category.getIdCategory());
        sb.append(" and s.price > 0");
        // condition for ShopItemFields

        for (int i = 0; i < shopItemFields.size(); i++){
            if (isNotEmpty(condition.get(i).getValue())){
                sb.append(" AND ").append("(").append(shopItemFields.get(i));
                if (condition.get(i).getFieldType().equals(ParameterType.ParamType.NUMBER.getParamTypeName())) {
                    StringBuilder numberExpr = new StringBuilder();
                    numberExpr.append(condition.get(i).getMathCondition());
                    numberExpr.append(condition.get(i).getValue());
                    if(numberExpr.toString().equals("=0")){
                        numberExpr.append(" or ").append(shopItemFields.get(i)).append(" is null");
                    }
                    sb.append(numberExpr.toString());
                } else {
                    sb.append(" LIKE '%");
                    sb.append(condition.get(i).getValue()).append("%'");
                }
                sb.append(")");
            }
        }

        // conditions for outside of shopitem's fields
        for (int i = 1; i <= maxParamValueNumber; i++){
            if (isNotEmpty(condition.get(i + shopItemFields.size() - 1).getValue())){
                //      sb.append(" AND ").append("p").append(i).append(".parametervalue" + i);
                sb.append(" AND ").append("(").append("pval" + i);
                if (condition.get(i + shopItemFields.size() - 1).getFieldType().equals(ParameterType.ParamType.NUMBER.getParamTypeName())){
                    StringBuilder numberExpr = new StringBuilder();
                    numberExpr.append(condition.get(i + shopItemFields.size() - 1).getMathCondition());
                    numberExpr.append(condition.get(i + shopItemFields.size() - 1).getValue());
                    if(numberExpr.toString().equals("=0")){
                        numberExpr.append(" or ").append("pval" + i).append(" is null");
                    }
                    sb.append(numberExpr.toString());
                } else {
                    sb.append(" LIKE '%");
                    sb.append(condition.get(i + shopItemFields.size() - 1).getValue()).append("%'");
                }
                sb.append(")");
            }
        }

        Query shopItemsCount = sessionFactory.getCurrentSession().createNativeQuery(sb.toString());
        return ((BigInteger)shopItemsCount.getSingleResult()).longValue();
    }




    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<ShopItem> findAllItemsForUpdateByIds(final Object[] ids) {
        return sessionFactory.getCurrentSession()
                .createNamedQuery("ShopItem.findAllItemsForUpdateByIds", ShopItem.class)
                .setParameterList("ids", ids).setLockMode(LockModeType.PESSIMISTIC_WRITE).getResultList();
    }

    private boolean isNotEmpty(String value){
        return value != null && !value.equals("");
    }

    @SuppressWarnings("unchecked")
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Object[]> findAllShopItemsWithParameterValuesForCategory(final Category category){
        //At first we try to find max number additional parameters for all ShopItems in concrete Category

        int maxParamValueNumber = getNumberOfParametersForCategory(category);

        // Now we build query like
        /*
        select shopitem.*, p1.parametervalue, p2.parameterValue, p3.parameterValue from shopitem
        left join (select parametervalue, ShopItem from parametervalue where Parameter IN
                (select parameter.idParameter from parameter where Category = 1 and ParameterOrdinal = 1)) p1
        on idShopItem = p1.ShopItem
        left join (select parametervalue, ShopItem from parametervalue where Parameter IN
                (select parameter.idParameter from parameter where Category = 1 and ParameterOrdinal = 2)) p2
        on idShopItem = p2.ShopItem
        left join (select parametervalue, ShopItem from parametervalue where Parameter IN
                (select parameter.idParameter from parameter where Category = 1 and ParameterOrdinal = 3)) p3
        on idShopItem = p3.ShopItem
        where shopitem.Category = 1;
        */


        // and so on....

        StringBuilder queryStringBuilder = new StringBuilder(
                "select idShopItem, Category, itemName, price, weight, volume, qnty, imagelink  "); //right order of fields
        for (int i = 1; i <= maxParamValueNumber; i++){
            queryStringBuilder.append(", ");
            queryStringBuilder.append("p").append(i).append(".parametervalue as pv").append(i);
        }
        queryStringBuilder.append(" from shopitem");
        for (int i = 1; i <= maxParamValueNumber; i++){
            queryStringBuilder
                    .append(" left join (select parametervalue, ShopItem from parametervalue where Parameter =")
                    .append("(select parameter.idParameter from parameter where Category = ").append(category.getIdCategory())
                    .append(" and ParameterOrdinal =")
                    .append(i).append(")) p").append(i)
                    .append(" on idShopItem = p").append(i).append(".ShopItem ");

        }
        queryStringBuilder.append(" where shopitem.Category = ").append(category.getIdCategory());
        Query shopItemsWithParams = sessionFactory.getCurrentSession().createNativeQuery(queryStringBuilder.toString());
        // Отбор по параметру (основные или ординал-ссылка)
        //
        return shopItemsWithParams.getResultList();
    }




    @Transactional(Transactional.TxType.MANDATORY)
    protected int getNumberOfParametersForCategory(final Category category){
        CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Integer> criteriaQuery = cb.createQuery(Integer.class);
        Root<Parameter> root = criteriaQuery.from(Parameter.class);
        criteriaQuery.select(cb.max(root.get(Parameter_.parameterOrdinal)));
        criteriaQuery.where(cb.equal(root.get(Parameter_.category), category));
        Integer maxParamValueNumber = sessionFactory.getCurrentSession().createQuery(criteriaQuery).getSingleResult();
        return (maxParamValueNumber == null ? 0 : maxParamValueNumber);
    }

    @SuppressWarnings("unchecked")
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<ShopItemStatTop10Dto> findTop10QtyShopItems(){
        return sessionFactory.getCurrentSession()
                .createNamedQuery("ShopItem.top10ShopItemsByQty").setMaxResults(10)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<ShopItemAdvTop10Dto> findTop10QtyShopItemsAdv() {
        return sessionFactory.getCurrentSession()
                .createNamedQuery("ShopItem.top10AdvShopItemsByQty")
                .setResultTransformer(Transformers.aliasToBean(ShopItemAdvTop10Dto.class))
                .setMaxResults(10)
                .getResultList();

    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public long countShopItemsInCategories(List<Category> categoryList){
        return (long) sessionFactory.getCurrentSession()
                .createNamedQuery("ShopItem.countItemsInCategories")
                .setParameterList("categories", categoryList)
                .getSingleResult();
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public long countShopItemsInSingleCategory(Category category) {
        return (long) sessionFactory.getCurrentSession()
                .createNamedQuery("ShopItem.countItemsInSingleCategory")
                .setParameter("category", category)
                .getSingleResult();
    }


}
