package tk.blindpew123.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.exceptions.NoSuchShopItemException;
import tk.blindpew123.services.dao.ShopItemDao;
import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.Category;
import tk.blindpew123.services.entities.Parameter;
import tk.blindpew123.services.entities.ParameterValue;
import tk.blindpew123.services.entities.ShopItem;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShopItemService {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ParameterService parameterService;
    @Autowired
    private ShopItemDao shopItemDao;

    @Transactional
    public List<ShopItemDto> getShopItemsForCategoryAsDtoList(final long id){
        return shopItemDao.getShopItemsForCategoryAsDtoList(categoryService.getCategoryById(id));
    }

    @Transactional
    public List<Object[]> getShopItemsForCategoryAsObjArray(final long id, final PageListParamsDto pageListParamsDto){
        return shopItemDao.getShopItemsForCategoryAsObjArray(
                categoryService.getCategoryById(id), pageListParamsDto);
    }

    @Transactional
    protected List<Object[]> getAllShopItemsWithParameterValuesForCategory(final long id){
        Category category = categoryService.getCategoryById(id);
        return shopItemDao.findAllShopItemsWithParameterValuesForCategory(category);
    }

    @Transactional
    public ShopItemsColNamesColTypesTripletDto getShopItemsAndValuesAndNamesAndTypesAsDto(final long id){
        ShopItemsColNamesColTypesTripletDto tripletDto = new ShopItemsColNamesColTypesTripletDto();
        tripletDto.setShopItemsList(getAllShopItemsWithParameterValuesForCategory(id));
        List<String> names =  new ArrayList<>(ShopItem.getFieldAliases());
        names.addAll(parameterService.getAllParameterNamesForCategory(id));
        tripletDto.setColumnNames(names);
        List<String> types = new ArrayList<>(ShopItem.getFieldTypes());
        types.addAll(parameterService.findAllParameterTypesForCategoryAsOrderedList(id));
        tripletDto.setColumnTypes(types);
        return tripletDto;
    }

    @Transactional
    public void saveOrUpdateShopItemDtoWithParam(final ShopItemSaveDto shopItemSaveDto){
        // Соханить ШопАйтем
        ShopItem shopItem = mapShopItemDtoToShopItem(shopItemSaveDto.getShopItem());
        shopItemDao.saveOrUpdate(shopItem);
        // Сохранить поштучно значения параметров;
        // Получить описание параметра из базы по категории и ординалу
        List<Parameter> parameters = parameterService
                .getParametersForCategoryOrdered(shopItem.getCategory().getIdCategory());
        for (int i = 0; i < shopItemSaveDto.getParamValues().length; i++){
            ParameterValue parameterValue= parameterService
                    .getParameterValueByShopItemAndParameter(shopItem, parameters.get(i));
            if (parameterValue == null){
               parameterValue = new ParameterValue();
               parameterValue.setShopItem(shopItem);
               parameterValue.setParameter(parameters.get(i));
            }
            parameterValue.setParameterValue(shopItemSaveDto.getParamValues()[i]);
            parameterService.saveOrUpdateParameterValue(parameterValue);
        }
    }

    protected ShopItem mapShopItemDtoToShopItem(final ShopItemDto shopItemDto){
        ShopItem resultShopItem = new ShopItem();
        resultShopItem.setIdShopItem(shopItemDto.getIdShopItem());
        resultShopItem.setCategory(categoryService.getCategoryById(shopItemDto.getIdCategory()));
        resultShopItem.setItemName(shopItemDto.getItemName());
        resultShopItem.setPrice(shopItemDto.getPrice());
        resultShopItem.setQnty(shopItemDto.getQnty());
        resultShopItem.setVolume(shopItemDto.getVolume());
        resultShopItem.setWeight(shopItemDto.getWeight());
        resultShopItem.setImageLink(shopItemDto.getImageLink());
        return resultShopItem;
    }

    @Transactional
    public List<ShopItemDto> getListShopItemDtoForCategory(final long id){
        Category category  = categoryService.getCategoryById(id);
        return shopItemDao.getShopItemsForCategoryAsDtoList(category);
    }

    @Transactional
    public SingleShopItemTupleDto getSingleShopItemInfo(final long id){
        ShopItem shopItem = shopItemDao.findById(id);
        if(shopItem == null) {
            throw new NoSuchShopItemException("No such item with id: " + id);
        }
        SingleShopItemTupleDto shopItemTupleDto = new SingleShopItemTupleDto();
        shopItemTupleDto.setShopItem(shopItem);
        shopItemTupleDto.setShopItemNamesAndValues(parameterService.getAllParameterNamesAndValues(shopItem));
        return shopItemTupleDto;
    }

    @Transactional
    public List<Object[]> getShopItemsSortedAndFiltered(final long id,
                                                        final FilterFormDto filterFormDto,
                                                        final PageListParamsDto pageListParamsDto){
        Category category = categoryService.getCategoryById(id);

        List<Object[]> result = shopItemDao.findAllShopItemsWithParameterValuesSelectedAndOrdered(filterFormDto.getFilterList(),
                filterFormDto.getSortNameNumberField(), category, ShopItem.getFieldNames(), pageListParamsDto);

        return result;
    }
    @Transactional
    public ShopItem getShopItemById(final long id){
        return shopItemDao.findById(id);
    }


    @Transactional
    public List<ShopItem> getAllShopItemForCreatingOrder(final Object[] ids){
        return shopItemDao.findAllItemsForUpdateByIds(ids);
    }

    @Transactional
    public List<ShopItemStatTop10Dto> getTop10QtyItems(){
        return shopItemDao.findTop10QtyShopItems();
    }

    @Transactional
    public List<ShopItemAdvTop10Dto> getAdvTop10QtyItems(){
        return shopItemDao.findTop10QtyShopItemsAdv();
    }

    @Transactional
    public long getCountShopItemsInCategories(List<Category> categoryList){
        return shopItemDao.countShopItemsInCategories(categoryList);
    }

    @Transactional
    public long getCountShopItemsForCategory(long id){
        Category category = categoryService.getCategoryById(id);
        return shopItemDao.countShopItemsInSingleCategory(category);
    }

    @Transactional
    public long getCountShopItemsWithParamsForCategory(final long id,
                                                       final FilterFormDto filterFormDto) {
        Category category = categoryService.getCategoryById(id);

        return shopItemDao.countShopItemsInSingleCategoryWithParams(filterFormDto.getFilterList(),
                filterFormDto.getSortNameNumberField(), category, ShopItem.getFieldNames());
    }

    public PaginationDto getPaginationDto(PageListParamsDto pageListParamsDto){
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setCurrentStartRecord(pageListParamsDto.getStart());
        paginationDto.setPageSize(pageListParamsDto.getSize());
        paginationDto.setCurrentPage(pageListParamsDto.getStart() / pageListParamsDto.getSize());
        return paginationDto;
    }


}
