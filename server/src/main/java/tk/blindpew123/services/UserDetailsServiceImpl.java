package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import tk.blindpew123.services.dao.UserDao;
import tk.blindpew123.services.entities.Role;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger logger = LogManager.getLogger(UserDetailsServiceImpl.class.getName());

    @Autowired
    private UserDao userDao;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(final String emailAsLogin) throws UsernameNotFoundException {

        logger.debug("Trying identificate user with name " + emailAsLogin);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        tk.blindpew123.services.entities.User user = userDao.findUserByLogin(emailAsLogin);
        if (user == null) {
           throw new UsernameNotFoundException("Not found");
        }

        for (Role role : user.getRoles()){
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
            logger.debug(authority);
            logger.debug(role.getName());
            grantedAuthorities.add(authority);

        }
        return new User(user.getEmailAsLogin(), user.getPassword(), grantedAuthorities);
    }
}
