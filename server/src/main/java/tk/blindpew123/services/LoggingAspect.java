package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;


@Aspect
public class LoggingAspect {
    private static final Logger logger = LogManager.getLogger(LoggingAspect.class.getName());

    @Before("execution(* tk.blindpew123..*(..)) " +
            "&& !execution(* tk.blindpew123.services.entities..*(..)) " +
            "&& !execution(* tk.blindpew123.services.dto..*(..)) " +
            "&& !execution(* tk.blindpew123.controllers..*(..))")
    public void logBefore(JoinPoint joinPoint) {
        logger.debug(joinPoint.getTarget());
        logger.debug(" >>> entered: " + joinPoint.getSignature().getName()+"(...)");
        Object[] signatureArgs = joinPoint.getArgs();
        logger.debug("-- with args: ");
        for (Object signatureArg: signatureArgs) {
            logger.debug(signatureArg);
        }
        logger.debug("-- end args");
    }

    @Before("execution(* tk.blindpew123.controllers..*(..))")
    public void logBeforeController(JoinPoint joinPoint) {
        logger.debug("----------------------------");
        logger.debug("-- Controller: " + joinPoint.getTarget());
        logger.debug("-- method: " + joinPoint.getSignature().getName());
        Object[] signatureArgs = joinPoint.getArgs();
        logger.debug("-- with args: ");
        for (Object signatureArg: signatureArgs) {
            logger.debug(signatureArg);
        }
        logger.debug("-- end args");
    }


    @AfterReturning(value = "execution(* tk.blindpew123..*(..)) " +
            "&& !execution(* tk.blindpew123.services.entities..*(..)) " +
            "&& !execution(* tk.blindpew123.services.dto..*(..)) ",
            returning = "returnValue")
    public void logReturning(JoinPoint joinPoint, Object returnValue){
        logger.debug("-- and method " + joinPoint.getSignature().getName());
        logger.debug("-- returns " + returnValue);
        logger.debug("");
    }

    @After("execution(* tk.blindpew123..*(..)) " +
            "&& !execution(* tk.blindpew123.services.entities..*(..)) " +
            "&& !execution(* tk.blindpew123.services.dto..*(..)) ")
    public void logReturning(JoinPoint joinPoint){
        logger.debug(joinPoint.getTarget());
        logger.debug("^^^ exited from method: " + joinPoint.getSignature().getName());
    }

    @AfterThrowing(value = "execution(* tk.blindpew123..*(..)))", throwing = "error")
    public void logReturning(JoinPoint joinPoint, Throwable error){
        logger.debug("-- and method " + joinPoint.getSignature().getName());
        logger.debug("!!!!! throws :" + error);
        logger.debug(error.getStackTrace());
        logger.debug("");
    }
}
