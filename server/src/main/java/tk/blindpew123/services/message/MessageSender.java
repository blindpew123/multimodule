package tk.blindpew123.services.message;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import tk.blindpew123.services.CategoryService;


@Component
public class MessageSender {

    private static final Logger logger = LogManager.getLogger(MessageSender.class.getName());

    @Autowired
    @Lazy
    private JmsTemplate jmsTemplate;

    public void sendMessage(final String string) {

        jmsTemplate.send(new MessageCreator(){
            @Override
            public Message createMessage(Session session) throws JMSException{
                ObjectMessage objectMessage = session.createObjectMessage(string);
                return objectMessage;
            }
        });
        logger.debug(">>>>>> JMS: message sent");
    }

}
