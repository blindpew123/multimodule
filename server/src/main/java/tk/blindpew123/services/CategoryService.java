package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.exceptions.CategoryManagementException;
import tk.blindpew123.exceptions.NoSuchCategoryException;
import tk.blindpew123.services.dao.CategoryDao;
import tk.blindpew123.services.dto.CategoryContentSummaryDto;
import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.dto.DeleteCategoryDto;
import tk.blindpew123.services.entities.Category;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 *  Works with Category and CategoryParameters DTO.
 */
@Service
public class CategoryService {


    private static final Logger logger = LogManager.getLogger(CategoryService.class.getName());

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private ShopItemService shopItemService;

    /**
     * status works, 100% tested
     *
     * Gets List<CategoryDto> from DAO
     *
     * @return list CategoryDto or null when something happens
     */
    @Transactional
    public List<CategoryDto> getAllCategoriesAsDtoList(){
        return categoryDao.findAllCategoriesAsDtoList();
    }

    /** Status: works, 100% tested
     *
     * Adds or update Category in database according received CategoryDto
     * @param categoryDto data for creating Category, which will be added to database
     * @return list CategoryDto with updated info or null when something happens
     */

    @Transactional
    public Category saveOrUpdate(final CategoryDto categoryDto){
        try {
            Category result = null;
            Category category = new Category();
            category.setIdCategory(categoryDto.getIdCategory());
            category.setCategoryName(categoryDto.getCategoryName());
            category.setCategoryNumber(categoryDto.getCategoryNumber());
            category.setParent(categoryDao.findById(categoryDto.getParentId()));
            categoryDao.renumberSameLevelCategories(category.getParent(), category.getCategoryNumber(), 1);
            categoryDao.saveOrUpdate(category);
            result = category;
            return result;
        } catch (Exception e) {
            throw new CategoryManagementException(e);
        }
    }

    /**
     * Marks as deleted Category with id from DTO, and also all children Categories
     * @param deleteCategoryDto
     * @return
     */
@Transactional
    public void deleteCategory(final DeleteCategoryDto deleteCategoryDto) {
        Category category = getCategoryById(deleteCategoryDto.getIdCategory());
        if (category == null) {
            throw new NoSuchCategoryException("No such Category with id: " + deleteCategoryDto.getIdCategory());
        }
        categoryDao.delete(getCategoryListForDelete(category));
    }

    /** Status: works, 100% tested
     *
     * Returns Category with passed id
     * @param id id for searching Category
     * @return finded Category or null, if Category with passed id not exist
     */
    @Transactional
    public Category getCategoryById(final long id){
        Category category = categoryDao.findById(id);
        if (category == null) {
            throw new NoSuchCategoryException("No such Category with id: " + id);
        }
        return category;
    }

    /** Status: works, 100% tested
     *
     * Returns name of Category with passed id
     * @param id id for searching Category
     * @return finded Category's name or throws NoSuchCategoryException if Category with passed id not exist
     * @see NoSuchCategoryException
     */

    @Transactional
    public String getCategoryNameById(final long id){
        Category category = categoryDao.findById(id);
        if (category == null) {
            throw new NoSuchCategoryException("No such Category with id: " + id);
        }
        return category.getCategoryName();
    }

    /**
     * Returns list of children Categories
     *
     * @param id id of Category which selected as parent for search
     * @return list of children Categories
     */
    @Transactional
    public List<Category> getAllCategoriesForParent(final Long id){
        Category parent = null;
        if (id != null && id != 0) {
            parent = categoryDao.findById(id);
            if (parent == null){
                return new ArrayList<>();
            }
        }
        return categoryDao.findAllCategoriesForParent(parent);
    }

    /**
     *  Returns List<Category> for building such UI elements as a breadcrumbs
     * @param id id of current Category for building Category's breadcrumbs
     * @return List<Category> with path to current Category
     */
    @Transactional
    public List<Category> getBreadcrumbsForId(final Long id){
        List<Category> result = new ArrayList<>();
        Long currentId = id;
        while (currentId != null){
            Category category = categoryDao.findById(currentId);
            if (category == null) {
                throw new NoSuchCategoryException("No such Category with id: " + id);
            }
            result.add(category);
            currentId = category.getParent() == null ? null : category.getParent().getIdCategory();
        }
        return result;
    }

    private List<Category> getCategoryListForDelete(Category category) {
        List<Category> listForDelete = new ArrayList<>();
        List<Category> listForSearch = new ArrayList<>();
        listForDelete.add(category);
        listForSearch.add(category);
        do {
            List<Category> findedCategories = new ArrayList<>();
            for (Category cat:listForSearch) {
                findedCategories.addAll(categoryDao.findAllCategoriesForParent(cat));
            }
            if (findedCategories.size() == 0) {
                break;
            }
            listForDelete.addAll(findedCategories);
            listForSearch = findedCategories;
        } while (true);
        return listForDelete;
    }

    @Transactional
    public CategoryContentSummaryDto getCategorySummaryDto(final long id){
        Category category = categoryDao.findById(id);
        if (category == null) {
            throw new NoSuchCategoryException("No such Category with id: " + id);
        }

        CategoryContentSummaryDto summaryDto = new CategoryContentSummaryDto();
        List<Category> categoryList = getCategoryListForDelete(category);
        summaryDto.setChildrenCount(categoryList.size() - 1); // exclude itself
        summaryDto.setShopItemsCount(shopItemService.getCountShopItemsInCategories(categoryList));
        return summaryDto;
    }

}
