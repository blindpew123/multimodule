package tk.blindpew123.services.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import tk.blindpew123.services.UserService;
import tk.blindpew123.services.dto.UserRegisterDto;

@Component
public class UserValidator implements Validator {

    private static final int MAX_FIELD_LENGTH = 45;
    private static final int MIN_PASS_LENGTH = 6;

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserRegisterDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserRegisterDto user = (UserRegisterDto) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty");
        if (user.getUserName().length() > MAX_FIELD_LENGTH) {
            errors.rejectValue("userName", "Size.userForm.userName");

        }

        // TODO: email format validation

        if (userService.findByEmailAsLogin(user.getUserName()) != null) {
            errors.rejectValue("userName", "Duplicate.userForm.userName");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < MIN_PASS_LENGTH || user.getPassword().length() > MAX_FIELD_LENGTH) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
