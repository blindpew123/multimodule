package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.validation.BindingResult;
import tk.blindpew123.services.dao.*;
import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.*;
import tk.blindpew123.services.message.MessageSender;
import tk.blindpew123.services.validators.OrderFormValidator;

import javax.transaction.Transactional;
import java.security.Principal;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderService {

    private static final Logger logger = LogManager.getLogger(OrderService.class.getName());
    private static final String QUEUE_MESSAGE = "UPDATE!";

    @Autowired
    private UserService userService; // получить user
    @Autowired
    private CustomerService customerService; // имя фамилия и адреса
    @Autowired
    private CartService cartService; // может будем вносить изменения
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private PaymentTypeDao paymentTypeDao;

    @Autowired
    private ShippingDao shippingDao;

    @Autowired
    private OrderStatusDao orderStatusDao;

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    private ShopItemService shopItemService;

    @Autowired
    private OrderFormValidator orderFormValidator;

    @Autowired
    private MessageSender messageSender;

    @Transactional
    public OrderProcessDto getOrderProcessDto(final CartListDto cartListDto, final Principal principal){
        OrderProcessDto processDto = new OrderProcessDto();

        processDto.setCustomer(customerService.getCustomerByUser(
                userService.findByEmailAsLogin(principal.getName())
        ));
        Hibernate.initialize(processDto.getCustomer().getAddresses());
        processDto.setCustomerAddresses(processDto.getCustomer().getAddresses());
        boolean hasPrimary = false;
        for (Address address : processDto.getCustomerAddresses()){
            if (address.isPrimaryAdr()){
                hasPrimary = true;
                break;
            }
        }
        if (!hasPrimary && processDto.getCustomerAddresses().size() > 0){
           processDto.getCustomerAddresses().get(0).setPrimaryAdr(true);
        }
        processDto.setPaymentTypes(paymentTypeDao.findAll());
        processDto.setShippings(shippingDao.findAll());
        CartListDto cartListNew = new CartListDto();
        cartListNew.setCartItems(new ArrayList<>(cartListDto.getCartItems()));
        cartService.updateTotal(cartListNew);
        processDto.setCartListDto(cartListNew);
        return processDto;
    }

    @Transactional
    public Order processOrder(final OrderProcessDto processDto,
                              final BindingResult bindingResult,
                              final Principal principal) {

        Order order = null;
        // мы здесь, значит у нас нет проблем с адресом и пейментом, отрицательным количеством.
        // нулевое количество пересчитывается на предыдущей проверке
        // Здесь мы проверяем на присутствие в базе наличие товаров select for update

        orderFormValidator.validate(processDto, bindingResult);
        if (bindingResult.hasErrors()){
            return null;
        }

        // prepare ids for select and map for two lists comparing

        Object[] itemsFromCartIds = new Object[processDto.getCartListDto().getCartItems().size()];
        Map<Long, CartItemDto> mapCartItems = new HashMap<>();
        for (int i = 0; i < processDto.getCartListDto().getCartItems().size(); i++) {
            itemsFromCartIds[i] = processDto.getCartListDto().getCartItems().get(i).getIdShopItem();
            mapCartItems.put(processDto.getCartListDto().getCartItems().get(i).getIdShopItem(),
                    processDto.getCartListDto().getCartItems().get(i));
        }

        List<ShopItem> shopItemsForOrder = shopItemService.getAllShopItemForCreatingOrder(itemsFromCartIds);

        logger.info("Start process order");

        // Final checking availability ShopItems in stock for each CartItem
        for (int i = 0; i < shopItemsForOrder.size(); i++) {

            if (mapCartItems.get(shopItemsForOrder.get(i).getIdShopItem()).getQnty()
                    > shopItemsForOrder.get(i).getQnty()) {
                //error
                int errPosition = processDto.getCartListDto().getCartItems()
                        .lastIndexOf(mapCartItems.get(shopItemsForOrder.get(i).getIdShopItem()));
                logger.info("Order validate: found error: qty less than expected "
                        + shopItemsForOrder.get(i).getItemName());
                bindingResult.rejectValue("cartListDto.cartItems[" + errPosition + "].qnty",
                        "QtyErr.cart.cartItems.qnty", "" + shopItemsForOrder.get(i).getQnty());

            } else {
                // decrease available qty on stock
                shopItemsForOrder.get(i).setQnty(shopItemsForOrder.get(i).getQnty()
                        - mapCartItems.get(shopItemsForOrder.get(i).getIdShopItem()).getQnty());
                logger.info("Decreasing stock for " + shopItemsForOrder.get(i).getItemName());

                // processed
                cartService.deleteCartItemById(mapCartItems.get(shopItemsForOrder.get(i).getIdShopItem()).getIdCartItem());
                mapCartItems.remove(shopItemsForOrder.get(i).getIdShopItem());
            }
        }


        if (mapCartItems.size() > 0) { // something from cart not found on stock
           // rollback tx
            logger.info("Rollback");
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

        } else { // creating order
            logger.info("Creating order");
            order = new Order();
            order.setCustomer(customerService.getCustomerByUser(userService.findByEmailAsLogin(principal.getName()))); // TODO watch! detached Entity.
            order.setAddress(customerService.getAddressById(processDto.getSelectedAddress()));
            order.setOrderDate(LocalDateTime.now());
            PaymentType paymentType = paymentTypeDao.findById(processDto.getSelectedPayment());
            order.setPaymentType(paymentType);
            // different OrderStatuses for different PaymentType
            if (paymentType.getIdPaymentType() == PaymentType.Payment.CASH.getValue()) { //TODO may be only one status
                order.setOrderStatus(getOrderStatusById(OrderStatus.Status.PROCESSING.getValue()));
            } else {
                order.setOrderStatus(getOrderStatusById(OrderStatus.Status.AWAITING_PAYMENT.getValue()));
            }
            order.setShipping(shippingDao.findById(processDto.getShipping()));
            orderDao.saveOrUpdate(order);
            for (CartItemDto cartItemDto : processDto.getCartListDto().getCartItems()) {
                OrderItem orderItem = new OrderItem();
                orderItem.setOrder(order);
                orderItem.setItemPrice(shopItemService.getShopItemById(cartItemDto.getIdShopItem()).getPrice());
                orderItem.setQty(cartItemDto.getQnty());
                orderItem.setShopItem(shopItemService.getShopItemById(cartItemDto.getIdShopItem()));
                orderItemDao.saveOrUpdate(orderItem);
                order.getOrderItemList().add(orderItem);
            }
        }

        return order;
    }

    public List<Order> getAllOrderForCustomer(final Customer customer){
        List<Order> orders = orderDao.findAllOrderByCustomer(customer);
        return orders == null ? new ArrayList<>() : orders;
    }

    @Transactional
    public OrdersHistoryDto getOrdersHistoryDto(final OrdersHistoryDto ordersHistoryDto,
                                                final Principal principal){
        List<Order> orders = getAllOrderForCustomer(
                customerService.getCustomerByUser(
                        userService.findByEmailAsLogin(principal.getName())));

        ordersHistoryDto.setOrderList(orders);
        return ordersHistoryDto;
    }

    @Transactional
    public OrderStatus getOrderStatusById(final long id){
        return orderStatusDao.findById(id);
    }

    @Transactional
    public List<Order> getOrdersBetweenDate(final LocalDateTime d1, final LocalDateTime d2){
        return orderDao.findAllOrdersBetweenDates(d1, d2);
    }

    @Transactional
    public List<OrderAdminDto> getOrderAdminDtoList(final LocalDateTime d1, final LocalDateTime d2){
        List<OrderAdminDto> orderAdminDtoList = new ArrayList<>();
        List<Order> orderList = getOrdersBetweenDate(d1, d2);
        for (Order order: orderList){
            OrderAdminDto orderAdminDto = new OrderAdminDto();
            orderAdminDto.setIdOrder(order.getIdOrder());
            orderAdminDto.setCustomerFirstName(order.getCustomer().getFirstName());
            orderAdminDto.setCustomerLastName(order.getCustomer().getLastName());
            orderAdminDto.setOrderDate(order.getOrderDate());
            orderAdminDto.setPaymentType(order.getPaymentType().getPaymentName());
            orderAdminDto.setShipping(order.getShipping().getShippingName());
            orderAdminDto.setOrderStatus(order.getOrderStatus().getIdOrderStatus());
            orderAdminDto.setAddress(order.getAddress().toString());
            orderAdminDto.setOrderItems(new ArrayList<>());
            for (OrderItem orderItem : order.getOrderItemList()){
                OrderItemAdminDto orderItemAdminDto = new OrderItemAdminDto();
                orderItemAdminDto.setShopItem(orderItem.getShopItem().getItemName());
                orderItemAdminDto.setItemPrice(orderItem.getItemPrice());
                orderItemAdminDto.setQty(orderItem.getQty());
                orderAdminDto.getOrderItems().add(orderItemAdminDto);
            }
            orderAdminDtoList.add(orderAdminDto);
        }

        return orderAdminDtoList;
    }
    @Transactional
    public List<OrderStatus> getAllOrderStatuses(){
        return orderStatusDao.findAll();
    }

    @Transactional
    public void orderStatusUpdate(final OrderStatusUpdateDto orderStatusUpdateDto){
        Order order = orderDao.findById(orderStatusUpdateDto.getIdOrder());
        order.setOrderStatus(orderStatusDao.findById(orderStatusUpdateDto.getIdStatus()));
    }

    @Transactional
    public Double getTotalForPeriod(final LocalDateTime d1, final LocalDateTime d2){
        return orderDao.findTotalForPeriod(d1, d2);
    }

    public void sendMessage(){
        try{
            messageSender.sendMessage(QUEUE_MESSAGE);
        } catch (Exception e){
            logger.debug("XXXXXX JMS error! " + e.getMessage());
        }
    }



}
