package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tk.blindpew123.exceptions.NoSuchCustomerException;
import tk.blindpew123.services.dao.AddressDao;
import tk.blindpew123.services.dao.CustomerDao;
import tk.blindpew123.services.dao.OrderDao;
import tk.blindpew123.services.dto.Top10CustomerDto;
import tk.blindpew123.services.dto.UserProfileEditDto;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.User;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    private static final Logger logger = LogManager.getLogger(CustomerService.class.getName());

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private AddressDao addressDao;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;



    /**
     * Returns Customer for User. When there aren't Customer for User it will be throw NoSuchCustomerException
     * @param user User for searching
     * @return Customer if exist
     * @see NoSuchCustomerException
     */

    @Transactional
    public Customer getCustomerByUser(final User user){
        return customerDao.findCustomerByUser(user);
    }

    /**
     * Finds address for id
     * @param id Address' id
     * @return Address or null when there aren't Address for that id
     */

    @Transactional
    public Address getAddressById(final long id){
        return addressDao.findById(id);
    }

    /**
     * Returns Customer as DTO with filled fields for user
     * @param userProfileEditDto UserProfileEditDto for filling
     * @param user which matches Customer's database record
     * @return filled Dto, or empty Dto, when Customer not exist
     */

    @Transactional
    public UserProfileEditDto getDtoWithCustomerProfile(final UserProfileEditDto userProfileEditDto, final User user){
        Customer customer = null;
        try {
            customer = getCustomerByUser(user);
        } catch (NoSuchCustomerException e){
            logger.debug(e);
            customer = new Customer();
            customer.setUser(user);
            customer.setAddresses(new ArrayList<>());
        }
        // check addresses is not init
        userProfileEditDto.setFirstName(customer.getFirstName() == null ? "" : customer.getFirstName());
        userProfileEditDto.setLastName(customer.getLastName() == null ? "" : customer.getLastName());
        userProfileEditDto.setUserName(user == null ? "" : user.getEmailAsLogin());
        userProfileEditDto.setOldUserName(user == null ? "" : user.getEmailAsLogin());
        Hibernate.initialize(customer.getAddresses());
        userProfileEditDto.setAddresses(customer.getAddresses());
        userProfileEditDto.setBirthDate(customer.getBirthDate() == null ? "" : customer.getBirthDate().toString());
        userProfileEditDto.setIdUser(user == null ? 0 : user.getUser());
        userProfileEditDto.setEmptyAddress(new Address());
        userProfileEditDto.setIdCustomer(customer.getIdCustomer());
        return userProfileEditDto;
    }

    /**
     *  Saves data from Dto to Customer. Updates password (when it changes) and/or username in User
     * @param userProfileEditDto Dto with data for saving
     * @return true, when userName was changed and user has need to re-login
     */

    @Transactional
    public boolean saveOrUpdateCustomerProfile(final UserProfileEditDto userProfileEditDto){
        boolean logoutRequired = false;
        User user = userService.findById(userProfileEditDto.getIdUser());
        Customer customer = null;
        try {
            customer = getCustomerByUser(user);
        } catch (NoSuchCustomerException e){
            customer = new Customer();
            customer.setUser(user);
        }

        customer.setFirstName(userProfileEditDto.getFirstName());
        customer.setLastName(userProfileEditDto.getLastName());
        if (customer.getIdCustomer() == 0){
            customerDao.saveOrUpdate(customer);
        }
        saveOrUpdateAddresses(userProfileEditDto.getAddresses(), customer);

        customer.setBirthDate(userProfileEditDto.getBirthDate().isEmpty() ?
                null :  LocalDate.parse(userProfileEditDto.getBirthDate()));

        if (!user.getEmailAsLogin().equals(userProfileEditDto.getUserName())){
            logoutRequired = true;
            user.setEmailAsLogin(userProfileEditDto.getUserName());
        }

        if(!userProfileEditDto.getNewPassword().isEmpty()){
            user.setPassword(bCryptPasswordEncoder.encode(userProfileEditDto.getNewPassword()));
            userProfileEditDto.setNewPassword("");
            userProfileEditDto.setOldPassword("");
            userProfileEditDto.setNewPasswordConfirm("");
        }
        return logoutRequired;
    }

    @Transactional
    public String removeAddress(final List<Address> addressList, final int pos){
        List<Order> list = orderDao.findOrdersWithAddress(addressList.get(pos));
        Address address = addressList.remove(pos);
        Address targetAddress = addressDao.findById(address.getIdAddress());
        String resultMessage = "Aдрес " + address.toString() + " успешно удален";
        if (list != null && list.size() > 0) {
            targetAddress.setCustomer(null);     // address used for order(s). Can't delete from db
        } else {
            addressDao.delete(targetAddress); // useless address. just delete
        }
        return resultMessage;
    }

    @Transactional
    public void clearPrimary(final List<Address> addressList){
        for (Address address : addressList){
            Address tmp = addressDao.findById(address.getIdAddress());
            tmp.setPrimaryAdr(false);
            address.setPrimaryAdr(false);
      //      customerDao.saveOrUpdateAddress(tmp);
        }
    }

    @Transactional
    public String setPrimary(final Address address){
        if (address == null){
            return "Address doesn't exist";
        }
        Address tmp = addressDao.findById(address.getIdAddress());
        tmp.setPrimaryAdr(true);
        address.setPrimaryAdr(true);
     //   customerDao.saveOrUpdateAddress(tmp);
        return "Address " + address.toString() + " set as primary";
    }

    @Transactional
    public void saveOrUpdateAddresses(List<Address> addresses, Customer customer){
        if (addresses == null || customer == null){
            return;
        }

        for(Address address : addresses){
            address.setCustomer(customer);
            addressDao.saveOrUpdate(address);
        }
    }

    @Transactional
    public void saveOrUpdateAddress(final Address newAddress, final long userId){
        Customer customer = customerDao.findCustomerByUser(userService.findById(userId));
        newAddress.setCustomer(customer);
        addressDao.saveOrUpdate(newAddress);
    }

    @Transactional
    public List<Top10CustomerDto> getTop10CustomerTotal(){
        return customerDao.findTop10CustomerTotal();
    }
}
