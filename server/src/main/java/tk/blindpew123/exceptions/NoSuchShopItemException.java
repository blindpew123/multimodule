package tk.blindpew123.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception signals that attempt to find ShopItem with selected id has failed.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such ShopItem")
public class NoSuchShopItemException extends RuntimeException {
    public NoSuchShopItemException(){
        super();
    }
    public NoSuchShopItemException(String message){
        super(message);
    }
    public NoSuchShopItemException(Exception e){
        super(e);
    }
}
