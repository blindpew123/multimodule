package tk.blindpew123.exceptions;

/**
 * This exception signals that attempt perform some operation with selected Parameter has failed.
 */

public class ParameterManagementException extends RuntimeException{
    public ParameterManagementException(){ }
    public ParameterManagementException(String message){
        super(message);
    }
    public ParameterManagementException(Exception e){
        super(e);
    }
}
