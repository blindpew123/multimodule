package tk.blindpew123.exceptions;

import tk.blindpew123.services.entities.User;

/**
 * This exception signals that attempt to find Customer for selected user has failed.
 */

public class NoSuchCustomerException extends RuntimeException{
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
