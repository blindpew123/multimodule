package tk.blindpew123.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception signals that attempt to find category with selected id has failed.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Category")
public class NoSuchCategoryException extends RuntimeException {

    public NoSuchCategoryException(){}
    public NoSuchCategoryException(String message){
        super(message);
    }
    public NoSuchCategoryException(Exception e){
        super(e);
    }
}
