package tk.blindpew123.exceptions;

/**
 * This exception signals that attempt perform some operation with selected category has failed.
 */
public class CategoryManagementException extends RuntimeException {
    public CategoryManagementException(){ }
    public CategoryManagementException(String message){
        super(message);
    }
    public CategoryManagementException(Exception e){
        super(e);
    }

}
