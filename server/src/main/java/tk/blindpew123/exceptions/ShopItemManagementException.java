package tk.blindpew123.exceptions;

/**
 * This exception signals that attempt perform some operation with selected ShopItem has failed.
 */
public class ShopItemManagementException extends RuntimeException {
    public ShopItemManagementException(){ }
    public ShopItemManagementException(String message){
        super(message);
    }
    public ShopItemManagementException(Exception e){
        super(e);
    }
}
