-- important --

LOCK TABLES `ishopdb`.`role` WRITE;
INSERT INTO `ishopdb`.`role` VALUES (1,'ROLE_USER');
INSERT INTO `ishopdb`.`role` VALUES (2,'ROLE_ADMIN');
UNLOCK TABLES;

use ishopdb;

insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Cards', NULL, 1, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Accessories', NULL, 2, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('MTG Books', NULL, 3, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Dices', '2', 1, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Sleeves', '2', 2, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Oversize Cards', '1', 1, 0);

insert into parametertype(ParameterTypeName)
values ('String');
insert into parametertype(ParameterTypeName)
values ('Number');

insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Power', 2, 1, 1);
insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Toughness', 2, 1, 2);
insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Mana Cost', 1, 1, 3);
insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Sides', 2, 4, 1);



insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Accursed Spirit',1.5, 1, 10, 0.03, 7, 'm15/85.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Act of Treason',2, 1, 10, 0.03, 0, 'm14/125.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Black Dice d6',2, 4, 10, 0.03, 1, 'dices/6black.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Air Servant',.5, 1, 10, 0.03, 1, 'm14/42.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Child of Night',3, 1, 10, 0.03, 2, 'm14/89.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Dark Prophecy',5, 1, 10, 0.03, 2, 'm14/93.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Magma Spray',1, 1, 10, 0.03, 2, 'jou/103.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Unmoored Ego',0.99, 1, 10, 0.03, 2, 'grn/212.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('High Priest of Penance',1.0, 1, 10, 0.03, 7, 'c18/183.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Finest Hour',0.99, 1, 10, 0.03, 300, 'c18/180.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Inspiring Commander',0.1, 1, 10, 0.03, 4, 'ana/5.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Banefire',2, 1, 10, 0.03, 100, 'm19/130.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Liliana`s Contract',0.1, 1, 10, 0.03, 6, 'm19/107.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Teferi, Mage of Zhalfir',1, 1, 10, 0.03, 4, 'ima/75.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Skyknight Legionnaire',.99, 1, 10, 0.03, 4, 'grn/198.jpg');

insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '3', 1);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '2', 1);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '3B', 1);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '2R', 2);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (4, '6', 3);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '4', 4);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '3', 4);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '4U', 4);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '2', 5);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '1', 5);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '1B', 5);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, 'BBB', 6);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, 'R', 7);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '1UB', 8);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '1', 9);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '1', 9);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, 'WB', 9);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '2GWU', 10);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '1', 11);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '4', 11);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '4WW', 11);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, 'XR', 12);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '3BB', 13);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '3', 14);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '4', 14);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '2UUU', 14);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '2', 15);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '2', 15);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '1RW', 15);


insert into user(emailAsLogin, password) values (1, '$2a$11$NADuousojzp8hVOcnjqFge.YrXdj5pQz0qejZ.GWYa6tgjAggV2em');
insert into user(emailAsLogin, password) values (0, '$2a$11$NADuousojzp8hVOcnjqFge.YrXdj5pQz0qejZ.GWYa6tgjAggV2em');
insert into user(emailAsLogin, password) values (9, '$2a$11$NADuousojzp8hVOcnjqFge.YrXdj5pQz0qejZ.GWYa6tgjAggV2em');

insert into user_role(user_id, role_id) values (1,1);
insert into user_role(user_id, role_id) values (2,2);
insert into user_role(user_id, role_id) values (3,1);

insert into customer(firstName, lastName, birthday, user) values ('Сергей', 'Яковлев', null, 1);
insert into customer(firstName, lastName, birthday, user) values ('Вася', 'Куликов', null, 3);

insert into address(Country, City, PostCode, Street, Building, Apt, Customer, PrimaryAdr)
VALUES ('Россия', 'Санкт-Петербург', 198000, 'Невский пр.', 2, 101, 1, 0);

insert into address(Country, City, PostCode, Street, Building, Apt, Customer, PrimaryAdr)
VALUES ('Россия', 'Санкт-Петербург', 195000, 'Московский пр.', 101, 300, 1, 1);

insert into address(Country, City, PostCode, Street, Building, Apt, Customer, PrimaryAdr)
VALUES ('Россия', 'Мурманск', 100000, 'Полярный пр.', 10, 30, 2, 1);

insert into shipping (ShippingName)
values ('Courier Delivery');
insert into shipping (ShippingName)
values ('Russian Post');

insert into paymenttype (PaymentName) VALUES ('Cash');
insert into paymenttype (PaymentName) VALUES ('VISA/MasterCard');

insert into orderstatus (OrderStatusName) VALUES ('Processing');
insert into orderstatus (OrderStatusName) VALUES ('Awaiting Payment');
insert into orderstatus (OrderStatusName) VALUES ('Shipped');
insert into orderstatus (OrderStatusName) VALUES ('Delivered');
insert into orderstatus (OrderStatusName) VALUES ('Return');

insert into customerorder (Customer, Address, PaymentType, Shipping, OrderStatus, OrderDate)
values (1,1,1,1,1,'2018-09-24 15:40:03');
insert into customerorder (Customer, Address, PaymentType, Shipping, OrderStatus, OrderDate)
values (2,3,1,1,1,'2018-09-24 15:40:03');

insert into orderitem (CustOrder, idOrderItem, ShopItem, Qty, ItemPrice)
values (1,1,1,50,2);
insert into orderitem (CustOrder, idOrderItem, ShopItem, Qty, ItemPrice)
values (1,2,2,20,3);
insert into orderitem (CustOrder, idOrderItem, ShopItem, Qty, ItemPrice)
values (2,3,2,50,1);



