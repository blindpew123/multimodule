<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!doctype html>
<html style="height:100%;">
<c:if test="${empty pageElementDescriptor}">
    <jsp:useBean id="pageElementDescriptor" class="tk.blindpew123.services.dto.PageElementDescriptor"/>
    <c:set target="${pageElementDescriptor}" property="title" value="MTG Shop"/>
    <c:set target="${pageElementDescriptor}" property="contentURI" value="WEB-INF/jsp/views/news.jsp"/>
</c:if>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" type="text/css"> -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <!--  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"> -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/all.css">
    <link href="${pageContext.request.contextPath}/resources/css/mdb.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/common.css" rel="stylesheet">
    <c:if test='${fn:contains(pageElementDescriptor.contentURI, "/shop")}'>
        <c:url value="${pageContext.request.contextPath}css/sidebar_select.css" var="sidebarFilterCss"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sidebar_select.css">
    </c:if>
    <title><c:out value="${pageElementDescriptor.title}"/></title>
</head>

<body
        <c:if test='${fn:contains(pageElementDescriptor.title, "MTG shop")}'>
            style="height:100%;
            background: url('${(pageContext.request.contextPath) += "/resources/img/assets/back.jpg"}');
            background-repeat: no-repeat;
            background-size: auto;"
        </c:if>
>
<jsp:include page="WEB-INF/jsp/views/menu.jsp" flush="true"/>
<c:if test='${fn:contains(pageElementDescriptor.contentURI, "shopItemsList")}'>
    <jsp:include page="WEB-INF/jsp/views/sidebarFilter.jsp"/>
</c:if>
<div style="margin-bottom: 90px">
    <jsp:include page="${pageElementDescriptor.contentURI}" flush="true"/>
    <jsp:include page="WEB-INF/jsp/views/footer.jsp"/>
</div>
<jsp:include page="WEB-INF/jsp/views/cart.jsp"/>
<c:if test='${fn:contains(pageElementDescriptor.contentURI, "shopItemsList")}'>
    <script>

        /* Open the sidenav */
        function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
        }

        /* Close/hide the sidenav */
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }


    </script>
</c:if>
<script>
    function supports_html5_storage() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    }

    function getCart() {
        if (supports_html5_storage()) {
            var cart = localStorage.getItem("cart");
            console.log(cart);
        } else return null;
    }

    window.addEventListener("DOMContentLoaded", function () {
        console.log("DOM ready");
        getCart();

        if ($('[data-cart-error-msg]').length) {
            $("#modalCartForm").modal()
        }

    }, false);


</script>
</body>
</html>