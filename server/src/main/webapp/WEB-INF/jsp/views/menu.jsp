<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="currentURI" value="${requestScope['javax.servlet.forward.servlet_path']}"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="numLastBread" value="${fn:length(pageElementDescriptor.breadCrumbs)}" />
<c:set var="cartColor" value="${'text-white'}"/>
<c:if test="${fn:length(cart.cartItems) > 0}">
    <c:set var="cartColor" value="${'text-warning'}"/>
</c:if>
<nav class="navbar fixed-top navbar-dark bg-dark navbar-expand-md bg-faded justify-content-center">
    <a href="/index.jsp" class="navbar-brand d-flex w-50 mr-auto">MTG Shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse w-100" id="collapsingNavbar">
        <ul class="navbar-nav w-100 justify-content-center">
            <li class="nav-item  ${numLastBread > 0 ? '': 'd-none'}">
                <a class="nav-link"
                        href="${numLastBread == 1 ? '/index.jsp' : (pageContext.request.contextPath).concat(pageElementDescriptor.breadCrumbs[numLastBread-2].url)}">
                    <i class="fas fa-arrow-up"></i>
                </a>
            </li>

            <c:forEach items="${pageElementDescriptor.currentLevelMenu}" var="catMenu" varStatus="status">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/shop/list/${catMenu.idCategory}/0">${catMenu.categoryName}</a>
            </li>
            </c:forEach>

        </ul>
        <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
            <li class="nav-item">
                <c:if test="${empty pageElementDescriptor.userName}"><a class="nav-link" href="${pageContext.request.contextPath}/login" id="not-user-info">Log in</a></c:if>
                <c:if test="${not empty pageElementDescriptor.userName}">
                    <a class="nav-link" href="${pageContext.request.contextPath}/customer/showUserInformation" id="user-info"><i class="fas fa-user-alt"></i>&nbsp;${pageElementDescriptor.userName}</a></c:if>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link ${cartColor}" data-toggle="modal"
                   data-target="#modalCartForm"><i class="fas fa-shopping-cart mr-1"></i>Shopping cart</a>
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center">
    <a href="/" class="navbar-brand d-flex w-50 mr-auto">Navbar 3</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse w-100" id="collapsingNavbar">
        <ul class="navbar-nav w-100 justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="#"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"></a>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
            <li class="nav-item">
                <a class="nav-link" href="#"></a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link" data-toggle="modal" data-target="#modalCartForm"><i
                        class="fas fa-shopping-cart"></i></a>
            </li>
        </ul>
    </div>
</nav>


