<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:url value="/customer/processUserInformation" var="userInfo"/>
<c:set var="newAdrNotOk" value="${not empty addressMessage &&
fn:contains(addressMessage, 'ошибки')}"/>

<div class="container">
    <div class="row">
        <div class="col col-md-9">

            <form:form method="POST" action="${userInfo}" acceptCharset="UTF-8" modelAttribute="userProfileEditDto">
                <form:input path="idUser" hidden="true"/>
                <form:input path="oldUserName" hidden="true"/>
                <div class="row">
                    <div class="col">
                        <div class="md-form form-group mt-5">
                            <form:input cssClass="form-control validate" cssErrorClass="invalid" path="firstName"
                                        type="text"
                                        required="true"/>
                            <form:label path="firstName">First name</form:label>
                            <form:errors cssClass="text-danger" path="firstName"/>
                        </div>
                        <div class="md-form form-group mt-5">
                            <form:input cssClass="form-control validate" cssErrorClass="invalid" path="lastName"
                                        type="text"
                                        required="true"/>
                            <form:label path="lastName">Last name</form:label>
                            <form:errors cssClass="text-danger" path="lastName"/>
                        </div>

                        <div class="md-form">
                            <form:input cssClass="form-control validate" cssErrorClass="invalid" path="userName"
                                        required="true"
                                        type="text"/>
                            <form:label
                                    path="userName">Username</form:label>
                            <form:errors cssClass="text-danger" path="userName"/>

                        </div>
                        <div class="md-form">
                            <form:input cssClass="form-control validate" cssErrorClass="invalid" path="oldPassword"
                                        type="password"/>
                            <form:label path="oldPassword">Current password</form:label>
                            <form:errors cssClass="text-danger" path="oldPassword"/>

                        </div>
                        <div class="md-form">
                            <form:input cssClass="form-control validate" cssErrorClass="invalid" path="newPassword"
                                        type="password"/>
                            <form:label path="newPassword">New password</form:label>
                            <form:errors pcssClass="text-danger" ath="newPassword"/>

                        </div>
                        <div class="md-form">
                            <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                        path="newPasswordConfirm"
                                        type="password"/>
                            <form:label path="newPasswordConfirm">Confirm new password</form:label>
                            <form:errors cssClass="text-danger" path="newPasswordConfirm"/>

                        </div>
                        <div class="md-form">
                            <span>Дата рождения</span>
                            <form:input cssClass="form-control validate" path="birthDate" type="date"/>
                            <!--   <form:label path="birthDate">Birthday</form:label>-->
                            <form:errors cssClass="text-danger" path="birthDate"/>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <c:if test="${not empty addressMessage}">
                            <div>
                                <p class="${newAdrNotOk ? 'text-danger' : 'text-success'}">${addressMessage}</p>
                            </div>
                        </c:if>
                        <c:forEach items="${userProfileEditDto.addresses}" var="address" varStatus="status">

                            <button class="btn btn-deep-orange"
                                    name="delId" value="${status.index}" formnovalidate>Delete address
                            </button>
                            <button class="btn btn-deep-orange"
                                    name="primaryId" value="${status.index}"
                                    ${address.primaryAdr ? 'disabled' : ''}
                                    formnovalidate>Make primary
                            </button>

                            <div class="md-form">
                                <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                            path="addresses[${status.index}].country" required="true" type="text"/>
                                <form:label path="addresses[${status.index}].country">Country</form:label>
                                <form:errors cssClass="text-danger" path="addresses[${status.index}].country"/>
                            </div>
                            <div class="md-form">
                                <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                            path="addresses[${status.index}].city" required="true" type="text"/>
                                <form:label path="addresses[${status.index}].city">City</form:label>
                                <form:errors cssClass="text-danger" path="addresses[${status.index}].city"/>
                            </div>
                            <div class="md-form">
                                <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                            path="addresses[${status.index}].postCode" required="true" type="text"/>
                                <form:label path="addresses[${status.index}].postCode">Postcode</form:label>
                                <form:errors cssClass="text-danger" path="addresses[${status.index}].postCode"/>
                            </div>
                            <div class="md-form">
                                <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                            path="addresses[${status.index}].street" required="true" type="text"/>
                                <form:label path="addresses[${status.index}].street">Улица</form:label>
                                <form:errors cssClass="text-danger" path="addresses[${status.index}].street"/>
                            </div>
                            <div class="md-form">
                                <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                            path="addresses[${status.index}].building" required="true" type="text"/>
                                <form:label path="addresses[${status.index}].building">Building</form:label>
                                <form:errors cssClass="text-danger" path="addresses[${status.index}].building"/>
                            </div>
                            <div class="md-form">
                                <form:input cssClass="form-control validate"
                                            path="addresses[${status.index}].apt" required="true" type="text"/>
                                <form:label path="addresses[${status.index}].apt">Apt.</form:label>
                                <form:errors cssClass="text-danger" path="addresses[${status.index}].apt"/>
                            </div>

                        </c:forEach>
                    </div>
                </div>
                <c:if test="${userProfileEditDto.idCustomer != 0}">
                    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header" role="tab" id="headingOne1">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                               aria-expanded="${newAdrNotOk ? 'true' : 'false'}"
                               aria-controls="collapseOne1" class="collapsed">
                                <h5 class="mb-0">
                                    New address <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <div id="collapseOne1" class="collapse ${newAdrNotOk ? 'show' : 'hide'}" role="tabpanel"
                             aria-labelledby="headingOne1"
                             data-parent="#accordionEx">
                            <div class="card-body">
                                <div class="md-form">
                                    <div class="md-form">
                                        <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                                    path="emptyAddress.country" required="true" type="text"/>
                                        <form:label path="emptyAddress.country">Country</form:label>
                                        <form:errors cssClass="text-danger" path="emptyAddress.country"/>
                                    </div>
                                    <div class="md-form">
                                        <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                                    path="emptyAddress.city" required="true" type="text"/>
                                        <form:label path="emptyAddress.city">City</form:label>
                                        <form:errors cssClass="text-danger" path="emptyAddress.city"/>
                                    </div>
                                    <div class="md-form">
                                        <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                                    path="emptyAddress.postCode" required="true" type="text"/>
                                        <form:label path="emptyAddress.postCode">Postcode</form:label>
                                        <form:errors cssClass="text-danger" path="emptyAddress.postCode"/>
                                    </div>
                                    <div class="md-form">
                                        <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                                    path="emptyAddress.street" required="true" type="text"/>
                                        <form:label path="emptyAddress.street">Street</form:label>
                                        <form:errors cssClass="text-danger" path="emptyAddress.street"/>
                                    </div>
                                    <div class="md-form">
                                        <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                                    path="emptyAddress.building" required="true" type="text"/>
                                        <form:label path="emptyAddress.building">Building</form:label>
                                        <form:errors cssClass="text-danger" path="emptyAddress.building"/>
                                    </div>
                                    <div class="md-form">
                                        <form:input cssClass="form-control validate" cssErrorClass="invalid"
                                                    path="emptyAddress.apt" required="true" type="text"/>
                                        <form:label path="emptyAddress.apt">Apt.</form:label>
                                        <form:errors cssClass="text-danger" path="emptyAddress.apt"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                </c:if>
                <button class="btn btn-deep-orange" name="save" formnovalidate>Save changes</button>
            </form:form>
        </div>
        <div class="col mt-3">
            <a href="${pageContext.request.contextPath}/customer/orderHistory"> <button class="btn btn-orange">Orders</button></a>
        <a href="${pageContext.request.contextPath}/logout"><button class="btn btn-outline-orange">Log out</button></a>
        </div>
    </div>
</div>




