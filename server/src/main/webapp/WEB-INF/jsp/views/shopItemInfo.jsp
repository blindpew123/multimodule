<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container mt-5">
    <div class="row">
        <div class="col-3 w-75">
            <img class="img-fluid" src="${fn:contains(shopItemTupleDto.shopItem.imageLink,'http') ? shopItemTupleDto.shopItem.imageLink :
                             (pageContext.request.contextPath) += '/stock/' += shopItemTupleDto.shopItem.imageLink}"
                 alt="${shopItemTupleDto.shopItem.itemName}">
        </div>
        <div class="col">
            <div class="row">
                <div class="col">
                    <h2 class="text-warning font-weight-bold text-left">${shopItemTupleDto.shopItem.itemName}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div>

                        <c:forEach items="${shopItemTupleDto.shopItemNamesAndValues}" var="paramNameValue"
                                   varStatus="index">
                            <div><span>${paramNameValue.name}: </span><span>${paramNameValue.value}</span></div>
                        </c:forEach>
                    </div>
                </div>
                <div class="col">
                    <h5>Price: ${shopItemTupleDto.shopItem.price}</h5>
                    <span>Available:</span><span>${shopItemTupleDto.shopItem.qnty}</span>
                    <form:form method="POST"
                               acceptCharset="UTF-8" modelAttribute="cartItemDto"
                               onsubmit="addToCart()">
                    <div class="form-group ">
                        <form:input path="qnty" type="number" min="0" cssClass="form-control col-2"/>
                        <form:hidden path="idShopItem"/>
                        <form:hidden path="price"/>
                        <button class="btn btn-orange " style="margin-left: -2px">Add to cart</button>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function addToCart() {
        console.log("В корзине")
        return true;
    }

    function addValue() {
        $('#counter').val(function (i, oldval) {
            return parseInt(oldval, 10) + 1;
        });
    }
</script>