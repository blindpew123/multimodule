<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="container mt-5">
    <div class="row">
        <c:url value="/cart/processOrder" var="cartProcessUri"/>
        <form:form method="POST" action="${cartProcessUri}" acceptCharset="UTF-8" modelAttribute="orderDto">
        <div class="col w-75">
            <div class="card card-body">

                <!-- Title -->
                <h3 class="card-header-title mb-3">
                        ${orderDto.customer.firstName +=" " += orderDto.customer.lastName}
                </h3>

                <!-- Card content -->
                <div class="flex-row">
                    <h5 class="text-warning">Delivery address</h5>
                    <!-- Addresses -->
                    <c:if test="${fn:length(orderDto.customerAddresses) == 0}" >
                        <p class="text-danger">You don't have any address</p>
                    </c:if>
                    <c:forEach items="${orderDto.customerAddresses}" var="addresselem" varStatus="status">
                        <div class="custom-control custom-radio w-100 text-md-left">
                            <form:radiobutton cssClass="custom-control-input" path="selectedAddress"
                                              value="${addresselem.idAddress}"
                                              checked="${addresselem.primaryAdr ? 'checked' : '' }"/>
                            <label class="custom-control-label" for="selectedAddress${status.count}">
                                    ${addresselem.country += " " += addresselem.postCode
                                            +=" " += addresselem.city +=" " += addresselem.street += " " += addresselem.building
                                            +=" " += addresselem.apt}</label>
                        </div>
                    </c:forEach>
                    <h5 class="mt-2 text-warning">Shipping</h5>
                    <!-- shippings -->
                    <c:forEach items="${orderDto.shippings}" var="shippingelem" varStatus="status">
                        <div class="custom-control custom-radio w-100 text-md-left">
                            <form:radiobutton cssClass="custom-control-input" path="shipping"
                                              value="${shippingelem.idShipping}"
                                              checked="${status.first ? 'checked' : '' }"/>
                            <label class="custom-control-label" for="shipping${status.count}">
                                    ${shippingelem.shippingName}</label>
                        </div>
                    </c:forEach>
                    <h5 class="mt-2 text-warning">Payment</h5>
                    <!-- Payments -->
                    <c:forEach items="${orderDto.paymentTypes}" var="payelem" varStatus="status">
                        <div class="custom-control custom-radio w-100 text-md-left">
                            <form:radiobutton cssClass="custom-control-input" path="selectedPayment"
                                              value="${payelem.idPaymentType}"
                                              checked="${status.first ? 'checked' : '' }"/>
                            <label class="custom-control-label" for="selectedPayment${status.count}">
                                    ${payelem.paymentName}</label>
                        </div>
                    </c:forEach>
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
            <button class="btn btn-deep-orange mt-2" style="margin-left: -1px" name="makeOrder"
                    ${fn:length(orderDto.cartListDto.cartItems) > 0
                    && empty cartError
                    && fn:length(orderDto.customerAddresses) > 0
                    ? '' : "disabled"}>Make order</button>
            <c:if test="${not empty cartError}">
                <div class="text-danger">Check Shopping cart</div>
            </c:if>
            </form:form>
        </div>
        <div class="col">
            <c:forEach items="${orderDto.cartListDto.cartItems}" var="cartItem" varStatus="status">
                <div class="row">
                    <div class="col w-75">

                        <h4>${cartItem.shopItemName}</h4>
                    </div>
                    <div class="col text-right">
                         <h4>${cartItem.qnty}</h4>
                    </div>
                    <div class="col text-right">
                        <h4>${cartItem.price}</h4>
                    </div>
                </div>
            </c:forEach>
            <div class="row mt-5">
                <div class="col border-top text-right">
                <h3>Итого:&nbsp;${orderDto.cartListDto.total}</h3>
                </div>
            </div>

            <!--
            Итого, доставка бесплатно

            кнопка оплатить
            кнопка, внести изменения -->


        </div>

    </div>
</div>