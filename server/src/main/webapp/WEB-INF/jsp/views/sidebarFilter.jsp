<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<jsp:useBean id="filterCriteriaDto" class="tk.blindpew123.services.dto.FilterCriteriaDto"/>
<c:url value="/shop/list/${currentCategoryId}/0" var="selectFormUri"/>
<div id="mySidenav" class="sidenav" style="opacity: 0.9;">
    <div class="container ">
        <form:form method="GET" action="${selectFormUri}" acceptCharset="UTF-8" modelAttribute="formDto">
            <div class="row">

                <div class="col clearfix">
                    <a style="float:right" href="javascript:void(0)" onclick="closeNav()"><i
                            class="fas fa-times text-danger"></i></a>
                    <H2 class="text-warning">Sorting and Filtering</H2>

                    <div class="row mt-5">
                        <div class="col-2 border-right border-danger mr-5">
                            <span class="text-warning">Sort by</span>
                            <div class="form-group mt-2">
                                <form:select cssClass="form-control" path="sortName">
                                    <form:option value="" label="Without sorting"/>
                                    <form:options items="${formDto.aliases}"/>
                                </form:select>

                            </div>
                        </div>
                        <div class="col-3">
                            <c:forEach items="${formDto.filterList}" var="element" varStatus="status">
                            <c:if test="${status.index>0 && status.index%5 == 0}">
                                </div>
                                <div class="col-3">
                            </c:if>
                            <c:choose>
                                <c:when test="${element.fieldType == 'Number'}">
                                    <span class="text-warning">${element.fieldAlias}</span>
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <form:select path="filterList[${status.index}].mathCondition"
                                                         cssClass="form-control">
                                                <form:options items="${filterCriteriaDto.listConditions}"/>
                                            </form:select>
                                        </div>
                                        <div class="form-group col">
                                            <form:input path="filterList[${status.index}].value" type="number"
                                                        cssClass="form-control"/>
                                        </div>
                                    </div>

                                </c:when>
                                <c:otherwise>
                                    <span class="text-warning">${element.fieldAlias}</span>
                                    <div class="form-group">
                                        <form:input path="filterList[${status.index}].value" cssClass="form-control"/>
                                    </div>
                                </c:otherwise>
                            </c:choose>

                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col clearfix">
                        <span style="float:right">
                            <button class="btn btn-orange btn-block mt-2"
                                    type="submit"
                                    value="Save">Apply
                            </button>
                        </span>
                </div>
            </div>
        </form:form>
    </div>
</div>