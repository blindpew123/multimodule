<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="container">

    <form:form method="POST" modelAttribute="userForm" class="form-signin">
    <h2 class="form-signin-heading">Create an account</h2>
    <spring:bind path="userName">
    <div class="form-group ${status.error ? 'has-error' : ''}">
        <form:input type="text" path="userName" class="form-control" placeholder="Username"
                    autofocus="true"></form:input>
        <form:errors path="userName"></form:errors>
    </div>
    </spring:bind>

    <spring:bind path="password">
    <div class="form-group ${status.error ? 'has-error' : ''}">
        <form:input type="password" path="password" class="form-control" placeholder="Password"></form:input>
        <form:errors path="password"></form:errors>
    </div>
    </spring:bind>

    <spring:bind path="passwordConfirm">
    <div class="form-group ${status.error ? 'has-error' : ''}">
        <form:input type="password" path="passwordConfirm" class="form-control"
                    placeholder="Confirm password"></form:input>
        <form:errors path="passwordConfirm"></form:errors>
    </div>
    </spring:bind>

    <button class="btn btn-lg btn-orange btn-block mt-2" type="submit">Registration</button>
    </form:form>
