<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container mt-3">
    <div class="row">
        <div class="col">
            <span class="text-secondary"><a href="javascript:void(0)" onclick="openNav()">
                <p class="text-danger">Filter and sorting</p></a></span>
            <%--${aliasesForShopItemObj} --%>
        </div>
    </div>
    <c:choose>
        <c:when test="${true}">
            <div class="row">
            <c:forEach items="${shopItemsObjArrayList}" var="item" varStatus="status">
                <c:if test="${status.index > 0 && (status.index % 6) == 0}">
                    </div><div class="row">
                </c:if>

                <div class="card col-sm-2 col-1">
                    <a href="<c:url value='/shop/shopItemInfo/${item[0]}' />">
                        <img class="card-img-top mt-1"
                             src="${fn:contains(item[7],'http') ? item[7] :
                             (pageContext.request.contextPath) += '/stock/' += item[7]}"
                             alt="${item[2]}">
                    </a>
                    <div class="card-body">
                        <h6 class="card-title text-right"><a href="<c:url value='/shop/shopItemInfo/${item[0]}' />"
                                                  class="text-warning">${item[2]}</a></h6>
                            <%--   <ul>
                             <c:forEach items="${item}" var="descrPart" varStatus="stat.index" begin="8">
                                  <li><span>${aliasesForShopItemObj[stat.index]}</span><span>${descrPart}</span></li>
                              </c:forEach>
                        </ul> --%>
                        <div class="card-text text-right">Available: ${item[6]}<br />
                        Price: ${item[3]}
                        </div>
                    </div>
                </div>


            </c:forEach>
            </div>
        </c:when>
        <c:otherwise>
            <div class="row">
                <div class="col"><h2>Nothing found!</h2></div>
            </div>
        </c:otherwise>
    </c:choose>
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-end mt-1">
            <c:forEach begin="0" end="${paginationDto.maxPages}" varStatus="status">

                    <c:if test="${status.index == 0}">
                        <li class="page-item ${paginationDto.currentPage == 0 ? "disabled" : ""}">
                            <a class="page-link"
                               href="${pageContext.request.contextPath}/shop/list/${paginationDto.currentCategory}/${paginationDto.pageSize*(paginationDto.currentPage - 1)}?${paginationDto.params == '' ? '' :  paginationDto.params}" ${paginationDto.currentPage == 0 ? "tabindex='-1'" : ""}>Previous</a>
                        </li>
                    </c:if>

                <li class="page-item ${paginationDto.currentPage == status.index ? 'active' : ''}"><a class="page-link"
                                         href="${pageContext.request.contextPath}/shop/list/${paginationDto.currentCategory}/${paginationDto.pageSize*status.index}?${paginationDto.params == '' ? '' :  paginationDto.params}">${status.count}</a></li>

                <c:if test="${status.last}">
                    <li class="page-item ${paginationDto.currentPage == status.index ? "disabled" : ""}">
                        <a class="page-link" href="${pageContext.request.contextPath}/shop/list/${paginationDto.currentCategory}/${paginationDto.pageSize*(paginationDto.currentPage + 1)}?${paginationDto.params == '' ? '' :  paginationDto.params}" ${paginationDto.currentPage == status.index ? "tabindex='-1'" : ""}>Next</a>
                    </li>
                </c:if>
            </c:forEach>
        </ul>
    </nav>
</div>