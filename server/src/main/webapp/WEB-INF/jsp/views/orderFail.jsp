<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="wlcm">
    <div class="text-center">
        <h2>Unfortunately, in the process of placing an order there were problems.<br>
        Please check your shopping cart</h2>
    </div>
</div>
