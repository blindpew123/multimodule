<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:url value="/index" var="start"/>
<div class="container mt-5">
<c:choose>
    <c:when test="${fn:length(orderHistoryDto.orderList) == 0}">
        <div id="wlcm">
            <div class="text-center">
                <h2>You have no orders, ${userName}</h2>
                <p class="text-sm-center">Let's <a href="${start}">start shopping</a></p>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
        <c:forEach items="${orderHistoryDto.orderList}" var="order" varStatus="status">
            <div class="card">
                <!-- Card header -->
                <div class="card-header" role="tab" id="headingOne${status.count}">
                    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne${status.count}"
                       aria-expanded="false"
                       aria-controls="collapseOne${status.count}" class="collapsed">
                        <h5 class="mb-0 clearfix">
                            <span class="float-right">${order.orderStatus.orderStatusName}</span>>
                            Заказ №${order.idOrder}&nbsp;<i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

                <div id="collapseOne${status.count}" class="collapse hide" role="tabpane${status.count}"
                     aria-labelledby="headingOne${status.count}"
                     data-parent="#accordionEx">
                    <div class="card-body">
                            ${order.paymentType.paymentName}
                        <ul class="list-group">
                            <c:forEach items="${order.orderItemList}" var="orderItem" varStatus="status">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                    ${orderItem.shopItem.itemName}
                                <span>${orderItem.itemPrice}</span>
                            </li>

                            </c:forEach>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:otherwise>
</c:choose>
</div>
</div>