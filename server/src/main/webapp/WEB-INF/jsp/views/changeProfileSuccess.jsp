<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="wlcm">
    <div class="text-center">
        <h2>Your profile is successfully changed.</h2>
        <c:if test="${not empty noAddress}">
        <p>But to place orders, you will need to add at least <a href="${pageContext.request.contextPath}/customer/showUserInformation">one address</a>.</p>
        </c:if>
    </div>
</div>
