<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/customer/showUserInformation" var="profile"/>
<div id="wlcm">
    <div class="text-center">
        <h2>Your account created, ${userName}</h2>
        <p class="text-sm-center">Please tell us <a href="${profile}">little more about yourself</a></p>
    </div>
</div>
