<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="modal fade" id="modalCartForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="opacity: 0.9;">
    <c:url value="/shop/cartProcess" var="cartProcess"/>
    <div class="modal-dialog modal-lg" role="document"
         style="overflow-y: initial !important;">
        <div class="modal-content card">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Shopping Cart</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form:form method="POST" action="${cartProcess}" acceptCharset="UTF-8" modelAttribute="cart">
                <div class="modal-body mx-3" style="max-height: 300px; overflow-y: auto;">
                    <c:if test="${fn:length(cart.cartItems) == 0}">
                        <span class="text-danger">Your shopping cart is empty</span>
                    </c:if>
                    <c:forEach items="${cart.cartItems}" var="cartItem" varStatus="status">
                        <div class="row border-top pt-2">
                            <div class="col-7 text-left ">
                                <span>${cartItem.shopItemName}</span>
                            </div>
                            <div class="col-1 text-right">${cartItem.price}</div>
                            <span><div class="col-2 " style="max-width: 6em">
                                <form:input cssClass="form-control"
                                            cssErrorClass="form-control is-invalid"
                                            path="cartItems[${status.index}].qnty"
                                            type="number" min="0" size="3"/>
                                <form:input path="cartItems[${status.index}].idShopItem" hidden="true"/>
                                <form:input path="cartItems[${status.index}].idCartItem" hidden="true"/>
                                <form:input path="cartItems[${status.index}].shopItemName" hidden="true"/>
                                <form:input path="cartItems[${status.index}].price" hidden="true"/>
                            </div></span>
                            <div class="col">
                                <button class="btn btn-danger btn-sm " style="margin-top: 2px" name="delId"
                                        value="${cartItem.idShopItem}">X
                                </button>
                            </div>
                            <form:errors path="cartItems[${status.index}].qnty" cssClass="text-danger ml-3"/>

                        </div>
                    </c:forEach>
                </div>
                <c:if test="${cart.total > 0}">
                    <div class="col border-top">
                        <h6 class="text-warning text-right mt-2" style="padding-right: 174px">
                            Total:&nbsp;${cart.total}
                        </h6>
                    </div>
                </c:if>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-deep-orange" name="save" value="save"
                        ${fn:length(cart.cartItems) == 0 ? 'disabled' : ''}>Save changes
                    </button>
                    <button class="btn btn-deep-orange" name="makeOrder" value="make"
                        ${fn:length(cart.cartItems) == 0  ? 'disabled' : ''}>Make order
                    </button>
                </div>
            </form:form>
        </div>
    </div>
</div>