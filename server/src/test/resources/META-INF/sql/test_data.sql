-- important --

LOCK TABLES `ishopdbtest`.`role` WRITE;
INSERT INTO `ishopdbtest`.`role` VALUES (1,'ROLE_USER');
INSERT INTO `ishopdbtest`.`role` VALUES (2,'ROLE_ADMIN');
UNLOCK TABLES;

use ishopdbtest;

insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Карты', NULL, 1, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Аксессуары', NULL, 2, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Книги по вселенной МТГ', NULL, 3, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Кубики', '2', 1, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Протекторы', '2', 2, 0);
insert into category (CategoryName, Parent, CategoryNumber, Deleted)
values('Оверсайз карты', '1', 1, 0);

insert into parametertype(ParameterTypeName)
values ('Строка');
insert into parametertype(ParameterTypeName)
values ('Число');

insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Сила', 2, 1, 1);
insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Защита', 2, 1, 2);
insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Мана-Стоимость', 1, 1, 3);
insert into parameter (ParameterName, ParameterType, Category, ParameterOrdinal)
values('Число-граней', 2, 4, 1);



insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Accursed Spirit',1.5, 1, 10, 0.03, 7, 'm15/85.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Act of Treason',2, 1, 10, 0.03, 0, 'm14/125.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Кубик',2, 4, 10, 0.03, 0, 'dices/6black.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Air Servant',.5, 1, 10, 0.03, 1, 'm14/42.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Child of Night',3, 1, 10, 0.03, 1, 'm14/89.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Dark Prophecy',5, 1, 10, 0.03, 1, 'm14/93.jpg');
insert into shopitem (ItemName, Price, Category, Weight, Volume, Qnty, imageLink)
values ('Magma Spray',1, 1, 10, 0.03, 1, 'jou/103.jpg');

insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '3', 1);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '2', 1);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '3B', 1);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '2R', 2);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (4, '6', 3);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '4', 4);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '3', 4);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '4U', 4);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (1, '2', 5);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (2, '1', 5);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, '1B', 5);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, 'BBB', 6);
insert into parametervalue(Parameter, ParameterValue, ShopItem)
values (3, 'R', 7);

insert into user(emailAsLogin, password) values (1, '$2a$11$NADuousojzp8hVOcnjqFge.YrXdj5pQz0qejZ.GWYa6tgjAggV2em');
insert into user(emailAsLogin, password) values (0, '$2a$11$NADuousojzp8hVOcnjqFge.YrXdj5pQz0qejZ.GWYa6tgjAggV2em');
insert into user(emailAsLogin, password) values (9, '$2a$11$NADuousojzp8hVOcnjqFge.YrXdj5pQz0qejZ.GWYa6tgjAggV2em');

insert into user_role(user_id, role_id) values (1,1);
insert into user_role(user_id, role_id) values (2,2);
insert into user_role(user_id, role_id) values (3,1);

insert into customer(firstName, lastName, birthday, user) values ('Сергей', 'Яковлев', null, 1);
insert into customer(firstName, lastName, birthday, user) values ('Вася', 'Куликов', null, 3);

insert into address(Country, City, PostCode, Street, Building, Apt, Customer, PrimaryAdr)
VALUES ('Россия', 'Санкт-Петербург', 198000, 'Невский пр.', 2, 101, 1, 0);

insert into address(Country, City, PostCode, Street, Building, Apt, Customer, PrimaryAdr)
VALUES ('Россия', 'Санкт-Петербург', 195000, 'Московский пр.', 101, 300, 1, 1);

insert into address(Country, City, PostCode, Street, Building, Apt, Customer, PrimaryAdr)
VALUES ('Россия', 'Мурманск', 100000, 'Полярный пр.', 10, 30, 2, 1);

insert into shipping (ShippingName)
values ('Курьерская доставка');
insert into shipping (ShippingName)
values ('Почта России');

insert into paymenttype (PaymentName) VALUES ('Наличные');
insert into paymenttype (PaymentName) VALUES ('Карта');

insert into orderstatus (OrderStatusName) VALUES ('В обработке');
insert into orderstatus (OrderStatusName) VALUES ('Ожидается оплата');
insert into orderstatus (OrderStatusName) VALUES ('Отгружен');
insert into orderstatus (OrderStatusName) VALUES ('Доставлен');
insert into orderstatus (OrderStatusName) VALUES ('Возврат');

insert into customerorder (Customer, Address, PaymentType, Shipping, OrderStatus, OrderDate)
values (1,1,1,1,1,'2018-09-24 15:40:03');
insert into customerorder (Customer, Address, PaymentType, Shipping, OrderStatus, OrderDate)
values (2,3,1,1,1,'2018-09-24 15:40:03');

insert into orderitem (CustOrder, idOrderItem, ShopItem, Qty, ItemPrice)
values (1,1,1,50,2);
insert into orderitem (CustOrder, idOrderItem, ShopItem, Qty, ItemPrice)
values (1,2,2,20,3);
insert into orderitem (CustOrder, idOrderItem, ShopItem, Qty, ItemPrice)
values (2,3,2,50,1);
