package tk.blindpew123.controllers;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class SeleniumConfig {
    private WebDriver driver;

    static {
        System.setProperty("webdriver.gecko.driver", findFile("geckodriver.exe"));
    }

    public SeleniumConfig() {
        Capabilities capabilities = DesiredCapabilities.firefox();
        driver = new FirefoxDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }



    static private String findFile(String filename) {
        String paths[] = {"c:/geckodriver/"};
        for (String path : paths) {
            if (new File(path + filename).exists())
                return path + filename;

        }
        return "";
    }

    public WebDriver getDriver() {
        return driver;
    }
}
