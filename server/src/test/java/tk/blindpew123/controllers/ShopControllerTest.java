package tk.blindpew123.controllers;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import tk.blindpew123.services.dto.CartListDto;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class ShopControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static SeleniumWebTest seleniumWebTest;

    private MockMvc mockMvc;

 /*   @BeforeClass
    public static void setupSeleniumWeb(){
        seleniumWebTest = new SeleniumWebTest();
    }*/

    @Before
    public void setUp() {
       mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("/indexpage.jsp"))
                .andExpect(model().attribute("cart", allOf(hasProperty("cartItems", hasSize(0)),
                                hasProperty("total", closeTo(0d,0.00001))
                        )
                ));
    }
 /*   @AfterClass
    public static void tearDown() {
        seleniumWebTest.closeWindow();
    }*/
}
