package tk.blindpew123.controllers;

public class SeleniumWebTest {
    private SeleniumConfig config;
    private String url = "http://localhost:8080/iShop/";

    public SeleniumWebTest() {
        config = new SeleniumConfig();
        config.getDriver().get(url);
    }
    public void closeWindow() {
        this.config.getDriver().close();
    }
}
