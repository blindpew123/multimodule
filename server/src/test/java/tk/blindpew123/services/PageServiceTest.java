package tk.blindpew123.services;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.PageElementDescriptor;

import java.security.Principal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class PageServiceTest {

    @Autowired
    private PageService pageService;

    @Autowired
    private UserService userService;

    @Test
    public void testGetCurrentPageDescriptor(){
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };
        PageElementDescriptor pageDescriptor = pageService.getCurrentPageDescriptor(
                "Title", "/path", 1L, principal);

        assertEquals("Title", pageDescriptor.getTitle());
        assertEquals("/path", pageDescriptor.getContentURI());
        assertEquals("1", pageDescriptor.getUserName());
        assertEquals(3, pageDescriptor.getCurrentLevelMenu());
    }
}
