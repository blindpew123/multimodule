package tk.blindpew123.services.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.entities.Category;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class CategoryDaoTest {

    private static final Logger logger = LogManager.getLogger(CategoryDaoTest.class.getName());


    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testSaveOrUpdate_SaveCategorySuccess() {
        Category category = new Category();
        category.setCategoryName("Тест");
        category.setCategoryNumber(5);
        category.setParent(categoryDao.findById(2));
        categoryDao.saveOrUpdate(category);
        assertNotEquals(0, category.getIdCategory());
    }

    //Can't save Category without name
    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSaveOrUpdate_SaveFailDueToValidation() {
        Category category = new Category();
        category.setCategoryNumber(5);
        category.setParent(categoryDao.findById(2));
        categoryDao.saveOrUpdate(category);
        sessionFactory.getCurrentSession().getTransaction().commit();
        assert (false);
    }


    //"Deleted" Categories will not selected
    @Test
    public void testFindAllCategoriesAsDtoList_Success() {
        List<CategoryDto> result = categoryDao.findAllCategoriesAsDtoList();
        logger.info(Arrays.toString(result.toArray()));
        assertEquals(5, result.size());
    }


    @Test
    public void testSaveOrUpdate_UpdateSuccess() {
        Category category = new Category();
        category.setCategoryName("Тест");
        category.setCategoryNumber(8);
        category.setParent(categoryDao.findById(2));
        categoryDao.saveOrUpdate(category);
        assertNotEquals(0, category.getIdCategory());
        Category categoryForUpdate = categoryDao.findById(category.getIdCategory());
        categoryForUpdate.setCategoryName("Не тест");
        categoryDao.saveOrUpdate(categoryForUpdate);
        assertEquals("Не тест", categoryDao.findById(category.getIdCategory()).getCategoryName());
    }

    //Can't save Category with name's length = 0
    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSaveOrUpdate_UpdateFail() {
        Category category = new Category();
        category.setCategoryName("Тест");
        category.setCategoryNumber(8);
        category.setParent(categoryDao.findById(2));
        categoryDao.saveOrUpdate(category);
        assertNotEquals(0, category.getIdCategory());
        Category categoryForUpdate = categoryDao.findById(category.getIdCategory());
        categoryForUpdate.setCategoryName("");
        categoryDao.saveOrUpdate(categoryForUpdate);
        sessionFactory.getCurrentSession().getTransaction().commit();
        assert (false);
    }

    @Test
    public void testRenumberSameLevelCategories_UpSuccess() {
        Category category = categoryDao.findById(2);
        List<Category> categoryList = categoryDao.findAllCategoriesForParent(category);
        assertEquals(1, categoryList.get(0).getCategoryNumber());
        assertEquals(2, categoryList.get(1).getCategoryNumber());
        categoryDao.renumberSameLevelCategories(category, 1, 1);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        assertEquals(2, categoryDao.findById(categoryList.get(0).getIdCategory()).getCategoryNumber());
        assertEquals(3, categoryDao.findById(categoryList.get(1).getIdCategory()).getCategoryNumber());
    }

    @Test
    public void testfindAllCatgoriesForParent_ParentNull(){
        List<Category> categoryList = categoryDao.findAllCategoriesForParent(null);
        assertEquals(3, categoryList.size());
    }

    @Test
    public void testfindAllCatgoriesForParent_ParentNumber(){
        List<Category> categoryList = categoryDao.findAllCategoriesForParent(categoryDao.findById(2));
        assertEquals(2, categoryList.size());
    }

    // Test delete Category and all children
    @Test
    public void testDeleteCategory_Success() {
        Category category = categoryDao.findById(2);
        categoryDao.delete(category);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        assert(categoryDao.findById(2).isDeleted());
        assert(categoryDao.findById(4).isDeleted());
        assert(categoryDao.findById(5).isDeleted());
    }

    @Test
    public void testDeleteCategory_FailDueToNotExistInDb() {
        Category category = new Category();
        category.setIdCategory(100);
        category.setCategoryName("dsdffs");
        categoryDao.delete(category);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        assertNull(categoryDao.findById(100));
    }

    @Test
    public void testFindById_Success() {
        assertNotNull(categoryDao.findById(1));
    }

    @Test
    public void testFindById_NullDueToWrongId(){
        assertNull(categoryDao.findById(1001));
    }


}
