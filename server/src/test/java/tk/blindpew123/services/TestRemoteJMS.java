package tk.blindpew123.services;

import org.junit.Ignore;
import org.junit.Test;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

import static java.rmi.Naming.lookup;

public class TestRemoteJMS {
    private String MESSAGE = "Hello, World!";
    private String CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
    private String DESTINATION = "jms/queue/adv";

    @Ignore // useless, when Wildfly is not running
    @Test
    public void testSendMessage() throws NamingException, JMSException {
        Context namingContext = null;
        // Set up the namingContext for the JNDI lookup
        final Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.naming.remote.client.InitialContextFactory");
        env.put(Context.PROVIDER_URL, "http-remoting://127.0.0.1:8100");
        env.put(Context.SECURITY_PRINCIPAL, "guest");
        env.put(Context.SECURITY_CREDENTIALS, "guest");
        namingContext = new InitialContext(env);
        QueueConnectionFactory connectionFactory = (QueueConnectionFactory) namingContext
                .lookup(CONNECTION_FACTORY);

        Queue destination = (Queue) namingContext
                .lookup(DESTINATION);
        QueueConnection connection = connectionFactory.createQueueConnection("guest","guest");

        connection.start();

        QueueSession session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);

        QueueSender sender = session.createSender(destination);
        TextMessage message = session.createTextMessage("Hello from main");

        sender.send(message);

        sender.close();
        session.close();
        connection.close();
    }

}
