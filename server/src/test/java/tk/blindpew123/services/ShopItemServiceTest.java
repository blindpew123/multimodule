package tk.blindpew123.services;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.ShopItem;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class ShopItemServiceTest {

    @Autowired
    public ShopItemService shopItemService;

    @Test
    public void testGetShopItemsForCategoryAsObjArray_Success(){
        PageListParamsDto pageListParamsDto = new PageListParamsDto();
        pageListParamsDto.setStart(0);
        pageListParamsDto.setSize(10);
        List<Object[]> testArraysList = shopItemService
                .getShopItemsForCategoryAsObjArray(1, pageListParamsDto);
        assertNotNull(testArraysList);
        assertEquals(6, testArraysList.size());
        assertEquals(8, testArraysList.get(0).length);
    }

    @Test
    public void testGetAllShopItemsWithParameterValuesForCategory_Success(){
        List<Object[]> testArraysList = shopItemService.getAllShopItemsWithParameterValuesForCategory(1);
        assertNotNull(testArraysList);
        assertEquals(6, testArraysList.size());
        assertEquals(11, testArraysList.get(0).length);
    }

    @Test
    public void testGetShopItemsAndValuesAndNamesAndTypesAsDto_Success(){
        ShopItemsColNamesColTypesTripletDto dto = shopItemService.getShopItemsAndValuesAndNamesAndTypesAsDto(1);
        assertEquals(11, dto.getColumnNames().size());
        assertEquals(11, dto.getColumnTypes().size());
        assertEquals(6,dto.getShopItemsList().size());
        assertEquals(11, dto.getShopItemsList().get(0).length);
    }

    @Test
    public void testSaveOrUpdateShopItemDtoWithParam_Success(){
        ShopItemSaveDto saveDto = new ShopItemSaveDto();
        ShopItemDto shopItemDto = new ShopItemDto();
        shopItemDto.setIdCategory(1);
        shopItemDto.setItemName("Test Card");
        shopItemDto.setPrice(0.1);
        shopItemDto.setQnty(2);
        shopItemDto.setVolume(0.01);
        shopItemDto.setWeight(1);
        shopItemDto.setImageLink("");
        saveDto.setShopItem(shopItemDto);
        String[] paramsValues = new String[3];
        paramsValues[0] = "";
        paramsValues[1] = "";
        paramsValues[2] = "3U";
        saveDto.setParamValues(paramsValues);
        shopItemService.saveOrUpdateShopItemDtoWithParam(saveDto);
        SingleShopItemTupleDto checkDto = shopItemService.getSingleShopItemInfo(8);
        assertNotNull(checkDto);
        assertEquals("3U", checkDto.getShopItemNamesAndValues().get(2).getValue());
    }

    @Test
    public void testMapShopItemDtoToShopItem_Success(){
        ShopItemDto shopItemDto = new ShopItemDto();
        shopItemDto.setIdCategory(1);
        shopItemDto.setItemName("Test Card");
        shopItemDto.setPrice(0.1);
        shopItemDto.setQnty(2);
        shopItemDto.setVolume(0.01);
        shopItemDto.setWeight(1);
        shopItemDto.setImageLink("");
        ShopItem shopItem = shopItemService.mapShopItemDtoToShopItem(shopItemDto);
        assertNotNull(shopItem);
        assertEquals("Test Card", shopItem.getItemName());
    }

    @Test
    public void testGetSingleShopItemInfo_Success(){
        SingleShopItemTupleDto checkDto = shopItemService.getSingleShopItemInfo(1);
        assertNotNull(checkDto);
        assertEquals("3", checkDto.getShopItemNamesAndValues().get(0).getValue());
    }

    @Test
    public void testGetShopItemsSortedAndFiltered_Success(){
        FilterFormDto filterFormDto = new FilterFormDto();
        for (int i = 0; i < 11; i++){
            FilterCriteriaDto filterCriteriaDto  = new FilterCriteriaDto();
            if (i == 8) {
                filterCriteriaDto.setValue("1");
                filterCriteriaDto.setMathCondition(">");
                filterCriteriaDto.setFieldAlias("p1");
                filterCriteriaDto.setFieldType("Число");
            } else {
                filterCriteriaDto.setValue("");
                filterCriteriaDto.setMathCondition("");
                filterCriteriaDto.setFieldAlias("");
                filterCriteriaDto.setFieldType("");
            }
            filterFormDto.getFilterList().add(filterCriteriaDto);
        }
        filterFormDto.setSortName("p1");
        PageListParamsDto pageListParamsDto = new PageListParamsDto();
        pageListParamsDto.setSize(10);
        pageListParamsDto.setStart(0);
        List<Object[]> list = shopItemService.getShopItemsSortedAndFiltered(1, filterFormDto, pageListParamsDto);
        assertEquals(3, list.size());
    }

    @Test
    public void testGetShopItemById(){
        ShopItem shopItem = shopItemService.getShopItemById(1);
        assertNotNull(shopItem);
        assertEquals(1,shopItem.getIdShopItem());
    }

    @Test
    public void testGetAllShopItemForCreatingOrder(){
        List<ShopItem> itemList = shopItemService.getAllShopItemForCreatingOrder(new Object[]{1L,2L,3L});
        assertEquals(3, itemList.size());
        assertEquals(1, itemList.get(0).getIdShopItem());
    }

    @Test
    public void testGetTop10QtyItems(){
        List<ShopItemStatTop10Dto> listTop10 = shopItemService.getTop10QtyItems();
        assertNotNull(listTop10);
        assertEquals(2, listTop10.size());
    }

    @Test
    public void testGetAdvTop10QtyItems(){
        List<ShopItemAdvTop10Dto> listTop10 = shopItemService.getAdvTop10QtyItems();
        assertNotNull(listTop10);
        assertEquals(2, listTop10.size());
    }
}

