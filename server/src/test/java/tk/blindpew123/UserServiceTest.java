package tk.blindpew123;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.UserService;
import tk.blindpew123.services.dto.UserRegisterDto;
import tk.blindpew123.services.entities.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class UserServiceTest {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserService userService;

    @Test
    public void testSaveUserWithUserRole(){
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto.setPassword("q");
        userRegisterDto.setUserName("q");
        userService.saveUserWithUserRole(userRegisterDto);
        User user = userService.findByEmailAsLogin("q");
        assertNotNull(user);
        assertEquals("q", user.getEmailAsLogin());
        assert bCryptPasswordEncoder.matches("q", user.getPassword());
    }
}
